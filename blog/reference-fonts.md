[brutal]: #title "Reference fonts"

Here I think Reference Fonts should be a curated selection of
fonts that can be used to answer questions like
“How far below the baseline does the comma extend?” and
“What is the typical space between M and H?”.
Or at least providing a starting point for exploring such
questions more thoroughly.

What should those fonts be?

Helvetica and Times New Roman are so ubiquitous that they should
be included.

Default font on various browsers and programs.
DejaVu (default on Firefox)

Fonts that are well regarded for their permanance and quality.
Janson, Garamond, Baskerville, Clarendon.

Fonts that are available in Regular, Italic, and Bold.

Fonts that are properly spaced, kerned.

Fonts that cover a reasonable range of glyphs
(more than just 95!).

Families (example: Sans, Serif, Mono)

## Some lists

The major open fonts (see also
https://en.wikipedia.org/wiki/Open-source_Unicode_typefaces)

### Superfamilies

- Croscore, Arimo, Cousine, Tinos, metric compatible with Arial, Courier, Times
- DejaVu Family,https://dejavu-fonts.github.io/ descended from Bitstream Vera, default
  for Firefox content
- Freefont Family
- Kurinto, https://kurinto.com/ incredible specimen book.  Comprehensive (100s of Megabyte to download!)
- IBM Plex, https://www.ibm.com/plex/ IBM branding released under SIL
- Luxi Family,? https://gitlab.freedesktop.org/xorg/font/bh-type1 Mono, Sans, Serif; ugly but seems comprehensive.
- Noto (Google), Hyperfamily (derived from Droid?)
- Roboto, only just merits superfamily.

### Families

- Adobe Source
- Canada1500, http://typodermicfonts.com/canada1500/
- Charis SIL, descended from Bitstream Charter
- EB Garamond, is very good https://fonts.google.com/specimen/EB+Garamond?query=eb+garamond&sidebar.open=true&selection.family=EB+Garamond:wght@500
- Fira, a sans serif for Firefox OS
- Junicode
- Libertine, Wikipedia Logo

- M+ Family,https://en.wikipedia.org/wiki/M%2B_FONTS,https://mplus-fonts.osdn.jp/
  Sans Mono Kanji + Latin No Italic/Oblique, but included
  here for the otherwise extensive selection of weights
  and styles.
- Open Sans, same designer as and derived from Droid Sans
- Overpass, https://overpassfont.org/ (both this and plex
  have quite a nice glyph browser), this has a sans, sans
  oblique, and mono, so technically should be in
  superfamiles, but ah meh.
- Ubuntu Family,
- Utopia,
  https://gitlab.freedesktop.org/xorg/font/adobe-utopia-type1
  There are derivatives that are probably superior in
  practice. See Wikipedia.

Open, but minor:
- Bitstream Charter, ancestor of Charis SIL. Probably easier to use the more comprehensive Charis SIL, which has the same basic letterforms.
- Doulos SIL, only available in Roman (similar to Times New Roman)
- Gentium, only in Roman and Italic, but a bold is planned. Both the Roman and the Italic are quite good.
- Ubuntu Titling, one weight, lower case only
- Andika SIL, a single humanist sans regular, but it is exceptionally clear
- Liberation Family, metric compatible replacement for
  Arial, Times, Courier, but probably better superseded by
  the Croscore Arimo, Tinos, Cousine fonts. Still in use
  at developer.mozilla.org
- Nimbus, Nimbus Roman No9 L and Nimbus Sans L have
  superior derivatives in Freefont (for example, better
  kerning).
- Nimbus Mono
- Droid Family (kinda old), subsumed by Noto

Defective

- ET Book, Edward Tufte!  https://edwardtufte.github.io/et-book/ no kerning (wah!)
- Cardo, http://scholarsfonts.net/cardofnt.html
  unmaintained, no kerning, corrupted by incorect
  vertical spacing (fixable?). Shame though, as otherwise
  looks okay. The italic is sound.
