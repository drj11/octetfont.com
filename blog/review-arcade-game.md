[brutal]: #title "Arcade Game Typography, a review"
[brutal]: #date "2021-04-14"
[brutal]: #author "David Jones"

The book is _Arcade Game Typography_ by Toshi Omagari.

It's essentially a specimen book for a specialised genre:
Arcade Game fonts.

If you're even a little bit into arcade games you have to get this book.
It is unique and probably
the first really serious attempt to
investigate Arcade Game Typography.

As first mover Toshi has a lot of groundwork to lay:
genres to define, histories to write, theories and rules to write.
The rules of this work are: 8×8 pixels, monospace,
featured in a published _arcade_ game.

Typically each specimen has a short descriptive paragraph, pointing out
unusual features, historical relevance, or linking to other works.
Some specimens have a full page video game screenshot.

But it's not just specimens,
special layouts, essays, and full- and double-page spreads add context,
offer questions and research, and show a little of what lies beyond.
They serve well to break
up what could otherwise be a monotone work.

The book is beautifully designed and well printed except for the occasional
colour mistake.
A marked feature of video games typography is colour,
which quickly becomes routine, with several colours being used in one font.
Sometimes the colours chosen for printing make it difficult to see the
difference between a coloured pixel in the font and the background
(which would be masked out in a video game), and sometimes two font palette
colours are so close when printed that they are very difficult to tell
apart.

There is even the discovery of a _genre_ unique to video games.
The Letraset favourite Data 70 is an amusing graphical oddity in
most of typography, but here in the world of video games,
the graphical style is used so much that it has been elevated to a
genre:
MICR, short for Magnetic Ink Character Recognition.

As the closing essay notes the specialised field of hand-drawn 8×8 fonts
soon got eclipsed when video game hardware systems were able to use the
same vector-based fonts and font engines that office and home computer
systems use.
That makes the designs of this period a brief flare.
I agree with the author, there is much to be gained by typographical study
of the fonts produced by often typographically naive designers and
programmers in a constrained, but colourful and dynamic, graphic system.
