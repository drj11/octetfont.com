[brutal]: #title "A shopfront for fonts"

How to sell fonts?

## raw list

- fontspring.com - rejected. you keep 70%
- fontshop.com - no (via myfonts?)
- myfonts.com - I'm signed up (fonts.com fontshop.com linotype.com); you earn 50% (of net!).
- dafont.com - free only, pay via donations?
- 1001fonts.com - free only
- etsy - approx 86%
- creativemarket.com - 60%; they are trying to eat the middle
- thehungryjpeg.com - you keep 70%
- fontbundles.net - you keep 50-75%
- sofontsy.com - secret compensation deals (see beckmccormick)
- creativefabrica.com - you keep 50-75%
- crella.com
- CreateBright (broken?)
- HypeForType - you keep 50%
- google drive (can't sell, but can give away; page on behance)
- Fontstand (rent by month!)
- FutureFonts
- youworkforthem.com - you keep 50%; affiliate program
- i love typography - private invite
- itch.io vendor sets rate, anywhere from 0 to 100%.
- dropbox


##  Exclusive?

The [Get
Started](https://foundrysupport.monotype.com/hc/en-us/articles/360028863311-Get-Started)
Monotype page says “If your font is sold elsewhere, the pricing
on our sites must be the same or less”, implying that it is
possible to sell fonts elsewhere.

I think most resellers or foundries are okay with you selling
elsewhere.


## fontspring.com

I quite like their “worry free” program.
Rejected 2021-06-17. Apparently they like corporate branding
style fonts.


## dafont.com

i submitted 2 fonts, and 18 months to two years after i
submitted them, Artzoid got rejected, and Necker got accepted.

Submission requires:

- a font file / zip
- licence selection
- author note
- optional illustration, 800px wide max


## MonoType cluster

Recommended glyph set is here:

https://foundrysupport.monotype.com/hc/en-us/articles/360029280752-Recommended-Character-Set

This is now available in `chas monotype`.

Atwin has been available on the Monotype cluster since 2021-08.

Graphics are recommended to be 2000×1000 (landscape) according
to https://foundrysupport.monotype.com/hc/en-us/articles/360028552632-Font-Marketing-Best-Practices


## FutureFonts

Their thing is marketing fonts through their development cycle,
from an incomplete single font to a large family.

Do take submissions, but their process is basically
"make a PDF and submit it".

https://www.futurefonts.xyz/submissions

I submitted AIRVENT. Rejected 2022-01-25


## etsy

https://www.etsy.com/uk/seller-handbook/article/how-to-sell-digital-downloads-on-etsy/47330319230

As far as i can tell from
https://help.etsy.com/hc/en-gb/articles/360035902374-Beginner-s-Guide-to-Etsy-Fees?segment=selling
etsy charge 0.20 to list 1 item (multiple items still get charged 0.20 per
item); they take 5%; and payment processing is another 5% (+0.20).

On a USD 10.00 item (a font, say) you would get USD 8.40


## HypeForType

You keep 50%.

https://www.hypefortype.com/sellyourfonts/

They have a comprehensive submission pack.
Images, description, foundry, and so on.

- 5 to 20 PNG preview images, 950 wide by 460
- a specimen (showing all or most of letters?), 806 by 1116 high

They have a minimum and a recommended charset:

Which obviously i can't cut and paste out of the PDF. *sigh*
It's:
- a to z
- A to Z
- 0 to 9
- !?$£€#&
- @%*{[(-)]}:;,^_+|"<>\'./—–

In their PDF doublequote and singlequote have been drawn
typographically, but there are no others, so i expect they want
the ASCII versions. I've had to guess that the dashes required
are en-dash and em-dash.
It's basically ASCII without tilde or
backquote (both of which might be considered accents), and with
en- and em-dash and sterling and euro.

Atwin nearly meets this.

Their recommend is:
- For all letters except double-s and mu both lower- and upper-case
  are needed;
- yen
- thorn, eth, mu, double-s
- ae, oe, oslash
- a grave,acute,circumflex,tilde,umlaut,ring
- ccedilla
- e grave,acute,circumflex,umlaut
- i grave,acute,circumflex,umlaut
- ene
- o grave,acute,circumflex,tilde,umlaut
- s- and z-caron
- u grave,acute,circumflex,umlaut
- y acute,umlaut
- currency sign, pilcrow, long f, cent, exclamdown,
  questiondown, perthousand, registered, copyright,
- multiply, divide, plusminus, bullet
- guillemets, single and double
- en- and em-dash
- backquote, tilde, broken bar, logical not, raised circle
  (probably U_00B0 degree sign), circumflex and caron accent,
- typograhic quotes, single and double and low forms, another
  dot (middledot?), and ellipsis.

As far as i can tell this is Latin 1 Supplement but without
fractions, superscripts, and the spacing accents.
With en- and em-dash, sterling, and euro (same as minimal).
Extra: long s, typographic quotes, ellipsis.

Submitted Avimode.
As of 2022-03-30 they appear to be unresponsive to new submissions.
Appears to be running in "just keep the servers running" mode.


## YouWorkForThem

https://www.youworkforthem.com/submissions

e-mail them and include a link to your online portfolio.
Allow 5 to 10 days
(previously this document said quarterly,
so i wonder if it has improved).


## REFERENCES


[MCCORMICK]

https://beckmccormick.com/where-to-sell-your-fonts-my-experiences-with-various-marketplaces/

Actually seems like quite a good article.
Reviews what i guess is the "crafty" side: etsy, creativemarket,
thehungryjpeg, fontbundles, sofontsy, creativefabrica, youworkforthem,
crella and does not mention fontspring, fontshop, or the monotype cluster.

A later pers. comm. reveals that for myfonts you can't set price, and for
the others you need a "quality font".

[NINA]

https://blog.nina.coffee/2019/01/15/my-experience-selling-fonts-on-myfonts-with-pictures.html

Some pain over submission, no discussion of licensing.
Delay of approx 6 months.

Ramen Sans is quite cute though: https://nefertiti.gitlab.io/ramen-sans/

plaque is a simple single page, showing some of the weights and
sizes (there is an italic, but it's not shown).
No attempt to show glyph range or accent or script or language range.

# END
