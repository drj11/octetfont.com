## On Typography

"Thames and Hudson Manual of Typography", Ruari McLean, 1980.

"The Elements of Typographic Style", Robert Bringhurst, 1997
(2nd edition).

As our instructor points out, Typography is not Type Design.

The Thames and Hudson Manual of Typography is very focussed
around Book Design;
the edition that i have is very dated, being pre-computer.
The couple of chapters on lettering and type might provide
useful background.
Has a nice one-page break-out on the terminology and anatomy of
letterforms.

Elements of Typographic Style is a classic.
Really excellent stuff on how to wield type and
useful advice on lots of the non-alphabetic glyphs in a font.
My 2nd edition is now quite dated, hopefully the newer editions
are just as good.

## Type design

"Designing Type", Karen Cheng, 2007.

"How to create typefaces", Cristóbal Henestrosa / Laura Mesegeur
/ José Scaglione, 2017 (Orange Cover).

"Type tricks", Sofie Beier, 2017.

Designing Type is the absolute mainstream manual of how to
design the letters A to Z for a running-text font.
Excellent for designing another typeface to go alongside
Sabon / Centaur / Hoefler Text.
Or, more practically, for inspecting a few typical variations of
any letter that you might be stuck on.
Has a sans-serif section which i haven't read.
It feels petty to say, but i really dislike the shiny paper and
the thin small neo-grotesque font that it's typeset in.
New edition coming out?

How To Create Typefaces is charming and beautifully made.
Excellent advice and illustrations.
I have a soft spot for any book which puts the contents list on
its cover.

Type Tricks is small and fun
and printed using a spot-colour fluorescent accent colour.
Not so much of a "first manual" sort of book, but
great for dipping into.
Good (short) section on spacing using Tracy's method.


