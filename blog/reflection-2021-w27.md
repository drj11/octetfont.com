# 2021-W27 2021-07-08

Many things are tiring.
The University continues to be a source of annoyance.
The government has announced everything is opening. What a joke.

Went for a walk with DW. Good.


## Examples

Still pending: SYSTEM FAIL in Atwin.

Prepared specimen for Type Crit.
Preparing materials is useful.
Atwin looks terrible at < 24pt
(and, according to my critiquer, at 24pt too).

Stole the idea to use translations of the UNHDR preamble as text
for the Specimen because it is multilingual.
Critiquer commented that it was the first time that someone had
prepared multi-language material.


## Fonts

`font-atwin`

The Type Crit specimen revealed that kerning for accented
letters was wrong and needed doing.

Discovered how to create kerning groups in Glyphs Mini. Annoyed
that when joining Glyphs to a gorup it doesn't let unkerned
glyphs take on existing kern values. Meaning that you have to go
through every group and click on the lock to use that value for
the whole group (for example, to copy the kerning for A over to
the whole group that has all of the accented versions of A).

Presumably this would be different with `mark` and `mkmk`
tables, which Google Fonts would like, but Glyphs Mini does not
produce those. Font8 `ttf8` tools does produce a mark table, but
the kerning for wide accents is wrong.

Kerning for wide accents (eg Iumlaut) is wrong on Glyphs Mini.
Mostly it is fine, it merely looks odd but acceptable when I has
an accent.
But when I with an accent is next to another letter with an
accent the accents may clash.
Approaches:
narrow the accents.
kern.
GSUB/GPOS coombination of the above.

Accents mostly do not change the kerning from their root glyph,
but there are some forms that are drawn as composites that do
require kerning or some kerning exceptions:

Hbar left and right. For some reason it looks like i had to kern
Hbar twice.
Eth on left.
Lbar on left, and some exceptions for the right-hand case to
cover deep kerns that are L-something.
Tbar on left and right (similar to Lbar right) also requires
some exceptions for deep kerns.

ß and ÞZ needed some very minor kerning.

Æ in Atwin can _mostly_ be kerned like A on the left, but
requires some exceptions.
Annoyingly, can't group the exceptions with Ǣ which are exactly the same.

Kerning values < 20 are very minor. Consider eliminating?

There is a strong need for a tool to extract kerning tables from
compiled fonts so that we can write checks that for example all
glyphs with A as a base glyph are kerned the same as A. Or at
least, we can check the exceptions by hand.

As per Type Crit, the alternate D is better (more kerning), so
we should just use that.

How do we support accents for alternates?
Just do them all again?

The cedilla accent when composited is just a bit rubbish.

`font-auncial`

I started reworking this with a more circular O.
I got a better appreciation of how the tool width and angle
interact and create the almond shaped lozenge counter.

Note sure I like the new forms, but i should try them.

The old two-pencils-joined-together would be a good idea.
Or some actual drawing of any sort.

## END
