[brutal]: #title "The Geometry of Type"
[brutal]: #date "2021-05-06"
[brutal]: #author "David Jones"

The book is _The Geometry of Type_ by
Stephen Coles.
I have the UK 2012 edition;
the book is titled “The Anatomy of Type” in some territories.

100 fonts, each font is displayed on a two-page spread.
Split into 16 categories, prefaced with the usual blurbs, glossaries,
a type classification system loosely based on Vox, and one of those nice
poster spreads pointing what a serif and a stroke is.

The book is well made and written.
The choice of fonts unquestionably leans towards the modern, and
often boutique and expensive.
The modernity is a good thing, because, as the book makes clear,
much has happened in typography in “the early OpenType period”.
The selection is unquestioningly latin,
there is not even a mention that fonts for other scripts might exist.
We don't get to know when a
font has a Greek or Cyrillic set included,
which is increasingly common, but not so common that
you can take it for granted.

Each font is presented with a large sample, a secondary sample, and
in smaller size a character set
(a complete A to Z with numbers, accents, fractions, and
a few punctuation characters, but unlikely to be the entire glyph set).
Each font is blurbed, and a few fonts displayed for comparison.

The 2-page spread is fixed, and this is both a strength and a weakness.
It allows some variation to discuss particular features of a font,
such as the many ligatures of Interlock, or the related styles of Calibre
and Metric which are presented together.
Comparisons between fonts are possible, but
the large-size sample changes,
so it's a bit random whether a particular letter can be compared.

The narrative is well-hidden,
the whole book is something of a cryptic puzzle on some levels.
Why this selections of fonts, why these chapters?
Who, or what, is the protagonist, and where are they going?
We see names like Adobe, FF, ITC recurring.
Are these then the personas of the story?
We can attempt to draw out a narrative.
Starting with (Adobe) Jenson and finishing with Marian is a statement.
It is a very broad, indefinite, historical sweep.
Adobe Jenson is named after Nicolas Jenson, traditionally credited with
establishing the _typographical roman_ as a shape and a typeface.
Marian is one typographer's reduction of a font to a skeleton form,
and marks out a new way of thinking about classification.

The chapter divisions play tricks with your categorical mind.
Just as i reach the end of Geometric Sans and i see Forza,
i am thinking that doesn't really belong here, and
then i turn the page to find the next chapter, Humanist Sans.
Humanist Sans closes with Beorcana leaving me wondering whether it should
really be in Neo-Humanist Sans, the following chapter.
Delightful.

Every book of fonts with a category system comes with an apology for the
category system.
A lot of categorical systems end with a “miscellaneous”,
“display”, or “decorative” category.
Which is a collection of fonts too diverse and unusual to be categorised.
_Geometry_ is no exception,
meaning that the main thrust of the book does not end in a carefully
sharpened point, but in rambling and slightly incoherent conclusion.

What am i supposed to think of a chapter that puts the monospace Nitti next
to the polymorphic Ed Interlock and then leads us straight into the
relatively recent charmer that is Bree?
It leaves this reader slightly bewildered.

# END
