[brutal]: #xdate "2023-03-31"
[brutal]: #author "drj"
[brutal]: #title "Dyslexia and Fonts"

My GP has a website with an “Accessibility Widget” that
enables a "Dyslexia Friendly" font.
So, it now falls to me to Have Opinions On The Internet about Dyslexia Fonts.

My overarching opinion is that literature on how fonts improve
meaningful outcomes (which to me would be accuracy of comprehension,
quality of decisions made, and speed of making decisions) is scandalously
poor.
In addition to which, the literature on fonts that aim to improve
people who has dyslexia is even worse. It's barely better than
snake oil peddlers.

And yet, intuitively it's obvious that fonts (and typography) can
make a reading experience better or worse, and it's plausible
that in critical situations a good choice of font could make a real
difference.

Another gripe with a lot of so-called Dyslexia Font research is that
it rarely seems involve actual type designers.

Charles Bigelow wrote http://web.archive.org/web/20201111145946/https://bigelowandholmes.typepad.com/bigelow-holmes/2014/11/typography-dyslexia.html
back in the day, and there is no reason to think that much of changed.
The very short summary is:

- Font choice may or may not make a difference, but current research is
  not good enough to show that;
- Other typographic factors, such as size, line spacing, and tracking
  _do_ make a difference.


## The Font

The overall style of the font is a sans serif with a medium contrast
a vertical axis, annd bottom-heavy weighting and keystone effect that
makes the bottoms wider.
Occasional serifs and strokes are added to make some pairs
not so similar.
The bottom-heavy weighting, and partly the keystone effect, is
a common feature among fonts intended to help with Dyslexia.
Apparently to combat a b/d/p/w rotating effect.

Some particulars:

Many of these improve legibility (or are intended to).
By legibility typographers usually mean the ability for a letterform
to be recognised when isolated.

- **l** **I** **1** are separated in a manner similar to Menlo:
  **l** has a turn at the bottom of the stroke;
  **I** has small crossbars to make it an I-beam shape;
  **1** has a flag

Many fonts use this design skeleton and it is likely to improve legibility
even without any fancy evidence.

- To differentiate **b** from **d**, a flag is added to... **b**!
  The surprise here is that many fonts have a flag (a leftward
  pointing short stroke or sort of serif) on the **d** not the **b**.

On the **b** it looks weird, so i would like to see evidence that
it helps more than putting a flag on the **d**


- a slash is added to 0. Standard, uncontroversial.

- **6** and **9** are differentiated by having, in each case, a
  heavier bottom. Their design page implies that there is more going on,
  but there isn't.

- The **q** is drawn with the shape of the capital.

This is typographically unusual, but in practice it seems fine to me.
In English **q** is fairly low frequency, so the unusual form is
unlikely to disrupt most reading.

The **j** is also drawn with capital form (although descending).

On my GP website the **n** has the capital form. Unusual choice, given
the high frequency of **n** in English;
however, on the website of the widget, the font has **m** with capital form
and **n** with regular form.
Which makes more sense, because **m** has lower frequency than **n** and
it's a good idea to separate **m** and **n**.

Some general aspects:

The ascenders are really short. The  **d** is almost short enough to look
like the single story **a**.

Speaking of **a**, the font has a two-storey **a** but a single story **g**.
This is not so unusual as to be wrong, but conventionally a font
would have a single storey **a** and **g** or a two-storey **a** and **g**.

The ear on the **g** is too weak. It should either be straight, or
hooked like the **b** and **r**.

The openings on **a** **c** **e** **s** are tiny.
In a general purpose font this would not draw comment (Helvetica has
a similar feature), but in a font designed to be clear, this should be
avoided.
We know from the literature that small opening increase confusion.

The font comes with a website which comes with a browser.
Which is fine, but.
The diacritics tho.
Selecting the diacritics, shows the font in a horrible light.

The diacritics are shown in the wrong position, which is weird because
they look fine in Glyphs.app and `hb-view` shows them correctly too.

The diacritics themselves are drawn without enthusiasm, as rather generic
versions, instead of matching the style of the font.
Except for breve (or it's possible that the naturally bottom heaviness
of breve matches the rest of the font by accident).

The repertoire has a very 1990s feel. Superscripts for 1 2 3 are available
and fractions for ¼ ½ ¾ but no general superscript numbers or fractions.
`/fi` and `/fl` ligatures are drawn, but no OpenType Layout Features.
In fact no features at all, so also no kerning.

The ligatures by the way aren't necessary: **fi** and **fl** look fine
without the ligature glyphs.

The general impression is of a font made competently, but
lacking the finishing touches that a modern professional would bring.

# END