[brutal]: #title "k and s"
[brutal]: #author "David Jones"

What does Panel-A tell us about the shapes of **k K s S**?

Both **k** and **s** share an affinity with their upper case.

# k

A lowercase **k** can be approximated by
reducing the **K** to x-height and
extending the vertical stem to ascender height.

**K** has either one or two junctions. One junction where the leg
meets the arm at the point where they both join the vertical
stem (or nearly so). Or two junctions where the leg meets the
arm partway along its length, and the arm meets the vertical
stem at a separate place.

In a serif font the **K** has bilaterial serifs on all 4 terminals,
except that the left-hand serif of the leg (on the inside) is
reduced or eliminated in bold forms.

In a serif font the **k** has a left-flagging serif on the top of
the vertical stem, and otherwise has bilateral serifs.
With the same modification for bold forms as **K**:
reduce or eliminate the left-hand serif of the leg.

Where a font has contrast,
typically the leg of **k**/**K** is thick and
the arm is thin (the thinnest stroke in the font).

# s

The crude form of an **s** is a top and bottom each made from part
of a circle, joined by a spine.
This is far too simple a description however.

The spine is either straight (or nearly straight) in the middle
with curved ends ,or a more continuously curving sigmoid.
In either case there is a point in the middle where the curve of
the spine (from top to bottom) goes from the left side to the
right side. This is the median angle of the spine.

The median angle of the spine varies from 45 degree (Century
Gothic) to nearly flat (DIN, Souvenir Demi), more usually being
somewhere in between.
The lower-case **s** generally has a more shallow angle than
the upper-case **S** because generally the **s** is
squashed more vertically than horizontally compared to the **S**.

Not seen in this showing, but we know there are examples where
the median angle of the spine is nearly vertical (Churchard
Design) or very nearly horizontal (FF DIN).

Often the circular parts are made oblate and/or the terminal has
less of a curve.

All the relevant angles vary a lot. Helvetica is well-known for
having abrupt terminal cuts that are either horizontal or
vertical, horizontal in the case of the **s**.
Slightly angled in Franklin Gothic.
Very angle (not quite 45 degrees) in Avant Garde Gothic.
Even more angled (maybe 60 degrees) in Optima.
All with a corresponding final angle of the stroke.

In a serif (or flared terminal) the inside curve of the terminal has a
smoothness. The serif or flare is made either by making the
outside curve of the terminal turn away (Times, Bodoni), or
by opening it slightly (Optima, Souvenir).

The width of the **S** (which affects the oblateness of parts based
on a circle) varies considerably. Both when considered as the
**S**'s aspect ratio and when compared to other letters like **H** or **O**.
Usually the **S** is a little less wide than the **O**.
but sometimes is as wide or wider (Franklin Gothic, DIN), or
in the opposite direction considerably narrower (Century Gothic, Optima).

Wideness of **O** does not necessarily correspond with **S**.
DIN has narrow **O** and narrow **S**.
Souvenir has wide **O** and wide **S**.
Century Gothic and Optima have wide **O** and narrow **S**.
Franklin Gothic has narrow **O and wide (wider) **S**.

The aperture into the counters of the S is similar for the two
counters. The size of the aperture effects the shape of the
counter and the point at which the stroke is terminated (or the
other way around, if you want).
In Century Gothic the counters are convex and have no aperture
at all really.
Similarly Optima has only a slightly narrowing at the aperture.
More typically the aperture gap is about half the width of the
counter when measured perpendicular to the median angle of the
spine.
Occasionally the aperture is even more narrow.

In a serif font, Karen Cheng’s description of the counter having
a shape that could nestle an egg is a good one.
In a sans serif the curved part tend to be a little bit more
geometric, and the terminal cut (typically straight) adds its
own element to the counter.

The bottom part of the **S** is bigger than the top part, in width,
height, and value (if not all of those then at least some).

Typically the left-most part of the bottom terminal is at or to
the left of the extreme point of the top curve.
Typically the right-most part of the top terminal is at or to
the left of the extreme point of the bottom curve.
In otherwords the **S** is not symmetric.

Sometimes the differences between the two parts of the **S** are
large enough to be seen at ordinary sizes, other times
magnification is required.

# END
