# Thoughts on editors

someone pointed out that emacs special power is
unifying everything as a buffer of characters. i agree.

There are editors out there that are lifestyle commitments.
the most obvious of these is `emacs`.
it's not just an editor, it's a _programming language_,
a documentation browser, an e-mail client, a plugin system
and a market for plugins.

Then there are editors that are just, editors. nano.

i would argue that vi crossed over, when it grew into vim.
It's probably why i like vi and dislike vim
(my daily editor is nvim, mostly because
current versions of vi are hard to obtain and/or buggy).

The more serious an editor is, the more it has a scripting capability.
nano isn't really scriptable, but even it has an rc file with
a command based scripting language;
you can't excute these scripts in editor or on a standalone basis,
which is why i say it isn't scriptable.

`/bin/ed` is naturally scriptable in that it doesn't care where the input
comes from and doesn't require or use or full-screen tty capabilities.

At some point, as the scripting side grows, we cross over the
"Chasm of Turing Completeness". Where the scripting system becomes
a Turing Complete language. Either clumsily or accidentally,
in the case of `sed`, or deliberately and centrally, as in `emacs`.

# END
