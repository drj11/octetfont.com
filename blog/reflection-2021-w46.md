# 2021-W46 2021-11-18

It's been a while. Again.


## Linocuts!

Did a 2-colour chough, with a tiny amount of red.


## Lino fonting

Stalled.


## ILT

No movement on this. I need to ping them.


## Book design

Done a small amount of preliminary work. Tried Palatino.
A couple of designs for the text on the cover, some of them
using [Infini](https://fontsinuse.com/typefaces/39792/infini).


## FWB

Small progress on `fwb`.

Started `feax` (Feature Extract) for converting the features in
OpenType fontfiles to readable feature file syntax. That is a
lot of work.

Created as a spin-off from `feax` the program `glys` which lists
the names of the glyphs.

Experimented with creating tools to join SVG files horizontally,
`slugs`, and vertically, `gley`.


## plak / hb-view

A recent change in hb-view means that any use that puts a
newline in the command-line args breaks.
This breaks plak and many many tests.
Some of which I've fixed.


## Metafont

Very recently started reading the Metafont Book and got `mf`
working on my laptop. Can't preview anything on screen.


## Font Panels

Font panels continue to be useful.


## Fonts

`font-antscap`

Now has A to Z, several variants, and some feature rules.
It looks chaotic and great.

It's clear that further development on this will require some
method of managing the complexity of the desired and actual
rules.
Probably using a test suite and hb-shape (which outputs glyphs
by their name).

`Avimode`

(previously known as Acacounter)

Added tilde breve and comma accents, and related letters.
Added some bar and slash forms (is this complete?)

Added kerning.
Including kerning Eth by using ligatures and
modified shapes.
This is explosive in glyphs.

Creating a ligature for EÐ now also means i have to create the
corresponding accented set.
And, because Æ shares a right-hand shape with E, the same again for Æ.
And presumably Œ which i've just forgotten about.


`font-arculon`

Previously known as `font-ao`.

Fixed the numbers to be properly lower case.


`font-airvent`

A new find.
A scan and clean of an US military standard for
instrument panel lettering. 
The clean monoline sans-serif with rounded ends appeals to me.

Originally had upper case and numbers, i'm adding small caps.

I see many possibilities for developing this font.

Spaced but not kerned.


`font-artzoid`

A fairly ridiculous rapid draw of a sketch.
Quite funky and informal.
The numbers are extremely terrible.

Lower case only (upper case drawn on paper).

Spaced and kerned (lower case didn't require any kerning).


`font-amplette`

For no particularly good i invented a bunch of new latin letters
and added them.

I spaced the letters in amplette so that the name could be
typeset.


## Submissions

Submitted _artzoid_ to dafont, and _airvent_ to Future Fonts.
No response from either.


## One page

I created drafts of a one-page offer sheet.
It previews 9 fonts.
it's a good idea.


## END
