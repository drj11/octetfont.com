[brutal]: #title "Constrained hinting"
[brutal]: #author "David Jones"

Can hinting be implemented or assisted or conceptualised using
some sort of constraint problem?

## Keywords

For background and research, useful to look at the Wikipedia
pages for:

- [Mathematical_optimization](https://en.wikipedia.org/wiki/Mathematical_optimization)
- [Linear Programming](https://en.wikipedia.org/wiki/Linear_programming)
- [Integer programming](https://en.wikipedia.org/wiki/Integer_programming)


## Initial observations

Consider the Capital E in an upright regular sans, such as
Helvetica, or [League
Spartan](https://www.theleagueofmoveabletype.com/league-spartan).


    (x11,y11)        (x10,y10)
      +--------------+
      |              |
      |  +-----------+
      |  |(x8,y8)    (x9,y9)
      |  |(x7,y7)
      |  +---------+(x6,y6)
      |            |
      |  +---------+
      |  |(x4,y4)   (x5,y5)
      |  |
      |  |(x3,y3)    (x2,y2)
      |  +-----------+
      |              |
      +--------------+
    (x0,y0)          (x1,y1)

We can consider the vertical and horizontal _stems_ (in the
CFF Type 2 hinting sense) as constraints.
A horizontal stem creates an equality on the y-coordinates:

- y0 = y1
- y2 = y3

(and so on)

The vertical ordering of the horizontal stems creates an ordered
chain:

- y10 > y9 > y6 > y5 > y2 > y1

In a shape with more complicated forms, maybe a chain could be
made for all points on a contour between two stems (or at least
those contours that are flat/monotonic between stems).

Ordering stems by their width (in design units) creates more
chains.
In this hypothetical **E**:


    (x11,y11)        (x10,y10)
      +--------------+
      |              |
      |              |
      |              |
      |  +-----------+
      |  |(x8,y8)    (x9,y9)
      |  |(x7,y7)
      |  +---------+(x6,y6)
      |            |
      |  +---------+
      |  |(x4,y4)   (x5,y5)
      |  |
      |  |(x3,y3)    (x2,y2)
      |  +-----------+
      |              |
      |              |
      +--------------+
    (x0,y0)          (x1,y1)

- y10-y9 > y6-y5
- y2-y1 > y6-y5

and equalities:
- y10-y9 = y2-y1


Sizing a font and setting the baseline and top-line to a grid
(or however the scale is ultimately fixed), then creates two
more constraints.

Possibly we can create a _goodness_ function.
(nearness to exact scaled points?)

Can we then express it as a convex optimisation problem?
Over a space where each coordinate of each point is an axis (so
the number of dimensions is 2n where _n_ is the number of
points).

Of course this is an _integer_ problem.
Or at least, mostly integer problem.

Can we solve the ys and the xs separately? for y then x?

# END
