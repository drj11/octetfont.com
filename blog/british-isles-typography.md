[brutal]: #title "The Typography of the British Isles"
[brutal]: #author "David Jones"

What scripts and glyphs do we need to have an adequate coverage of the
British Isles, typographically speaking?

Politically, the British Isles consist of the UK
(The United Kingdom of Great Britain & Northern Ireland) and Ireland,
various crown dependencies (such as the Isle of Man), dominions, and so on.


## Recommendation

The CubicType recommendation for an initial set of letters for Latin
script fonts is:

- 26 letters, A to Z, plus Ð Þ Æ Œ

and for diacritic marks:

- above: acute, circumflex, diaeresis, dot above, macron, grave,
  caron, ring, tilde
- below: cedilla

Strong suggestion of also adding ß.


## Summaries

A summary of the research and discussion.

The [Wikipedia page "English terms with diacritical
marks"](https://en.wikipedia.org/wiki/English_terms_with_diacritical_marks)
is surprisingly helpful.

It suggests:

- 26 letters, A to Z, plus Ð Þ Æ Œ
- accents above:
  acute (née, resumé), circumflex (entrepôt),
  diaeresis (daïs), dot (on i and j, removed when other marks are applied),
  macron (lēad for poetry marking; Māori and New Zealand English),
  breve (drŏll for poetry marking), grave (cursèd emphasis)
- accents below:
  cedilla (soupçon and Manx Gaelic)
- some punctuation (what?)

From other British Isles languages:

- eng (Welsh Romani), yogh
- caron, ring, macron

From other influential languages:

- tilde
- ß

Given this selection of letters and this selection of accents,
all letter × accent combinations that are in unicode should be supported.
In particular, wcircumflex, « ŵ », is needed in Welsh and is commonly
missing from fonts
(possibly because it appears in the Latin Extended A block in Unicode).

Once you decide what languages you want to support the Unicode CLDR may
help you decide which letters, accents, and punctuations you need.

Maybe also consider the Microsoft Character Design Standards:
https://docs.microsoft.com/en-gb/typography/develop/character-design-standards/

Not required:

- breve
- comma (below)

Although these seem like reasonable accents to consider given
how common they are.


## A sort of table

- A to Z: most. Cornish, English, Scots. Other languages use fewer letters.
- acute, grave, diaeresis, circumflex: Welsh and others.
- caron, macron: Angloromani.
- cedilla: Manx Gaelic
- ring above: Welsh Romani (in the [loanword bråmla for
  example](https://en.wikipedia.org/wiki/Welsh_Romani_language)).
- dot above: historical forms of lenition in Irish and Scottish Gaelic.
- eng: maybe Welsh Romani.
- eth, thorn, yogh: historical English, Welsh, Scots;
  possible modern revivals.


## Languages of UK

For the UK,
[Wikipedia](https://en.wikipedia.org/wiki/Languages_of_the_United_Kingdom)
notes 11 indigenous voiced languages and an additional 3 sign languages.
Of the voiced languages
8 are living languages, and
4 are extinct ones (Cornish appears in both lists).
The living voiced languages are:

- English
- Scots (and Ulster Scots)
- Welsh
- Irish
- Angloromani
- Scottish Gaelic
- Cornish
- Shelta

The extinct languages are:

- Cornish
- Norn
- Pictish
- Cumbric

Even this restricted picture is quite complicated, and it gets further
complicated by languages brought to the isles by immigration.
In the UK there are significant groups speaking, as their main language,
Polish, Punjabi, Urdu, Bengali (including Sylheti and Chittagonian
which have no written forms), Gujurati, Arabic, French, Chinese,
Portuguese, Spanish, Tamil.
I stopped copying the Wikipedia list at < 100,000 speakers


## Languages of Ireland

The living indigenous languages of Ireland are
English, Irish, Ulster Scots (scots), and Shelta.
From that perspective, it's a subset of the UK.

The immigrant languages of Ireland are similar to the UK.


## Languages of Mann

As well as having some of the languages of the UK and Ireland,
the Isle of Man adds Manx Gaelic to the list.


## Archipelago summary

I am ethically restricted from documenting Shelta any further
(it is clearly not intended to be used or investigated by non-users), but
given its origins, it seems acceptable to assume that a typography that
covers both English and Irish would also be usable by anyone wanting
to write Shelta.

It's obvious that typographically we should support Latin script, because
that supports all of the indigenous languages and many of the immigrant
languages.
After Latin, it's not so obvious what a typographer should support.
Punjabi has three scripts, Urdu two, Bengali one
(also used for dialects of Urdu), Gujurati one (a variant of Devanagari).

The general advice applies:
think about your readership and consult an expert.
It is clear that for south asian languages
the devanagari script is important;
arabic script can cover some other languages and
not just the arabic language.
Chinese has two written forms (traditional and simplified);
both are important because of immigrant groups from both
populations of users.

I had intended that this article concentrate on the Latin script, so
that is what i will do.


## Latin script

The Latin script is more than just the alphabet, the letters A to Z.

Frankly, English needs a fairly full set of diacritic marks, even
though many people try to write it without.
Imagine Waitrose trying to sell crème fraîche with the accents.
Unforgiveable.

My aim here is to reach some sort of middle ground between
“26 letters is enough” (it's not), and “all possible latin-based scripts”
(which if you include Vietnamese and African languages, is a lot).
But a middle ground that still has some sort of basis.

Welsh uses grave, diaeresis, and circumflex.

Irish uses an acute accent ("long sign"), and may use dot-above for
lenition in older texts, or styles emulating the older use.
Another fun fact about Irish, eclipsis uses non-initial capitals, and
even in an "all caps" setting,
may use lowercase for the letters added by eclipsis.

Scots Gaelic (closely related to Irish), uses a _grave_ accent for long
vowels, and older forms use dot-above for lenition.
Older systems may also
use acute accent for either "vowel quality" or as Irish.

Cornish, at least according to the
[Wikipedia page for its Standard Written
Form](https://en.wikipedia.org/wiki/Standard_Written_Form),
appears to use the latin alphabet from English, with no accents.

The
[Wikipedia page for Angloromani](https://en.wikipedia.org/wiki/Angloromani_language)
does not explicitly name a writing system,
but uses two orthographies:
one based on English, one based on East European writing systems.
The [Wikipedia page for
Romani](https://en.wikipedia.org/wiki/Romani_language#Orthography) suggests
that the orthography is usually based on the contact language
(English in England, Romanian in Romania), but that this may now be
trending towards an English and Czech-oriented orthography
(it's not clear if these are simultaneous, separate, or interchangeable).

Manx Gaelic uses cedilla.
Which is useful in English in any case
for French loanwords and words such as façade which can be spelt
with a cedilla according to style.


## Typographic and Orthographic ligatures

Short version:

Orthographic ligatures, like « æ » and « œ »,
are required becaused some people will want to use them to
spell certain words (like archæology).

Typographic ligatures, like fi and fl, _may_ be used by the typographer to
achieve certain technical or stylistic effects.


## The olde letters

Eth, thorn, yogh

Eth is in current use in the motto of the coat of arm of The Shetland Isles.
It's also used historically in Welsh (replaced by **dd** in modern Welsh).
Thorn and Yogh are used in older versions of English and Scots.


## Punctuation

Note _Tironian et_ is required for Irish and Scottish Gaelic.

Apostrophe is used in English and Welsh for contractions and
clitics (as in « ’tis » and « ’tisn’t ».
In good typography it is represented by U+2019 RIGHT SINGLE QUOTATION MARK.


## Discussion

The accent list probably includes more accents than most English speakers
are used to using.
Notably it doesn't include:

- L with stroke (Polish)
- crossed D (South Slavic languages).

These aren't used in native British Isles languages, but they will be used
by British Isles residents and also as names and loanwords.
Consider as complete a set of accents and modified base letters as
possible.

Cedilla is only used by Manx Gaelic, but will be useful to users
of English for loanwords and outré spellings of words like
façade.

Caron and macron above are used by the Slavic orthography of Anglogromani
(i'm guessing), but are also common in other languages.

The dot-above accent is only used by historical forms of
Irish and Scottish Gaelic (as far as my limited research suggests).
In most fonts it will be easy to design and place however.

The ligatures « æ » and « œ » are entirely optional in English, but
necessary as distinct letters in some other languages.
I think that makes them worth including on that basis.

Eth and thorn are used in historical writing, and
_could_ be used in modern Welsh for the graphs dd and th.
Though it is unclear how much confusion that would cause.
They are letters in Icelandic.

Yogh is used in historical writing, and
could be used in modern Scots (for some uses of Y and z).

## END
