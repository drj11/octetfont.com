[brutal]: #title "Atwin, a critique"
[brutal]: #author "David Jones"

## My notes

The brief: What is going on here? Why are you doing this?
Type is serious business, you need a better story.

Punctuation.
The commas are weak (but they are true to the original specimen).
Spacing on the quotes is too wide.

Asterisk. The form is too normal. Which for this font, is odd.

Hash/poundsign doesn't work.

Bullet: not round, not conventional. Yen: what is going on here?

A: should the spacing between leg terminals be even?

Ccedilla: what is that cedilla?

Hbar: spacing/kerning on Hbar?

I: all the accents collide, fix it.

Uumlaut: tighten the umlaut? (on previous ones too)

Spacing is extremely tight at 24pt and below.
The WX spacing is very very tight
compared to say, YZ.

RA MA AIM tight. TA tight? VE tight? AM BO VE tight
ONG consistent? OMG?

## AD notes

Weird that D is the only letter to use square corners on the
outside, and the inside of the counter.

Fix/fiddle (I think, drawn down the spring point) of the
junction of the middle stem of M (the inner vertical one).
Also M: what is going on with dog-leg curve on right hand side
of middle leg-serif?

K: inner angle of the junction of vertical stroke and upper
diagonal (arm) is too round.

N: ease-in curves on top-left and top-right of thick top
horizontal stroke.

Consider changing numbers. why are some monoline? Throw the
weight around more?
