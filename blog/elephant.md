[brutal]: #xdate "2023-10-25"
[brutal]: #author "drj"
[brutal]: #title "The Magic Elphant (and its winding number)"

This is my elephant:

<embed src="image/elephant/ElephantTrim.mov" />

It represents my first foray into the technology of
_Variable Fonts_.
Variable fonts allow a design to be varied along one or more axes. 
Typically the axes embody existing design notions such as
_weight_ moving from very light to very bold (strokes get
thicker), or
_optical size_ moving from very small target sizes to very large
target sizes (thick/thin contrast, counters, and so on, change).

But the Variable Font system is flexible enough to model more or
less any change.
For geometry, an axis specifies a diff vector for each node; as
the value along the axis changes, the diff vector is scaled and
added to the node; thus the node moves from its origin in a line
along the diff vector.
It’s actually a little more complicated and flexible than that,
but that the basic principle.

What the video shows is a glyph from a font i made as the design
varies along the ELEP axis from 0 to 1.

Some notes:

- It’s the letter **E** for Elephant. It might not be obvious
  but any letter in a font can be a drawing of anything, the
  computer doesn’t care.
- I probably should have called the axis TRNK as the visible
  strip moves from the tail to the trunk as the axis value goes
  from 0 to 1.
- What?

It looks like i’ve drawn a slice of an elephant and somehow
morphed it to look like different parts of the elephant as you
move along the design axis.
I haven’t done that at all.

It’s a magic trick using winding number.
The elephant is a white elephant.
This is a clue.

The elephant in full looks like this:

<img src="image/elephant/D.svg" />

This is conceptually similar (and homeomorphic) to the letter **O**:

<img src="image/elephant/O.svg" />

It’s conceptually similar in that the letter **O** and the
elephant have an outer contour (a near circle for the **O**, a
rectangle for the elephant), and an inner contour that defines
the _counter_ (again a near circle for the **O**, but an
elephant shape for the elephant).

The white elephant is the counter (middle bit) of the **O**.

Consider a badly drawn **Ø**<br>
<img src="image/elephant/Oslash.svg" />

I’ve drawn the slash overly thick and almost covering the
right-hand part of the counter so that it gives the impression
of a blind pulled across the counter of the **O**.
Imagine that the blind is pulled across so that only a part of
the counter on the left is shown.
Now further imagine that there is a second blind on the left
that obscures part of the left-hand part of the counter.

That’s how the elephant trick works. There is a rectangular
blind on the left that starts (at ELEP=0) entirely to the left of the
elephant but that can be extended to the right to cover more of
the elephant. There is also a rectengular blind on the right
that starts (at ELEP=0) entirely convering the elephant and at
the other end of the variable font axis (at ELEP=1) is entirely
to the right of the elephant.

At intermediate values of ELEP there is a gap between the two
blinds through which the white elephant can be seen.
This is a bit like how a mechanical shutter works on an SLR
camera (i don’t think that mechanical shutters are very common
in the 21st century).

Here’s an intermediate position of ELEP (about 0.15)

# END
