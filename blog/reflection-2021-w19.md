# 2021-W19 2021-05-13

Everything is an experiment.
Possibly with no or only a poorly-expressed hypothesis.


## Tracings

`font-asiren`

I recently found the font
[Quick/Sirena](https://fontsinuse.com/typefaces/31841/quick)
and did a trial trace of the letters making up «CubicType».

`font-atwin`

Atwin, my tracing of
[Gemini](https://fontsinuse.com/typefaces/35722/gemini-computer),
has been cleaned and the british accent section completed.

`font-andily`

Andily, my tracing of [Lydian](https://fontsinuse.com/typefaces/6/lydian)
is on pause.
The cleaning work on Atwin clearly indicates that cleaning Andily would be
useful and productive.
The trial trace of Asiren shows how similar ductus produces similar
typographic questions when tracing.
In the particular case of Asiren/Andily the similar questions are:

- terminals. Is the angle and width consistent?  Should there be a bevel /
  chamfer / blend at the corners of a terminal?
- intersections. In a + style intersection, should these be modelled as
  two strokes crossing over each other, or should i trace around the cross?
- swell and concave. Are the sides of a stroke parallel, producing a
  constant thickness, or are they slightly convex or concave,
  producing a gentle swell or necking?

Is it a good idea to a few different tracings of some key letters to decide
these things early on?


## CubicType originals

`font-airbed`

A very limited trial of digitising, by hand–eye,
a few letters (D O M) of a sketch alphabet originally drawn in pencil.
It shows:

- the method works;
- the form is interesting enough to develop.

`font-acacounter`

This is paused for now.
It should be revisited in light of the `akern` learnings.
I don't love the name.

`font-akern`

The most useful recent project.
The idea was to create from scratch a font following the principles of
Karen Cheng's book «Designing Type».

_Designing_ takes a very mainstream approach.
Which isn't very exciting,
but is the bedrock on which i need to rest my typographical education.
It's essentially “how to draw the 6 most boring sorts of typefaces”.

So far I have a draft upper case alphabet (A to Z plus Thorn).

This shows:

- Cheng's book is a useful guide and crutch, providing a practical system
  of evolution and pragmatic advice.
  Start with O and E.
  This stroke should be thick, this one thin.
- I can draw type.
- I have an eye for balance and form; this needs developing.
- The practice of making a more-or-less mainstream font builds my
  understanding of form and value, which informs both fonts close to that
  form but also more distant ones (like Asiren and Acacounter).
- I think i may have a sort of fear of the lower case :).


## Portfolio

about.cubictype.com

I haven't paid much attention to this since failing to get an AIGA NY
mentorship, but i think it continues to be a reasonable to organise my
thoughts on my work so far.

It should be ever more visual.
I quite like the now-consistent application of colour for each section;
this was a relatively recent change that uses green-on-cream for Ranalina.
Other sections only have one sample per section, but
if they are expanded (Atwin needs expanding) then i should pick
and use a consistent colour pallette.

# END
