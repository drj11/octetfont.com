[brutal]: #title "Apostrophe"
[brutal]: #author "David Jones"

Apostrophe is the 27th letter of the English alphabet.
It doesn't appear in primary school alphabet posters,
but it's very common in words, appearing about as frequently as q
which always appears on an alphabet poster.

In Unicode it appears as U+0027 APOSTROPHE,
a character that does double duty as the apostrophe in words
like `women's` and the so-called 'single' quote.

Typographically it is a disaster, and most typographers join me
in recommending that U+2019 RIGHT SINGLE QUOTATION MARK is used
where the apostrophe is required.
U+2019 RIGHT SINGLE QUOTATION MARK should be used for
all apostrophes in English and other languages using Latin orthography.

U+2019 RIGHT SINGLE QUOTATION MARK should look like a raised comma
(a little 9).

Matching U+2019 we have U+2018 LEFT SINGLE QUOTATION MARK,
which should look like an inverted (and raised) comma:
a little 6.

English writers in the UK and North America use
DOUBLE QUOTATION MARK for quoted text,
in Unicode they are U+201C LEFT DOUBLE QUOTATION MARK and
U+201D RIGHT DOUBLE QUOTATION MARK.
They are doubled-up version of the SINGLE QUOTATION MARK and
look like little 66's (LEFT) and little 99's (RIGHT).

Referring to [BRINGHURST1996] who documents some of the
variation in quotation marks.

German typographers use low 99s to open and raised 66s to close.
In Unicode that's U+201E DOUBLE LOW-9 QUOTATION MARK and
U+201C LEFT DOUBLE QUOTATION MARK.
Note that LEFT DOUBLE QUOTATION MARK is used as an _opening mark_ in
English writing, but a _closing mark_ in German writing.

French and Italian typographers use _guillemets_, usually with a
<a href="unicode-space.html">thin space</a>.
In Unicode these are U+00AB LEFT-POINTING DOUBLE ANGLE QUOTATION MARK
and U+00BB RIGHT-POINTING DOUBLE ANGLE QUOTATION MARK.
Unicode inherited them from ISO 8859-1 and they are Unicode's
numerically least matching typographical quotation marks;
which is why I prefer them (despite writing in English).

Bringhurst claims that German typographers swap the guillements
so that the opening mark points right and the closing points left.

For putting quotes inside quotes, there are also single-angle
versions of the guillmets, which Unicode predictably calls
U+2039 SINGLE LEFT-POINTING ANGLE QUOTATION MARK and
U+203A SINGLE RIGHT-POINTING ANGLE QUOTATION MARK.


