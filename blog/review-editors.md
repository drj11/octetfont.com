[brutal]: #date "2023-04-26"
[brutal]: #author "David Jones"
[brutal]: #title "Review of Six Text Editors"

- [`tine`](https://github.com/deadpixi/tine)
- [`nano`](https://www.nano-editor.org/)
- `pico`
- [`micro`](https://micro-editor.github.io/)
- `mcedit`
- [`ne`](https://github.com/vigna/ne)

When I say _text editors_ i mean text editors in the
tradition of `ed`/`vi`/`edlin`:
Editors used in the terminal suitable for editing code / text.
For now, i'm not considering the other sort of editor, the GUI
editor (like Notepad or VS Code).

<img src="image/HelpKey.jpeg" style="width:88vw"
alt="Close-up of help and home keys" />

My day-to-day editor is `nvim` (Neovim, a descendent of `vi`),
but i've used a few weird editors, and i used `/bin/ed` and
`emacs` for long enough to be fluent in those too.

I spent 6 consecutive weeks trying 6 different editors;
each for 1 week.

It is clear from the notes below that most of the editors had
some niggle or other: either installing, or colours, or
use of the mouse, or function keys or arrow keys.
I'm using `kitty` on macOS and these editors are probably
native to Linux.
In the sense that they mostly get developed and used on Linux.
Possibly these niggles go away in Linux world.
Possibly these niggles can be "fixed" by suitable environment
variables, configuration variables, and other spells that take
longer than a week to learn.
Your mileage may vary.


## [`tine`](https://github.com/deadpixi/tine)

This is without a doubt the weird one of the bunch.
It's a 21st century recreation of `ED` from AmigaDOS/TRIPOS.
A friend suggested i check it out.

From git, and compile using `make`.

It is... quirky, dusty, and with a faintly Cambridge Computer
Lab aura.

I am trying to use it while barely having skimmed the man page.
You can't delete a line by "deleting the newline character".
Because its text model isn't "a file is a stream of bytes" model
that is one of the cornerstones of Unix.

The model is more like "a text file is a sequence of records".
While this seems odd from a modern perspective,
`tine` shares heritage with editors that i've used (at Cambridge)
that treated lines very separately from characters.
So, while i've used the Unix-model of "a file is a stream of bytes"
for long enough that this is weird,
i've also used enough record-based systems that i get it.

The "can't delete the newline" quirk is related to the
"you _can_ move the cursor off the end of a line" quirk.
The lines don't seem to end, you can just keep going to the
right "off the end".

By the way you can join lines with Ctrl-N (or Esc J Ret).
And pressing Esc to enter a command mode is sufficiently vi-like that
i was successfully able to quit the first time i used it without having
read the manual.

I miss just pressing `/` to start a search (like `vi`), but
i still like search enough that i used `Esc BF/something Ret`
to find something.

Some commands (Ctrl-E and Ctrl-]) have a "self flipping" behaviour:
go to end, but if there, go to beginning.
I quite like that, and it compacts the keyboard-command mapping
by avoiding 2 separate commands.

The status line is fittingly minimal.
I appreciate a compact status line, probably because
the first versions of `vi` that i used didn't display much
on the bottom line.

`tine` displays the portion beyond the end of the file as a
grey/checkerboard area.
Initially i found this a bit offputting, but you quickly get used to it.

Overall i like it. it combines the friendliness of `nano`
with the hostility of `/bin/ed` and has a command language
reminiscent of the editors i used in the early 1990s.

I've used it for some code, initially without autoindent because
i couldn't be bothered to read the manual.
I was using Go and i tend to type in a somewhat chaotic style
and leave the formatting to `go fmt`, so the lack of autoindent
was fine.

Switch on autoindent with `Esc AI Ret` by the way.

Worth checking out if you like quirky editors and you appreciate
minimalist designs.


## [`nano`](https://www.nano-editor.org/)

`nano` was created as a clone of `pico`,
because at the time the creators of `nano` didn't like the
license for `pico`.
Now `nano` is a slightly grander project with ambitions of its
own (and `pico` switched to APL).

I like the simplicity of `nano`.
I recommend it for a variety of "beginning code" courses
where the students may not have previously used a text editor.
That’s because it’s reasonably easy to use, and it is likely
to be already installed.

One of the features that i like is that it tells you
that "exit" is  `^X` in the shortcut area at the bottom.

But that brings me to a thing that i don't like:

- the space taken up by the shortcut area at the bottom.

`nano` is pretty wasteful with screen area (because `pico` was).
Of the 25 lines in my display, 5 are not available for editing text:

- 2 lines at the top: one for a title (editor name, file name, modified state)
  and one for a permanently blank line below it (!);
- 3 lines at the bottom: 2 of which are a memo for the shortcuts, and
  above those 2 lines a status line that is blank most of the time.

The blank line below the title is just wasteful, remove it.
The line above the shortcuts is used as a sort of temporary message area
(for example,
press `^C` and you get a report of the current cursor position).
I think you could probably combine that and the title line into a
single status line.
You would probably have to get rid of the "GNU nano 2.0.6" that is
permanently displayed.
Although, maybe permanently displaying
the editor name and version is useful?
How else would i discover that `nano` supplied by Apple
dates from 2006 (the license changed to GPL 3 shortly after,
so maybe that explains why)?

I think 3 lines of meta/status would be possible: 2 for shortcuts, and
1 for status/title.

With effort it would be possible to reduce the shortcuts to 1 line
(they currently include fairly obscure shortcuts like `^J
Justify` and
`^R Read File` as well as `^Y Prev Page` and `^N Next Page` when in 2023
`PageUp` and `PageDown` probably work).
Put the cursor position on the status line and
you can get rid of `^C Cur Pos` too.

I suppose `^J Justify` and `^R Read File` make more sense it the context
of an editor that was used for an e-mail system in the 1990s, where
both of those functions would be useful.
But i live in 2023.

Also, i brag about `PageDown` working, but Ctrl-arrow doesn’t work
for me, so Next/Previous-Word doesn’t work. *sigh*

Function keys are good, and i appreciate that nano has presets for them.


## [`pico`](https://alpineapp.email/)

Well, score minus several million because i had to find _Alpine_
source tar balls and compile from source.
But that didn't work until i had "fixed" the `configure` script
on macOS to remove all the `APPLE_KEYCHAIN` stuff.

It's been a long while since i've used `pico`
(i used to use the e-mail system `pine` at uni).
Everything i said about `nano` probably applies to `pico`.
Because of their shared heritage
(`nano` was created as a clone of `pico`), they are very similar.

The wasteful space at top and bottom that i was complaining about in 
`nano` is basically an exact copy from `pico`.

While the documentation claims that it uses F3, it
does not in fact work.
At least Ctrl-arrow works (unlike `nano`).

Lack of Lisp mode or bracket matching makes my
attempts to program in Racket challenging.
Like the 1990s.
Although, i did program a bit of Lisp in the 1990s and i used
SunOS `vi`, which did have just enough of a Lisp mode.

`pico` automatically word wraps (at something reassuringly short like
72 columns).
This makes using it for programming challenging.
It turns out that you can switch this off,
with a _command line option_ only (`pico -w`).
It's one of the reminders that `nano` isn't just a clone,
it is more popular and has more eyes looking at it, and more
hands working on it.
Compared to `pico`, the fit and finish of `nano` is
just a little bit better.

Most people would be better off choosing `nano` over `pico`,
unless they are particularly allergic to GPL
(`pico` uses Apache License 2.0).


## [`micro`](https://micro-editor.github.io/)

`micro` continues the trend of editors named after prefixes in
the SI/Metric system, 
each a thousand times larger than the previous:

- pico 10⁻¹²
- nano 10⁻⁹
- micro 10⁻⁶

I had to download `micro` as a tarfile and install by hand.
Charming (in both non-ironic and sarcastic senses).
It's implemented in Go and distributed as a single binary.
I appreciate that.

The default colour scheme is annoying, but
i discover how to switch it off.
After that, the default default colour scheme
is actually quite nice.
The background is not quite black,
and the current line is subtlely highlighted with a grey
that is just a shade lighter.

Line numbers are on the left in a dimmer shade.
Clear enough to read if you need them, but not distracting, and
different enough that they are clearly not the text.

Matching bracket highlighting is automatic and uses just an underline;
it's a bit quirky, but you get used to it and
i like it (it is very handy when i'm editing Racket).

Mouse support is on by default; i appreciate that
trackpad scrolling scrolls the viewport without changing the line
that the cursor is on.

There is less information about this editor on the web than
some of the other editors, so sometimes finding shortcut keys
and so on was a little bit more involved.

I didn't have much to say about `micro`;
perhaps that's a sign of how unassuming it is.

If i was looking to switch editors,
i would seriously consider `micro`.
The only drawback for newcomers is the currently awkward install
experience.


## `mcedit`

`mcedit` is the editor bundled with Midnight Commander, which
is a recreation/homage to Norton Commander, a paradigmatic
[Orthodox File
Manager](https://en.wikipedia.org/wiki/File_manager#Orthodox_file_managers) originating in 1980s DOS systems.

It is easy enough to install using

    brew install mc

The colour sceme is slightly awful.
It's general look-and-feel is sophisticated DOS program (deliberately).

i discover that pressing Esc Left-arrow instantly quits the
editor (it looks like a crash, but no bad error code).
But i also discover it restarts exactly where i left off.
Still, that's weird.
Later i discover that this is activating the quit editor command;
probably by mis-parsing the input keys.

Status bar at the top, F-keys at the bottom.
It's a very compact and informative arrangement.
The status bar is perhaps a little bit overwhelming for first-time users.

The awful colour scheme is awful.
The default of white on blue is acceptable, and
very reminiscent of DOS-era Turbo Pascal 2.0.
I suspect on my macOS / kitty setup the blue is lighter than intended
(screenshots on Linux show a much darker blue, more like CGA text mode
dark blue).

But the syntax highlighting is unacceptable.
Section headings in markdown are rendered using red making them
almost unreadable and this user's setup;
i had to, again, disable syntax highlighting:

    : > ~/.local/share/mc/syntax/Syntax

That still doesn't fix the wrong tint of blue.

I don't have any more notes, which i guess means that i spent
the week without too many frustrations.

If you like DOS-era interfaces, you'll definitely like `mcedit`.


## [`ne`](https://github.com/vigna/ne)

    brew install ne

Confusingly for me, `ne` happens to be the same name as `New E`.
`New E` was Phil Hazel's new version of `E`;
`E` being the preferred full screen editor for the IBM 3084Q system
affectionately known as PHOENIX.

The default colour scheme is okay, although i wonder how it would
work on a more mac-like white terminal.

There are significant redraw bugs; they
don't prevent me from seeing the text, but they do continue to
plague the week's editing experience..

I was able to quit without reading any documentation by
virtue of the fact that pressing ESC shows, after a small pause, the menus.
Which are rather DOS/CUA like, without actually being very
[CUA compliant](https://en.wikipedia.org/wiki/IBM_Common_User_Access).

Sharing the status bar with the menu is quite a nice
space-saving feature.

I have even fewer notes for this editor, perhaps
a combination of: a bit of Week 6 weariness;
`ne` being fairly non-descript;
the screen update bugs being a bit tiresome.


## Overall thoughts

Can we please put a little bit of effort into making the mouse,
function keys, and extra keys (like PageUp and arrows) work?
They are welcome features and nice when they work, and
they do work on modern termnals (or should).
We don't have to pretend we're using an ADM-3A any more.

### On colour

For colour the main problem is a mix of two things:
there isn't a way for a program to know whether
the terminal background colour is black or white
(or some near-black or near-white colour or something else); and,
"default" colour palettes vary, but
most significantly between Linux and macOS.

This is not a theoretical concern, the colour schemes i have observed
mostly vary from unusable to lurid but usable.

First impressions matter, and if my first impressions are an
unreadable colour scheme that i then have to spend 20 minutes looking
on the web for disable codes then that's bad.

My recommendation would be to do something conservative that
is likely to work and avoids colour.
In the 21st century we may not be able to use colour reliably, but
we can use: bright/reverse-video/underline.
Anyone wanting colour should be able to find documentation and
enable it.
And those colour themes should say whether they are dark mode or not.

Perhaps something like CSS's `prefers-color-scheme` (aka "Dark
Mode" support) could be implemented in terminals:
a way to let the app know the preferred colour scheme, or
an abstract palette.

### Mousey

I don't start out as an expert in using your editor, so
being able to use the mouse (and arrow keys!) to navigate
is quite welcome.
While i personally appreciate the `vi` approach of having 42
different motion commands, i also appreciate how user-hostile
that approach is.

The chief problem here is that enabling mouse support invariably
disables the terminal's mouse cut-and-paste, so now i have to learn
an editor-specific shortcut for using the true clipboard.

There is some variation in scrollwheel/trackpad behaviour:

- neovim: moves cursor (with implied scrolling at window edges)
- micro: scroll window, cursor pinned to document
- mcedit: scroll window, cursor pinned to screen

I suspect that `neovim` is the _naive_ mode: i have disable `neovim` mouse
support, so the terminal is sending default key codes for scrolling
which turns out to be the up/down arrow keys.
Which on my mac are reversed in honour of Steve Jobs.

(Now that i have used a few editors where mouse support works well,
i may remove the mouse disable code from `neovim`.)

The good thing about mouse scroll is that the actual behavious is
instantly comprehensible, even if you don't particularly like it.

I prefer the `micro` implementation: it separates the action of
scrolling the viewport from the point-and-click action of
moving the editing cursor.

## Closing notes

I'm encouraged by the diversity and
health of the text-editor-in-terminal ecosystem.
It did not seem at all plausible when i was learning `vi` in 1992
that those skills would be useful 30 years later.
And that many editors from that era would be available and still
actively used and have been updated to stay current.
And joined by new competitors with clean implementations and
interesting ideas.

Of course, many of you are using VS Code and that is fine.

Try a new editor today!

# END
