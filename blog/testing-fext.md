[brutal]: #title "Testing a translator, fext"
[brutal]: #author "David Jones"

`fext` is a translator from the compiled form for Open Type
features to the AFDKO syntax.
It is, in effect, a reverse-compiler.

Normally a font-engineer would write features in AFDKO feature
file syntax and use `makeotf` to compile them into a binary
font-file.
Or an end-user tool would do the equivalent,
possibly adding features of its own.

There is at least one other feature translation system and
it is `ttx` which translates an entire font-file in- and out- of an
XML format.
The XML format is both fairly close to the binary font-file, and
sort of editable.

None of that is relevant to this discussion, except to lay a bit
of context for the sort of translation that `fext` does.


## Testing other translators, like `ttf8`

`ttf8` is a font compiler: it translates from SVG to TTF.


## Artefacts?

Some of the tests involve a particular artefact.
Where is a good place to put these?
Some of them are clearly test fixtures,
small files or bundles of data.
Those can go alongside the tests.

Others though are larger.
For example a complete font in SVG form.
These artefacts might be shared across multiple projects or
have a life of their own (it can be quite useful to use real-world
materials in a test).


## How do we test a translator?

Consider the following output from `fext` (this is a real, but
fairly small example):

```
; fext ../font-antscap/out.otf
# GSUB,version, 1 , 0
languagesystem DFLT dflt;

feature liga { # LU0 LU1 LU2 LU3 LU4 LU5 LU6
  lookup L1 { #T6(F0)[1]
    sub [I]' [N N.bl N.bltr N.tr] by [I.br];
  } L1 ;
  lookup L2 { #T6(F0)[1]
    sub [G K]' [I M N S N.bl N.bltr N.tr] by [G.br K.brtr];
  } L2 ;
  lookup L3 { #T6(F0)[2]
    sub [N W N.bl]' [S] [B D P S T W S.rev] by [N W N.bl];
    sub [N W N.bl]' [B D P S T W S.rev] by [N.tr W.tltr N.bltr];
  } L3 ;
  lookup L4 { #T6(F0)[1]
    sub [R Z G.br I.br K.brtr S.rev] [N N.tr]' by [N.bl N.bltr];
  } L4 ;
  lookup L5 { #T6(F0)[1]
    sub [S N.bltr W.tltr N.tr] [S]' by [S.rev];
  } L5 ;
  lookup L6 { #T6(F0)[1]
    sub [C E F S T X N.bltr K.brtr W.tltr N.tr] [W]' by [W.tltr];
  } L6 ;
  lookup L7 { #T6(F0)[4]
    sub [S]' [S] [W] by [S.rev];
    sub [S]' [I] by [S.rev];
    sub [R Z G.br I.br K.brtr S.rev] [I]' by [I.bl];
    sub [G]' [Y] by [G.tr];
  } L7 ;
} liga ;
```

In order to test `fext` in a reasonably black box way,
we would feed various font-files as input and observe the
output.

Consider the first line of the output above, `# GSUB,version, 1 , 0`.
This is essentially some debug output from `fext` in the form of
a comment.
It's reporting the version of the `GSUB` table used in the font.
Now, this is not essential information, being a comment it won't
affect any attempt to use the feature file.

Should the tests test that `fext` puts this comment in?
Each feature and lookup rule also has a comment with some
debug information in.

It might be attractive to run `fext` and
store the output as an _exemplar_ against which
a test run can be compared.
I think sometimes that will be a good idea, but 
if `fext` changes its output in a "harmless" way the test will
fail.
Changing a comment or the indentation amount would trigger such
a failure; these are probably harmless change, but i'll describe
more meaningful changes later.
Someone making or approving such a "harmless" change would have
to check all of the test exemplars and make new ones.

Instead of using an exemplar for an exact match, we could use it
to check partial matching using regular expressions perhaps.
Perhaps.
Ultimately if take this approach we have a system where we have
two outputs, one a supposedly correct exemplar and the other the
test-run output, and we are checking that they are in some sense
snytactically the same.
To do this well and do it robustly sounds like… a translation
problem!

In practical terms it may well be that checking a match against
an exemplar or some partial implementation of some sort of
translation system (tokenising and checking token streams for
example) will be good enough.
Many projects will have islands of stability in their life where
the "harmless" parts of the output don't change, so _in
practice_ checking the output for an exact match may be good
enough.

Apart from comments and whitespace, the feature file syntax has
several other places where there are choices,
and a reasonable output might vary.

First up is choice of Lookup names. 
In the above output the lookup rules are named L1 L2 through to L7.
These are entirely arbitrary names.
They appear in the AFDKO
feature file source but not in the binary font-file.
So `fext` which translates from the binary font-file to the
feature file syntax cannot know what lookup names were
originally used.
It would seem wrong to bake the exact choice of lookup names
that `fext` chooses into a test.
A future version of `fext` could choose different names and it
would be entirely reasonable.

Next up is groups.
The things in square brackets are groups of glyphs.
In AFDKO feature file syntax you can name your groups.
For example `@tropt = [N N.bl W];`.
Having named a group you can then use that name in a lookup rule.
This one features in lookup L3 above.

The current version of `fext` can "see" the glyph groups,
but again the names of groups do not appear in the binary
font-file.
`fext` has no way of knowing if a particular group was specified
using a literal or using a named group.
Additionally, in the feature file syntax
groups can "contain" other groups, but
when the binary font-file is compiled all groups are flattened so
the structure is lost.

In principle `fext` can see when some groups are shared, which
is when they use the same offset in the binary, but the current
version ignores this and flattens all groups.

Clearly a future `fext` would be "better" if it did _something_
better with named groups, but again how to test this?
Some sort of glyph group equivalence testing?

Both of those put a dampener on the idea that you could have an
original source feature file, compile a binary font-file, and
use `fext` to recreate a feature file.
You will recreate a feature file but there are many ways that it
will differ from the original source feature file.
Again, this is a feature-file to feature-file comparison
project.


## There and back again


Symbolically _F_, `fext`, compiles from `.otf` to `.fea`, and _B_,
`makeotf`, compiles from `.fea` to `.otf`.

We have considered thing.otf -F-> thing.fea compared against
exemplar-thing.fea (exactly, fuzzily, using some unspecified
magic).
And we've considered thing.fea -B-> thing.otf -F->
testrun-thing.fea compared against the original thing.fea.
This one also introduces a dependendency on B, although it's a
reasonable assumption that a fair amount of anyone's work in
this space will already involve running `makeotf`.

A further idea might be thing.otf -F-> thing.fea -B->
testrun-thing.otf. To recreate the _binary font-file_ given
the feature file that `fext` recovered.

Comparing binaries is a bit annoying.
It is unlikely that the binaries will be identical or that we could
reasonable expect that.
But perhaps we could use `ttx` on both the binaries to convert
them to XML and then extract the relevant parts of the XML for
comparison. Perhaps.

I suspect that the current `fext` would foul such a testing
system because it flattens all groups, and i suspect that
`makeotf` does not attempt any sort of intelligent group
sharing.
But it's possible that `fext` could be improved to the point
where the choices it makes in representing glyph groups would
lead `makeotf` to compile the groups into the new binary in the same
way as the original binary.
Or so as to make the comparison feasible.


## Semantics

So far we've looked at essentially textual comparisons.
Comparing either the text of the AFDKO feature syntax or the
"text" of the binaries (possibly by treating them with `ttx`
first).

Another, and in my opinion more honest, approach would be to
test the operation of the font-file.

The harfbuzz project has the tools `hb-view` and `hb-shape`.
`hb-view` is great for making PNGs, but comparing PNGs is
annoying (and has some of the problems outlined here for
translators. When are two PNG files "the same"? not when the
binaries are identical, because they may have the same pixels
compressed in different ways).
But `hb-shape` is the result of the _shaper_, it's essentially a
list of glyphs and positions _after_ they have come out of the
OpenType rule engine.

So now we could use `hb-shape` to compare two binary font-files,
`thing.otf` and `testrun-thing.otf` which was made by
compiling the output of `fext` back into a binary.
You would take a piece of text, like _IRONWORKS_, and
a set of features to enable and disbale, run the text through `hb-shape`
using both binaries and compare the output.
I quite like this idea. But.

We can probably choose trial texts by hand in a reasonable
number of cases.
And that may in fact be a good idea for testing certain _fonts_.
Testing that a _font_ has a particular behavious and using
`fext` to check that might indirectly test various parts of `fext`.
But a comprehensive test of `fext` would check its output in
this way for _any font_.
The list of trial texts to use is a bit tricky.
Cnosider using the text `shuffle` as a test-probe.
This is a useful test of a font's `ff` or `ffl` ligature,
but if the font does not include a feature lookup for
the `ff` or `ffl` ligature then it's not really testing much.
It would be better to have some way of compiling a list of trial texts
for each font. Which means…

Extracting a list of trial texts by analysing the lookup rules
in the OpenType features in the compiled binary font-file.
Which is sort of what `fext` itself does!
To avoid accusation of "marking ones own homework" we would have
to build a separate system to extract trial texts.
Perhaps it is simpler than a full translation system,
perhaps it could use the XML form provided by `ttx`.


## Translating testing translators

I think a lot of the problems we have here are common to
translators of any sort, and so is some of the approaches.

I already mentioned PNG. Two PNG files can have identical pixels
but different byte streams (even before the compression step).

Almost any compressed file format (ZIP and so on) can compress
an identical bytestream in different ways.

You might not think of these as translation problems, but
they can be.
Making a PNG file is essentially translating from a rectangle of
pixels (and other metadata which i am ignoring for now) to a
sequence of bytes.
Displaying a PNG file is translating from
a sequence of bytes to a rectangle of pixels.
In the case of PNG at least one side of this is fixed.
Considering an image as a rectangle of pixels
it really is reasonable to compile that
to PNG and back to pixels and get exactly the same pixels.

Simiarly ZIP is a translation problem, from a typical bytestream
to a usually smaller bytestream.
Again, it is reasonable to be
able to recovered the exact decompressed bytestream.

Another example back in my font-engineering world is `ttf8`,
which translates from SVG to TTF font-files
(in this sense it performs some of the sames tasks as makeotf).
The reverse tool is, in some sense, `hb-view`;
which takes a TTF font-file and outputs SVG.
`glox` is also a TTF to SVG tool.

# END
