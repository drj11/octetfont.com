# 2021-W40 2021-10-07

It's been a while.


## TypeWknd

I withdrew. Also didn't attend any of the content.


## Linocuts!

Have done
- two-colour ladybeetle
- two-colour rose
- Cartref Newydd
- Calcifer

They're quite good.


## Lino fonting

Have opened discussion with AL about her collagraph font.


## ILT

Had a useful discussion with ILT about joining them. Which would
be great. October/Novemeber timescale.


## Book design

Agreed to do book design for dad's poetry collection.


## FWB

Started the `fwb`, Font Work Bench, as an attempt to coherently
document some diverse tools.


## Font Panels

Designed Font Panels which is a simple collection of fonts.

The more useful part was actually collecting font files into
~/f/panel-*/ directories.

The fonts from the MS Works and Corel Draw install CDs are
incredible useful. A mixture of workable knockoffs or proper but
merely old TTF files.

Really useful for quickly answering questions of the form,
"What does a typical section sign look like?"


## Fonts

Several experimental fonts with only a few glyphs:
`font-252` made from 3 horizontal lines that form oscilloscope
traces, running continuously across the whole line of text.
`font-zebra` and `font-giraffe` ideas based on animal patterns.
`font-abeam` based on a distorted square/oval inspired by very
old CRT TVs.

`font-antscap`

an alphabet of 19 letters. Based around the idea of tape looping
around capstans. Intended for use with AFDKO features.

`Acacounter`

Considering changing name to Avi (anagram of via).

Good progress recently.
Some currency symbols and some tilde letters to do.


`font-ao`

Full alphabet with numbers. Candidate for Future Fonts?


`font-auncial`

No progress.

## END
