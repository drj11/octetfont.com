[brutal]: #title "Twentieth-Century Type, a review"
[brutal]: #author "David Jones"

1992 edition (I have the later remix as well)

## Introduction

The introduction sets out the pland and argument of the book.
A skeleton overview of the typography movement in the 20th
century, necessarily focussing on the latin scripts, in
chronological order, and on the main trunk and branches of the
movements, without getting lost in the leaves and the flowers.

The book itself appears to be excellent made and the book design
is good. Although set in Gill Sans Light which is an unusual
choice for a body font, it appears to be readable enough.

## 1890

starting here because of invention of mechanical typesetting
(monotype and linotype). already using stereotype (moulding a
copy). Fascinating illustrations of pantographs. Excellent use
of margins.

Linotype considered unsuitable for high-quality work in europe,
which continued to be set by hand. Monotype desirable because it
produced type as if set by hand. Concern over decline in quality
of typography (haha), with Morris in particular showing what was
possible if you had unlimited time and money.

Rise of ATF (a union!) as a reaction to Linotype. Rampant copying.
ATF promoting the ideas of a typeface _family_, several sizes
and weights.

Rise of french Art Nouveau poster movement. Lithography,
lettering in opposition to reusable type.

Akzidenz Grotesque published. incredible.

## 1900

Arts and Craft (Morris) continues its virtuous production of
highly crafter beautiful books, in opposition to the declining
standards of commercial print. Rise of typographer (and art
director) as a separate profession. Establishment of the
Wiener Werkstätte.

Franklin Gothic (Benton) endures to this day.

Point system (incompatible between America and Britain and
different from Europe) established.

## 1910

Cubism giving rise to Italian Futurism, Russian Futurists,
links to British Vorticism. Moderisme in Spain. All divergent.

Commercially, Linotype and Monotype continue to improve.
Monotype issue first typeface designed for mechcnical
typesetting: Imprint.

AIGA.

Johnston's sans for The Underground, “the first truly
twentieth-century sans serif".

G o u d y

## 1920

BAUHAUS

El Lissitsky, Aleksandr Rodchenko.

Futura. Die Neue Typographie, Jan Tschichold.

The quotes are in Gill Sans Bold Italic. Which is a pretty
terrible choice, although tolerable enough for short quotes. One
of the first quotes is part of Morrison's “First Principles of
Typography”. This is surely a hideous joke?

Gill Sans; Perpetua.

## 1930

Times New Roman.

Nazis arrest Tschichold; close Bauhaus.

Advertising.

As in the 1920s, a continued search for the reductive alphabet.

## 1940

W A R

«The New Typography»

## 1950

Zapf produces Palatino, Melior, and Optima.
Everyone else follows Swiss International Style.

Helvetica. Univers.

Phototypesetting and Letraset's (wet) transfer lettering just sneak in
into this decade.

## 1960

Phototypesetting has come to ruin hot metal typography.
Scalable type destroys the subtleties of weight, form, and
counter that were possible when different sizes were cut
separately.
Ligatures largely gone, because f and i can be overlayed
photographically.
The skilled compositor has less role to play, and their mastery
of spacing, hypenation and justification, is diminished.

IBM Selectric; Letraset _dry_ transfer.
