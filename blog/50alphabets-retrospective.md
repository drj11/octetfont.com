[brutal]: #xdate "2023-03-23"
[brutal]: #author "drj"
[brutal]: #title "50 alphabets, a retrospective"

I had the idea to do 50 alphabets in the year when i was 50.

How did i do? Well, i didn't get to 50, i got to about 25.
I knew 50 was going to be a challenge (and also somewhat arbitrary) so
i'm not too disappointed.

One of the things that slowed down the number a lot was...
signing up to a Font Design course and making a Full Font.
That font is [Arhinni](https://drj11.gitlab.io/microsite-arhinni/arhinni.html).

I counted Arhinni as 2½ alphabets, though in effort and scope it is much more.
It has capitals and little letters for Latin.
The base “alphabet” has about 36 letters
(that is, including /ae /germandbls /schwa /ezh and so on), and
about 18 or so composable accent marks.
Two sets of numbers (lining and lower case), subscripts, superscripts, and
vulgar fractions.
12 alternative decorative capitals.
26 astronomical and alchemical symbols.
Everything has been spaced, but not yet kerned
(neither were required to count under my rules).

That said, even counting more bonus points for Arhinni would not
push the count up to 50, nor would any reasonable estimate of
activity that i could have done instead of Arhinni.

At least one project was started and not completed by birthday, Arksign.
It is a complete alphabet now, although still requires work, like
many of the projects here.

A few sets of punctations and astronomical symbols have not
been counted at all, but it is a trivial amount.

## Trends

A few common activities were noticeable (and mostly intended):

- add numbers/lowercase/capitals to a font that didn't have them yet
- extend idea from a sketch of a few letters to a whole alphabet
- trace and extend to alphabet found or sketched letter samples
- clone someone else's design
- novel designs

Cloning was obviously the most problematic of these, because
it’s not obvious that merely tracing someone else’s work should count.
I let them count because part of the point of the exercise was to
iterate through alphabets to see as many as possible.
In the end i only did three (Adone, Avend, and the pixel font Pellucida),
which meant that i did not have to confront the issue of
“Have i just done too many poor quality rip-offs?”.

The clones were probably the easiest and fastest to do.

Extending the repertoire of an existing font was the next easiest.
Adding numbers to an already drawn alphabet turns out to not be
not too stressful.
Perhaps because many font-wide aspects are already decided:
weight, stroke style, proportions of x-height and width.

Tracing found letters (that is, someone else’s design) is about
as much effort as a novel design (born digital or traced).
You get some help because there are literal letters and you do not
have to design them.
But there are also hindrances. One is that to complete the alphabet you have
to fill in the missing gaps and so infer the design system.
Another is that poor capture (scanning, reproduction) means that
many details are blurred and have to be inferred or simply made up.
Is there supposed to be a tiny slope here or is the scan made at an angle?
Should be corners be sharp or have tiny chamfers or bevels?
Are these two strokes or counters supposed to be exactly the same size,
or merely nearly the same size?

When working on my own designs i can simply decide, i don’t have to try
and guess or second-guess someone else’s intent.

## Quality and futures

The quality is very variable.
There are some okay ideas and some good ideas, and execution varies
from poor to reasonable.
Time is a limited resource and i expect many of these designs will
stay as they are while others yet to be made will overtake them
in terms of finishedness.

It is fair to say that i do not like serifs much and i prefer
graphic designs.
I don't dislike serifs, it's just clear from the fonts that i have
chosen to work on that most of them are sans serif.
Only Arhinni and Aromech really count as designs with serifs.

# END