[brutal]: #title "The Elements of Typographical Style, a review"
[brutal]: #date "2020-10-18"
[brutal]: #author "David Jones"

The book is _The Elements of Typographical Style_ by Robert Bringhurst.
I have the second edition.

I bought this book in 1999 soon after it was published, but
didn't give it a thorough read through until this year.
I have only dipped in and out of it.

In recognition of its contribution to our field,
I have taken to calling the book _Elements_ for short.

_Elements_ is undoubtedly fundamental and required reading
for all aspiring typographers.
The material is excellent, and the advice is clear and brightly
illustrated with on-point, and occasionally witty, examples.
The book design and typography of the book itself is outstanding
and makes a crafty example of many of the good points the book
outlines.

In contrast to _Manual of Typography_, Elements takes a
more academically rigorous approach;
grounded in practice, but avoiding practicalities.
You will not find lessons in how to use _Fontographer_ here.
And this is one of the small theses of the book:
That good typography and good typographical style is timeless.

What then, are the elements of typographic style?

Like _Manual of Typography_ the primary subject of _Elements_ is
book design.
Newspapers, magazines, posters, and other forms, do get a look in,
but the examples and the advice are most coherent and cogent
when they appear in the context of books.

The elements are typefaces and fonts, letters, spaces, ink, and
the relationship between all these parts.

The chapters deal with some of these elements separately and
some jointly.
Never so isolated that you are left wondering over some abstract
point, but always leaving some connecting context.
While the linear order is reasonable,
after the first couple of chapters, it would be okay to read the
chapters in whatever order you fancies, or certainly to revisit
a chapter to revise the subject.

The appendices are worth at least a skim through and then
possibly more frequent consulting during and after reading.
Lists of sorts (we would probably say glyphs or forms now),
terms, designers, and foundries, and an admirable reading list.

## Contents

1. The Grand Design

2. Rhythm & Proportion

3. Harmony & Counterpoint

4. Structural Forms & Devices

5. Analphabetic Symbols

6. Choosing & Combining Type

7. Historical Interlude

8. Shaping the Page

9. The State of the Art

10. Prowling the Specimen Books

## Chapter 1 The Grand Design

This chapter is the manifesto.
First, the text. Do not overdress the text, but at the same time,
do not leave it limp and lifeless on the page.
Taste is often a matter of having a little decoration, but
not too much.
Bring joy to the page, but not the rictus of the joker.

This chapter also introduces the rest of the book,
letting you plot your own way through it.

## Chapter 2 Rhythm and Proportion

A bit of musical allusion here.

For a kerning consultant this is
one of the more exciting chapters.
There are lots of useful tips about spacing, and proportion.
I'm amused that sometimes the advice is:
some fonts have bad or no kerning tables, you’ll have to fix that.
Advice that is repeated in the Specimen chapter (Chapter 10).

The kerning advice is excellent, covering not just the basics,
such as kern «To» and your «Av», but
also the slightly more advanced ideas like
please kern «Tc» because while it doesn't appear in English,
it's embarrassing if you can't typeset _Tchaikovsky_ properly.
It goes beyond that into some advanced topics like diacritics:
«Wo» should be kerned, but with a diacritic, «Wö», should not.
Incidentally, this example appears in the OpenType documentation
illustrating an example where you might want to _lower the diacritic_.
Thankfully I've not seen that in action.

It's nice to see some advide about
contextual kerning of apostrophes, such as «L’Anse» where you
should check that the «L’» kern and the «’A» kern don't together
cause the L and A to clash.

But kerning is just one part of the chapter.
There is good advice on spacing and setting in the horizontal domain,
the vertical domain, and their interactions in paragaph spacing, as
well as good stuff on blockquotes, pull quotes and so on.

Should probably read in conjunction with
Chapter 8, Shaping the Page.

## Chapter 3 Harmony and Counterpoint

The musical theme continues,
opening with a parallel between
musical scales and typographic size scales.

There is more advocacy in this chapter.
Advocating for a minimal scale of sizes, for text figures,
small caps, ligatures, and robust support for non-English texts.
Advocating against unnatural use of bold, bold italic, and
poorly designed sloping styles.

It is a densely packed chapter and worth re-reading.

## Chapter 4 Structural Forms & Devices

This is about navigational systems in text documents.
Titles, chapter titles, headings, paragraph breaks, footnotes,
marginalia, page numbers, and so on.

Much of the advice in this chapter is supported with examples
from the book itself.
_Elements_ has excellent book design,
making this one of the strongest and clearest chapters.
The advice: A book should usually have a half-title and a title;
the example: check this book's title pages!

## Chapter 5 Analphabetic Symbols

_Analphabetic_ here means,
the shapes and symbols that are not in the alphabet,
but as _Elements_ says “travel with the alphabet but never quite belong”.
The dots, commas, strokes, brackets, splats, squiggles, and other
specialised and comedic members of our fonts.

Initially I thought that _analphabetic_ was
a bit of a silly term and a bit of a mouthful to verbalise.
I guess I'm “in” now, because I think it's okay.
But maybe non-alphabetic would be clearer.

_Elements_ has lots of say about hyphens, naturally.
One and a half pages. Hang them, incline, redesign them.
No love of the foundry staff here,
blaming them for hiding and mistreating the auteurs intent.
This is entirely normal amounts of words to spend on hyphens.

Plenty of good advice on dashes, ellipses, apostrophe, brackets,
parentheses.
Amusingly, some of the advice is is make your own hyphens or brackets,
or steal them from another font.
“If a font had bad accents, I would simply design my own”.
I accept that this could well be the best advice sometimes.
But surely the jobbing typographer doesn't have
the tools to edit a font even if they have the inclination and the skill?

Some of the advice is sufficiently questionable that
it is not followed in _Elements_ itself.
Hyphens at the line's end should hang in the margin.
But only the two pages that contain that advice have used
hanging hyphens.

Apparently the solution to the problem of
"should full stops at the end of
quotes appear inside the quotation marks",
is to kern the quoation marks over the full stops,
so they appear one above the other, aligned vertically.
I find this advice quite surprising, I'm willing to try it,
but _Elements_ doesn't try it.

The chapter ends with a piece of advice that is
a brilliant blend of practical and impractical:
a suggested keyboard layout (with Control, Shift, and Alt modifiers)
that improves on the OS provided ones and gives
the typographer access to
the most used latin script alphabetics and analphabetics.
Have you tried installing your own keyboard layout?

## Chapter 6 Choosing & Combining Type

Lots of practical advice,
often reflecting two recurring themes of _Elements_:
draw type from the same historic period;
draw type from the same designers.
_Elements_ illustrates these points with well chosen examples.
Some advice is intended to defamiliarise:
consider a blackletter instead of a bold,
separate the italic from the roman and
use it as a font in its own right (as typographers used to).
There is very useful advice for typography across multiple scripts.
Extending support and advice for
working in latin, greek, and cyrillic scripts is also
something of a recurring theme.

## Chapter 7 Historical Interlude

I suspect this was the chapter that Bringhurst enjoyed most.
The academic research is highlighted here.
The fruits of the labour of digging in the archives.

This is more than a mere interlude however.
The history of typography gives us
insight into the typefaces we use today.

Knowing a little bit of history allows us to
navigate typographic styles more confidently.
Knowing more history allows us to
model that history in our typography.
Making deliberate choices to emulate or echo a particular period
and harmonising our designs so that the fonts chime with each
other and with other aspects of a design.

## Chapter 8 Shaping the Page.

This is the most technical and the most esoteric.
Typical of twentieth century manuals it implicitly
assumes that the end-goal of typographic style, is book design.
The ideas in this chapter are around page shape,
text block shape, ratios golden and harmonic,
ratios of columns to margins,
margins to one another, ratios of page width to page height, and
so on and so forth.

The ratios are curated in a "my favorite ratios" table,
with references to ancient greek mathematics, Fibonacci sequences,
temperament in musical theory, and ISO 216.

I find the topic both bewildering and bemusing.
Is it really the case that the golden ratio (~ 1.618) is
sufficiently superior to 5:8 (1.6) to be noticeable?
Once I've folded my sheets, sewn sections together and
trimmed them, will I really be able to tell the difference anyway?
In a book 200mm high the difference between a page
ratio of 1.6 and a ratio of 1.618 is about 1.4mm in the width of
the page.
A folio on the outside of a section will be wider than a folio
on the inside, because the folio on the outside will have to
curve around the thickness of the section.
In my copy of _Elements_ that thickness is about 0.7mm,
so if we were to carefully unbind the book
the trimmed pages would differ in width by up to 0.7mm.

Having said all that, clearly one should be deliberate in page
design and not merely slap some text into a space.

The chapter has some fine drawings of hexagons, pentagons,
joined pentagons, showing the relationship of various rectangles
to the proportions of regular polygons.

## Chapter 10

Chapter 10, is called _Prowling Through the Specimen Books_
which I think is something of an in-joke.
_Prowling_ suggests something that monsters in the night might do.
Typographers are wolves circling the flock, checking out the sheep.
Is there are a weak one that can be singled out, teased away?
Which one will fall prey to the ravenous wolf?
I suppose there are worse fates than being picked out for a book poetry.

It is essentially a “my favourite types” chapter.
Lists of typefaces are always in danger of being a bit dry.
But this one manages to liven things up a little.
The author survey's fonts for hand- and machine-composition, as
well as phototype and digital versions,
occasionally cheekily serving up a recommendation for
a typeface only available in metal.
This section does well as a source of inspiration not only for
typesetting current works but also for
working on some useful revivals.

The chapter concentrates more on romans for body text,
but also has useful sections san serif text faces, blackletter,
uncial, script (meaning like handwriting), greek, and cyrillic.

I sometimes find the notes about the particularly historical
derivation of one typeface or another, or exactly which
foundry's basement the matrices were discovered in,
a bit academic.
But I accept that they provide a fabric in which the
relationships between types are woven.
And those relationships are often useful.

There are notes on speciality typography,
such as a books that are in latin script but also
require fragments of greek or cyrillic.
Helping you choose which typefaces could be used or which pairs will
work well together.
Useful if you need to do that sort of work.


END
