[brutal]: #title "OpenType Layout Order Bug"
[brutal]: #xdate "2023-03-09"
[brutal]: #author "drj"

I say «bug» in the title, but that’s about as likely to be a bug
in my understanding as it is a bug in the OpenType spec or its
implementations.

I have a system where i have a glyph, _A_ in the examples below,
with 3 forms:

- neutral form
- following form, to be used when this glyph follows another
- initial form, to be used when this glyph is in initial position and
  does not follow another.

When text is typeset, the neutral form should not appear, all occurrences
of _A_ should be transformed into either initial form or following form.

In the feature code that i show below, i use `/B` for the initial form,
and `/C` for the following form.

The feature code that works:

    feature ss01 {
      sub [B C] A' by C;
      sub A' by B;
    } ss01;

The feature code that does not work:

    feature ss02 {
      sub [B C] A' by C;
      sub A by B;
    } ss02;

Can you spot the difference? It’s that a prime (ASCII apostrophe) has
been removed from the `sub A` line in the second section of code.
And this somehow causes it to not work.

What does work mean anyway? So, according to my system description
any `/A`s in the input should be replaced with either `/B` or `/C` with
`/B` being used for the initial position and `/C` for all the others.

So, when working, `AAAA` -> `BCCC`. And that’s what the `ss01` rules do.

But the, non-working, `ss02` rules do `AAAA` -> `BBBB`.

I have put the two sets of rules into two different features, co-opting 
the `ss01` and `ss02` features normally used for Stylistic Sets.
It shouldn’t matter which feature tags i use, and it seems not to;
i originally discovered the problem with the rules in the `ccmp` feature.

Here's a thing where you can switch the features on and off and edit the 
text.

So i know something about what is going on: the number of lookups is 
different in the two cases.
Feature `ss01`, which works, has 1 lookup, with two rules.
Feature `ss02`, which does not work, has 2 lookups.
The second rule in `ss02` does not have a prime, and
this  seemingly trivial difference in rule syntax, means that
this rule is a different LookupType, and
then that requires a different Lookup (all the rules within
a single Lookup must be of the same LookupType).

The `makeotf` feature compiler automatically creates lookups
for rules that are not defined within an explicit Lookup, and
will split the `ss02` rules into two separate lookups.
For the skeptical (which includes my more careful self) i have
implemented a feature with those lookups made explicit:

    feature ss03 {
      lookup Follow {
        sub [B C] A' by C;
      } Follow;
      lookup Init {
        sub A by B;
      } Init;
    } ss03;

The names of the lookups, here `Follow` and `Init`, are chosen to
describe the purpose of the rules, but are arbitrary and are
compiled-out of the resulting binary font file.

Here’s an interactive where you can switch on and off the `ss03` feature;
you should find that it has the same behavious as the `ss02` feature 
above.


To understand why having 1 or 2 lookups might matter, i have to delve
a little in to how lookups operate on the glyph buffer.

Within a lookup the rules are consulted in order until the left-hand part 
of the rule matches, and then the substitution is applied, _and the lookup 
is terminated_.
Once a rule within a lookup matches, the following rules in the lookup are
skipped.

So this explains how the `ss01` works.
When the buffer starts with `/A` the first rule in the lookup does not
match at that position, but the second one does, so the second is applied.
Any `/A` later in the buffer will be matched by the first rule, so the 
first rule is used for those, turning then into `/C`, and the second
rule is not consulted at all.

What about `ss03` (or `ss02`, they are equivalent)?
Observing the output, it must be that the second lookup is applied before
the first.

But the text in the OpenType spec says:

> apply the lookups in the order given in the LookupList table.

(OpenType specification, table GSUB, «Table Organization»; quote found 
within rule 6 of the description: «To access GSUB information, clients 
should use the following procedure» 
https://learn.microsoft.com/en-us/typography/opentype/spec/gsub#table-organization)

The AFDKO has similar language, with «Assemble all lookups in these 
features, in LookupList order» and «Lookups will be created in the 
GSUB/GPOS table’s LookupList in the same order as the corresponding named 
lookup blocks or runs of rules in the feature file»
(OpenType Feature File Specification 
http://adobe-type-tools.github.io/afdko/OpenTypeFeatureFileSpecification.html#7)

And indeed, using `ttx` i can see that the lookups are in the intended 
order.

So, what is going on? Any ideas why the second lookup is being applied 
first? What have i not accounted for?

If you have answers, ping me on Mastodon: https://typo.social/@drj.

# END
