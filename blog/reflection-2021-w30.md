# 2021-W27 2021-07-29

The Chaos. It's been a while, apparently.
The University has gone.
The government has opened everything.


## TypeWknd

After a faltering start, accepted for TypeWknd;
they need tape by 2021-08-16 (see e-mail dated 2021-07-25).
Video should be PT20M long.

## Examples

Still pending: SYSTEM FAIL in Atwin.

Still need examples for Atwin. in 2000 x 1000.

## Linocuts!

Got a linocut kit and started using it.
Made two different birthday cards.
It's fun!

Sketched out some of an alphabet.
Could linocut letters, effectively yielding a set of printing blocks.
Could then print, scan, and make a font.

## Cubic Type

Nephew has sketched out some design ideas for a logo.

Another family member wants a book design.


## Fonts

`font-atwin`

Ran fontbakery and fixed some errors.
I think some remain because they are untractable geometry problems.
Some were genuine geometry misalignments.

The Type Crit specimen revealed that kerning for accented
letters was wrong and needed doing.


`font-auncial`

Considerably more effort on the new forms.
It is looking plausible now.
the d is still terrible.

## END
