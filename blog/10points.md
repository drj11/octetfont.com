[brutal]: #title "10 points"
[brutal]: #author "David Jones"
[brutal]: #date "2022-04-12"

_Asmol_ is a **one weird trick** font.
The weird trick is: Set the Units per M (UPM) to 10.
10 is ludicrously small.
Most design standards would use a UPM of 1000 for OTF, or
2048 for the older TTF.

Normally the UPM doesn't matter much:
on a typical em square does it really matter if we have 1000 or
2048 positions to choose from?
Seems like a lot in either case.

But with a UPM of 10, the em square is now 10 by 10,
and this is the integer grid on which the font glyphs must be
designed.

In fact you can't set UPM to 10.
The OTF spec requires it to be between 16 and 16384.
Slightly to my surprise the tools enforce that limit.

So i set UPM it to 20 and use a grid of 2 and
disallow odd coordinates (even with a grid of 2, sometimes
odd-valued coordinates creep in and i keep having to eliminate
them).

Here is the alphabet:

<img style="max-width:88vmin; max-height:88vmin;" src="image/asmol/asmol-alpha.svg">

In this discussion, i will treat it as if the UPM is 10.
If the tiny number below scare you, because you are used to
seeing them on a standard 1000 UPM grid, just multiply all the
numbers by 100 by adding 00s.


## E

Consider the letter **E**, and its height.
For basic legibility each stroke and each counter should be at
least 1 unit high, giving the E a minimum height of 5.
If you want lower-case and upper-case we probably want at least
5 for lower- and 7 for upper-case.

For symmetry reasons that i discuss when we get to **O** it
turns out that heights of 6 for lower-case and 8 for upper-case
are better.

Most designers (which is short for "most designers when
expressing an opinion about what most designers would find
acceptably mainstream") will say that
the middle crossbar of the **E** should
be slightly above the exact centre.
With a grid of 8, that gives us one option:

1:2:1:3:1

And we can have a thick/thin contrast of either 1:1 or 2:1.
I opted for 2:1.

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/E.svg">


## Z

The **E** could have been designed with pixels.
We can have diagonals though:
it's only the on- and off-curve
points that define their ends that have to be on the grid.

**Z** is a relatively simple illustration of this point.

**Z** has one _contour_. A _contour_ is a sequence of connected
lines and curves that ends up back where it starts.
In the diagram, i've added marks to show where the points that
define the contour are.

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/Z.svg">

We'll see a lot of contours and diagrams like this.


## A

**A** is another well-known letter with diagonals.

Here drawn with a stroke width of approximately 2.
The inner apex is a contour point, which also has to be on the grid.
That means that in this **A** the stroke of each leg is tapered
as it approaches the top apex.
Which in this case is good: it's considered good type design
practice.

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/A.svg">

We can quibble about whether the narrowing should be linear or
whether there is too much or too little or whether we should
have an ink trap, but the fact is that the grid constraint
does not rule out making artistic typographic decisions.

This **A** has another trick up its sleeve.
Look at the crossbar. Does it look like it can be on the grid?
Actually the inner triangular counter is exactly on the grid,
but the corners on the bottom of the crossbar are not.

If we look at the outline we can see the trick.
There are two contours, one for the legs, and one for the
crossbar.
The crossbar is on the grid, but the endpoints are inside the
legs.

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/A-grid.svg">

The OTF renderer works by effectively forming the union of the
two shapes, which creates new points.
Effectively where two shapes intersect,
there are implicit points that need not be on the grid.

Normally in a production font with this shape of **A** you would
expect two _non-overlapping_ contours: one that goes around the
outside of the **A** and an entirely separate one for the
counter inside (that's the hole in the **A**).

It's common to design the **A** like i have, and have a final
"remove overlaps" step to create the production font (Glyphs has
a checkbox for this).


## V

I drew the **V** similar to the **A** but with a sharper apex.
Sometimes the shapes are exact copies,
but more usually they are similar, but each adjusted.

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/V.svg">

On the **A** the apex has a flat width of 2.
In this **V** the flat width is 1 (the x-coords are 4 and 5).

What about the inside apex, which appears to have an x-coordinate of 4½?

Another trick of course, which the outline reveals:

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/V-grid.svg">

There are actually two contour points involved in the apex,
and the contour does a loop on the inside of itself.
This is another case where the exact winding rule that the OTF
renderer is using matters.
Or it matters that the implementation match the spec, to put it
another way.

This one is interesting because _in the design tool_ designers
often use this corner trick (Glyphs calls them "Open Corners")
to make it easier to adjust one part without destroying the
slope of another part.

I like to call these _trucks_.
They support the design but are not seen in the final font.

As mentioned above in **A** most design tools will have a pass
that removes these, and
it is generally highly recommended that you enable this pass.


## Remove overlap (on no account try to)

Of course for this font _remove overlap_ is an absolute disaster.


## O curve

The grid rules don't stop us having curves,
it's just that the on- and off-curve points must be on the grid.

Consider a generic **O**, two contours, one inside the other.
Each contour has an on-curve point at the extreme N S E W
points; so 4 each.
Because of the symmetrical nature of this O, that means the
height and the width of both contours have to be even (otherwise
the on-curve control points won't be halfway).

As hinted at in the discussion of **E** above,
the constraint that extremal control points be halfway
(left-right or up-down) and on the grid means our cap-height is 8.
Therefore the width of **O** is also 8 (we could have 6 i suppose,
but it makes the counter very narrow).

That fixes the on-curve points, leaving only the
off-curve control points.
They must be on a tangent, which is good, because the grid
requires that too.

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/O-grid.svg">

Given that this **O** is mirror symmetric both horizontally and
vertically, we need only consider the bottom left quadrant.

The grid constraints mean that there are only 4 possible
positions for each of the 2 control points on the outer contour,
assuming that we don't push them outside the (bounding) box.
We should also avoid
putting the control point on top of the on-curve point because
that creates a corner (discontinuity in derivative).
Neither of which a type-designer will do.

For the inner contour there are only 2 position horizontal
positions and 3 vertical positions.

This was the most pleasing combination i could find, given the
time avaiable to me.

The **O** in a real font won't be this symmetrical.
Even extremely geometrical fonts usually have some optical
correction.

## Optical overshoot compensation

Speaking of optical correction, it's basically impossible in
this font. Go away!


## C

Remember i said not to put the control on top of the on-curve
point? Yeah well, the **C** does that.

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/C-grid.svg">

Where the stroke terminates, on the inside there are two
on-curve points (one at the corner, one at the top of the
counter if considering the upper terminal).
These on-curve points are only separated by 2 units in x and 1
unit in y, meaning that it's basically impossible to draw
without some of the control points overlapping.

Slightly to my surprise it turns out to be possible, and
useful, to put an "off-curve" control point on top of an
"on-curve" point.
Geometrically of course that makes in on-curve, but
conceptually it remains off-curve.

The two curves that define the upper terminal of this **C**
each have overlapping points.
It looks like there is only one stick, but really there are two
it's just that one of them is zero-length.

Off-curve points are drawn with a small diamond, on-curve points
drawn with a small square.
Where two points are drawn at the exact same grid location,
the overlap is drawn in a sort of 8-point star.


## P

Does that **P** look suspicious to you? No?

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/P.svg">

Shall we think about the grid again? The height of the bowl of
the counter is the same as the gap between the baseline and the
outer contour of the bowl: 3.
In terms of vertical positions P is built on a 1:3:1:3 system.

Remember what i said about curves needing on-curve points at
their extrema?
Yeah well it turns out that those type-design
rules were more sort of guidelines anyway.
The outer outline curve is 5-high so if it had an extreme point,
the y-coordinate would need to be 5.5 (halfway between 3 and 8,
would make it symmetric).
In this case it's better if we remove the extreme on-curve
point.

Again the outlines illuminate:

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/P-grid.svg">

Each contour (inner and outer) has a curve that starts with a
tangent to the east, and finishes with a tangent to the west.
Normally such curves would also have an extreme on-curve point,
but as discussed above, the grid would force us to put such a
point in a ruinous place.

Without the on-curve extreme point, we keep the symmetry.


## S

The spine of the S is the diagonal bit that cross from top-left
to bottom-right.
The spine of this **S** lacks a control point on the inflexion.

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/S-grid.svg">

Each of the two outlines (one on the left side and one on the right),
has just two on-curve points at the North–South tangent.
But again, as for the **P**, if the **S** had an on-curve point
on the spine, the grid would force it to look horrible.

This is actually a little bit more conventional.
Some type designers like to avoid a control point on the
inflexion, particularly for variable designs.

The lower stroke of the S also also uses the "avoid an extreme
point" trick to avoid fixing the lower extreme point of the bottom
bowl to the grid.


## B

The **B** is the end-of-level boss that combines all previous
attacks.

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/B.svg">

The grid diagram might be a little bit hard to decode,
because there are off-curve points that overlap on-curve points.
We've seen that previously in **C** and **S**, but here,
there are different contours involved.

<img style="max-width:70vmin; max-height:70vmin;" src="image/asmol/B-grid.svg">

An off-curve control point for the upper bowl (one contour)
overlaps with an on-curve point for the outer contour
(a different contour).
But this also just happens to be a point that is interior to the shape,
because it uses the inside-loop trick from **V**.

The upper bowl (height 2) _does_ have an on-curve extremal point.
The lower bowl (height 3) _does not_ have an on-curve extermal
point, and this pushes the off-curve control points so far to
the right that one of them "goes past" a point on the outer
contour.


## Concluding remarks

This alphabet developed under a very tight constraint.
Type designers strive for and thrive under constraints.
The very act of _making type_ is an attempt to constrain
a writing system into a series of more-or-less rigid forms.

We invent systems, like OpenType layout, that loosen the
rigidity, and let us create varying combinations, but
those combinations and variations cannot match hand-writing,
lettering, or calligraphy: endless forms, most beautiful.

On a technical level,
i'm slightly surprised at how well the editing tools and
the font-renderers work on this font.
It has almost the smallest possible UPM, and
violates several recommended design rules.

# END
