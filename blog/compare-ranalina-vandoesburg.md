# A comparison of Ranalina and the van Doesburg alphabet

My reference for the van Doesburg alphabet is the figure on page
50 of [BLACKWELL1990].

Both typefaces feature a 5 line grid.
Exemplified by the E: 3 horizontal strokes form an equal spaced
stroke–space–stroke–space–stroke pattern.

Both typefaces are fonts born of rules.
Firstly, constrained to be constructed of
simple rectangles of a single width.

The van Doesburg alphabet constrains itself even further by
using only strictly horizontal or vertical strokes.
Ranalina allows itself to have diagonal strokes,
which gives it an advantage in V and X.
va Doesburg has quite imaginative solutions for Y and K:
The Y is the primary school lower-case y fitted into the 5 × 5 grid,
the K looks pained by being wrenched into the grid, but
the proportions of the counter spaces has been preserved.

The principal rectangle of the van Doesburg letters is a 5 × 5 square;
in Ranalina it is a 4 × 5 rectangle.
Ranalina has the better proportions,
but van Doesburg wins in purity of thought.

The van Doesburg letters adhere to a strict stroke touch rule:
Where a rectangle meets another, the short side of the rectangle
completely touches its neighbour.
Ranalina, with a more relaxed attitude, allows rectangles to
overlap only partly at their corners.

Consider the letter D, which, in the platonic ideal,
has a large round stroke.

Ranalina and van Doesburg take different approachs to capturing
this form in straight rectangles.
Ranalina lets a horizontal stroke and a vertical stroke overlap
partially at the corner.
The effect is as if a small square has been subtracted.
It is not a curve, but it is perhaps the suggestion of one.

Instead of hinting at a curve, van Doesburg buttresses up the
corners of the D with full thickness “serifs”.

The van Doesburg alphabet has more internal symmetry than Ranalina.
The S and Z are mirror images of each other, as are J L.
C N U are related by rotation, as are E M W.

As we find the alphabet in [BLACKWELL1990] it has only the basic 26
letters. No numbers.
Given the strict design rules for van Doesburg, it is difficult
to see how some of the numbers could be drawn unambiguously.
The 2 would look like the Z, the 5 like the S.

The van Doesburg letters are designed to stricter rules,
only one letter, the I, strays from the 5 × 5 grid;
all the strokes are horizontal or vertical, none stray outside the box.
Ranalina, while still severely constrained by its own design rules, 
has a little more fluidity in its shapes.
Overall the van Doesburg letters come across as uncompromising
in their principles.

