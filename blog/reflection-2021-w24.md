# 2021-W24 2021-06-17

## Fonts

`font-atwin`

Minor activity filling in quotes and brackets.

`font-andily`

plaqued.

`font-akern`

Added a beautiful ampersand (I think), and some other punctuation.

Small work creates more work
(drawing the ampersand leads to a push to draw more punctuation).

`font-amelt`

unhappy with name. TD suggests Amplette, which i like.

`font-agon`

Made the crossed-grid design for **X** the primary glyph.


## about.cubictype.com

(today), a catchup of recent work. `plak` really helped.
Colours are hard.


## Tools

`plak` continues, it's even installable with `pip` now
(from a repo or a .whl, not from pypi).

It may be time to move plak to Go.


## Talks

The TyeLab part of Typographics Conference seems like the best bit.
IT IS SO GOOD!

Slide decks are so well polished.
The people are so pretty, their spaces
(offices, studios, bedrooms!) are so nice and carefully furnished.

