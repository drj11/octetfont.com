# 2021-W30 2021-08-05


## TypeWknd

After a faltering start, accepted for TypeWknd;
they need tape by 2021-08-16 (see e-mail dated 2021-07-25).
Video should be PT20M long.

Essentially no progress on this. Opps.


## Atwin

Shipped Atwin to Monotype, it is now available
https://www.myfonts.com/fonts/cubic-type/atwin

## Linocuts!

Made a hedgehog linocut and printed about a dozen cards.
The kraft card is better than the plain white and plain yellow;
both of which seem too light.


## Fonts

`font-atwin`

Ran fontbakery and fixed some errors.
I think some remain because they are untractable geometry problems.
Some were genuine geometry misalignments.

The Type Crit specimen revealed that kerning for accented
letters was wrong and needed doing.


`font-auncial`

The "summer" style (the new forms) has a complete A to Z
alphabet.
The d is still terrible.

Research Tiro's et a bit. Draw it and a few ampersands for
summer.

## END
