<meta charset="utf-8">
<title>A few good ampersands</title>
<body>
<nav>
<a href="review-geometry.html">(previous)</a>
<a href="half-alphabet.html">(next)</a>
</nav>
<article>
<h1>A few good ampersands</h1>
<time>2021-05-19</time>
<address>by David Jones</address>
<p>The ampersand is essentially a ligature for <em>et</em>, the latin for <em>and</em>.</p>
<p>The usual form for this is a figure 8 with a crossed pair of tails on the
lower right.
Let’s have a look at some.</p>
<p>(the fonts in this article have all been chosen because
they are bundled with macOS, or are openly available;
while the selection is intended to be representative of the common forms,
it’s not particularly systematic.)</p>
<p>Baskerville is typical.</p>
<p><img src="image/ampersand/ampersand-baskerville-regular.svg"></p>
<p>Most sans serifs use the same shape, but without the serifs.
Here modelled by Avenir.</p>
<p><img src="image/ampersand/ampersand-avenir.svg"></p>
<p>Because i have a maths degree, i see that this form encloses 2 holes and
therefore has topological genus 2.</p>
<p>The reader shall indulge me in the briefest introduction to topology.</p>
<h2>Topological genus, the briefest introduction</h2>
<p>Topology studies to what extent we can smoothly deform one object into another,
and <em>topological genus</em> is a mathematical system for
describing objects with similar topology.
More simply, it’s the number of <em>holes</em> an object has.</p>
<figure>
<img src="image/genus.svg">
<figcaption>
Blobby objects of genus 0, 1, and 2 respectively (from left to right)
</figcaption></figure>

<p>It can get a lot more complicated that that, but i <a href="https://en.wikipedia.org/wiki/Genus_(mathematics)">refer you to Wikipedia,
if you want more topological complexity in your
life</a>,
because that’s cheaper than a copy of
<em>Algebraic Topology: A First Course</em> from
Springer’s Graduate Text in Mathematics series.</p>
<p>When I say, above, that the Avenir <em>&amp;</em> has genus 2,
I mean it has 2 holes.
If i made it out of some imaginary deformable material, i could squash it
around, without making any new holes or closing existing ones, to get the
“egg with 2 holes” shape above.</p>
<h2>Counters, closed &amp; open</h2>
<p>In typography, a hole inside a shape is called a <em>counter</em>.
In the metal era of type, a <em>punch</em> was made for each letter, the
holes in the punch were made with a <em>counter-punch</em>.</p>
<p>Counters in a shape can be closed, as in <em>O</em>, or open as in <em>G</em>.</p>
<p>Here we see PT Sans modelling the counters in
<em>&amp;</em> (2, both closed) and <em>a</em> (2, one open and one closed).</p>
<figure>
<img src="image/counter.svg">
<figcaption>
Counters are shown in blue
</figcaption></figure>

<p>In this 2-counter form, the top counter is teardrop shaped, pointed down
and slightly to the left or sometimes straight down;
the bottom counter is D shaped.
Both the counters are closed, making this a topological genus 2 form.</p>
<p>This is the most common form, but
there is considerable variation in the angles and terminals of the tails.</p>
<h2>Italic variant</h2>
<p>The italic style is, in ordinary serif font families,
not just the regular form drawn with a tilt;
it’s based on a different model.
For this reason, in many italics, the ampersand has a complete different form, and
shows more clearly its <em>et ligature</em> origins.</p>
<p>Sans serif families do tend to have italics that are
more like the regular font tilted over.
Most typographers use the term <em>oblique</em> for this, rather than <em>true italic</em>.
So, we see this alternate form of ampersand more commonly in serif families than
sans serif families.</p>
<p>A typical italic form is modelled here by Baskerville Italic.</p>
<p><img src="image/ampersand/ampersand-baskerville-italic.svg"></p>
<p>In Baskerville Italic (above) the swash terminals on the right-hand <em>T</em> part
are perhaps a little bit more flamboyant than average, but
still well within the acceptable spectrum.</p>
<p>Palatino Italic is similar:</p>
<p><img src="image/ampersand/ampersand-palatino-italic.svg"></p>
<p>Trattatello (bundled with macOS, marketed as
<a href="https://fontsinuse.com/typefaces/11114/p22-operina">P22 Operina</a> elsewhere) is
based on Arrighi’s <em>La Operina</em> pamphlet, and shows a clear correspondence to
how one might write <em>et</em> in handwriting:</p>
<p><img src="image/ampersand/ampersand-trattatello.svg"></p>
<p>This italic form is open, and could be thought of as a topological genus 0
form.</p>
<h2>Genus 1 forms</h2>
<p>There are a couple of genus 1 forms, not related to each other.</p>
<p>Apple Gothic has an ampersand which is the genus 2 form, but
with the top counter made open.
Courier (and Courier New) and Pria Ravichandran’s Catamaran have a similar form.</p>
<figure>
<img src="image/ampersand/ampersand-apple-gothic.svg">
<img src="image/ampersand/ampersand-courier.svg">
<img src="image/ampersand/ampersand-catamaran.svg">
<figcaption>
From left-to-right: Apple Gothic, Courier, Catamaran
</figcaption>
</figure>

<p>Topologically speaking, Trebuchet has the same form (same genus), but
it is more upright.
It has a very clear connection to <em>et</em>:</p>
<p><img src="image/ampersand/ampersand-trebuchet.svg"></p>
<p>The other genus 1 form is based on the lower case <em>e</em>.
This is used by Cochin and the formidable Hoefler Text,
both in their italic styles:</p>
<figure>
<img src="image/ampersand/ampersand-cochin-italic.svg">
<figcaption>
Cochin
</figcaption>
</figure>

<figure>
<img src="image/ampersand/ampersand-hoefler-text-italic.svg">
<figcaption>
The formidable Hoefler Text
</figcaption>
</figure>

<h1>Oddities</h1>
<p>Skia, from Apple, on the face of it has a fairly ordinary genus 2 form, but
at the top crossing the strokes do not line up, producing a dog-leg effect.</p>
<p><img src="image/ampersand/ampersand-skia.svg"></p>
<p>Unlike the usual form, this one cannot be drawn with a single stroke of a pen.</p>
<p>Apple Chancery has a genus 3 form which seems almost accidental.
It’s essentially the genus 2 form with an extra flamboyant swash that
connects on the left, to make a new closed counter.</p>
<p><img src="image/ampersand/ampersand-apple-chancery.svg"></p>
<p>Then there’s Zapfino.</p>
<p><img src="image/ampersand/ampersand-zapfino.svg"></p>
<p>Thanks Zapf.</p>
<h1>END</h1>
</article>

