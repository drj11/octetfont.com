[brutal]: #title "Open Fonts"
[brutal]: #author "David Jones"

Fonts are licensed under many different licenses and schemes.
Here I want to look at some of the openly available fonts.
I'm using _open_ in a liberal sense here, meaning that
you can download the font for free,
and you are able to use it in any work,
and you can redistribute it with reasonable restrictions.
In particularly you should be able to use it in
commercial works (like apps);
if you distribute it (bundled with an app say) then you
should not misrepresent what the font is and who made it.

There are lots and lots of these fonts, but here I want
to concentrate on those that are bookish.
Fonts that you could plausibly use to typeset a book
(or for some of the san serif options, a museum leaflet).
So I'm ruling out the comedy, decorative, obscure, and
other miscellany.
There are plenty of these and they have
their place.
Who can forget that summer when every single
spiced chicken wing bar used Lobster?

I've tried to include fonts that are reasonably complete
in both having regular, bold, and italic variants in the
family, and in having a good glyph coverage.

After the fact, I adding a _minimum kerning standard_
which is that the word «TAKEAWAY» should be kerned.

# Superfamilies

The heavy lifters, are the _superfamilies_.
Fonts made available in designs that span categories:
for example having a serif and a san serif, or having a
serif and a monospace.

Those are:
- DejaVu (Sans, Serif, Mono)
- IBM Plex (Sans, Serif, Mono, Condensed)
- Kurinto (Sans, Serif, Semi Serif, Mono, Textura,
  Old Style, Arte, and a bewildering amount more)
- FreeFont (Sans, Serif, Mono) Metric compatible, derived
  from Nimbus
- Noto (Sans, Serif, Mono, and more)
- Roboto (Sans, Slab Serif, Mono)

DejaVu, FreeFont, and Roboto are remixes of earlier font
collections, extending them in script and sometimes fonts.
IBM Plex, and Kurinto are their own creations.


## DejaVu

DejaVu is an extended remix of Vera (Bitstream).
Extending the number of styles and scripts.
At least on my computer as it is installed now,
the DejaVu fonts are the default fonts for Firefox.

The family includes a Serif, a Sans a Mono in several weights
and with italics.
Script coverage is good but not comprehensive.


## FreeFont

FreeFont is a font family from GNU that
aims to have large Unicode coverage
(but deliberately avoids the large Chinese, Japanese, Korean blocks).
It includes a Serif subfamily, a Sans subfamily, and a Mono subfamily,
each available in Regular, Bold, and Italic (Oblique).

The core latin scripts were based on Nimbus Roman No9 (for the serif),
and Nimbus Sans (for the sans),
which were made open at the end of the 20th Century.
The Nimbus fonts were designed as working replacements
(or knock-offs, if you will), of Times New Roman and
Arial/Helvetica;
the graphical themes continue in the larger expanded FreeFont.

As FreeFont consists of three different core fonts that
don't really have a unifying design theme,
it isn't a true superfamily in the modern sense.
But it is worth knowing about because
it can replace the hugely popular Times/Arial combo and
it has good Unicode coverage.


## IBM Plex

Plex is an interesting development.
It's IBM's replacement for Helvetica as their brand font of
choice.
It's not unusual for companies to pick fonts as a core part of
their brand, and it's not that unusual for companies to
commission the creation of new works.

It _is_ a little unusual for the fonts to then to be made openly available
(IBM Plex is available under the SIL license), and its unusual
for the font work to be so comprehensive.

IBM Plex features a Serif, Sans, Mono, and a Condensed Sans
(ah meh, but they seem to be very popuar right now), in 8
weights, and italics, and 100 scripts, and it comes with a
snazzy website and its own merchandise and marketing team.

Maybe it will become as sexy as VAG Rounded, Volkswagen's 1978
rebranding that was allegedly put in the public domain.
In any case it was available enough that
Letraset marketed it and it was ripped off everywhere.




## Noto

_Noto_ is the megafamily to end all other font families.
In a sense quite literally;
_Noto_ is short for “No Tofu”,
Tofu being a term used to describe the little _tofu-like_ boxes you get
when a font doesn't have a character.
The goal of Noto is to provide
a harmonised series of fonts covering all scripts in Unicode,
and therefore a _font of last resort_ that can be used
to display text from a script that your current font doesn't support.
There are 75 font families, hundreds of font files,
dozens of styles, weights and so on.
All scripts in Unicode have some coverage, but
coverage isn't always complete, and
not all scripts and fonts have all weights and styles.

The Latin/Greek/Cyrillic fonts are derived from Droid,
the Chinese/Japanese/Korean fonts are derived from Source (Adobe).


## Roboto

Popular because it's the default Android font
(it replaced Droid which used to be the default Android font).

This is probably the least of the superfamilies,
as it has only a Sans, a Slab Serif, and a Mono.

Is a slab serif a good replacement for a traditional serif?
Probably not, but given that this font is all over Android,
I thought it deserved including.


## Kurinto

Seemingly a personal endeavour to
end bad typography in academic publishing,
Kurinto is breathtaking in scope.
21 families each supporting Regular, Bold, Italic styles.
Everything is here, a roman text, a book font, a sans, a mono,
a semi-serif, an artistic venetian, a humanist , a newsprint font,
a set of Times/Arial/Courier compatibles, and so on and so on.

Comes with a website, a sampler, and a user guide.

