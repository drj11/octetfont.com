# 2021-W49 2021-12-09

Still curiously dark.


## Linocuts!

More printing of linocuts. Bit disappointed with the result of
2-colour water-based ink, but P used the linseed oil based inks
and they look great.

black ink on silver paper also looks great.


## Lino fonting

Still stalled.


## ILT

Still no movement on this. I need to ping them.


## Afublo

18 upper case letters plus ampersand. It's lovely.
The ampersand was quite hard, and i think more design variations
should be tried.

Wrote https://home.octetfont.com/blog/half-alphabet.html as a
reflection itself.


## fext

`feax` is now renamed to `fext` (still for Feature Extract).

It now supports the AFDKO `by NULL` syntax,
which is used for a 0-length Multiple Substitution
(despite being forbidden by OpenType spec).


## plak

Added the `--order uca` option.

Still needs the `--blank` option.


## feature-zone

Motivated by:
- test `fext` a more robustly, and;
- illustrate AFDKO Feature File semantics more accurately.

I created `feature-zone`, a sandpit for creating OTF files
with rules that illustrate a particular point.

Basis is a font that is stripped-back version of Airvent and
some feature files.


## Fonts

`font-avimode`

Updated example PNGs.


`font-airvent`

No movement.

- Upper case: Spaced but not kerned.
- Small caps: not spaced.


## END
