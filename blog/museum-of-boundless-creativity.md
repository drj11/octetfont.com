[brutal]: #title "Submission for Museum Of Boundless Creativity"

The Museum of Boundless Creativity is canvassing for submissions
to its digital collection.

I nominate the font Impact (Geoffrey Lee, 1965).
Impact is the font of memes,
and memes have become the 21st century's mushroom cloud,
rising through the cultural strata of the previous century
obliterating all subtlety and nuance.

While in use earlier in "image macros", it was Impact's use in
the ur-meme "I Can Has Cheezburger" that ignited the fuse
and led to an explosion of Impact-on-cat-image memes
that we still feel today.

Memes are everwhere now and within those memes the font
you'll often see is Impact.

Further reading:

Wikipedia: https://en.wikipedia.org/wiki/Impact_(typeface)

Vox: https://www.vox.com/2015/7/26/9036993/meme-font-impact
