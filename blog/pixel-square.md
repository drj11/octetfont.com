[brutal]: #title "The Pixel & The Square"
[brutal]: #author "David Jones"

A _pixel_ is traditionally short for _picture element_.
Conceptually these are square.
But what about the reality?

On a typical computer or console of the 1980's the pixel was in
fact stretched into a rectangle.

8:9

Also Mode 5 (pixels are twice as wide as "normal").

There may not even be a "canonical shape".
The BBC Micro (and several similar competitors) could change
graphic mode,
whereby one pixel would be stretched horizontally
to twice its original width.
This was particular useful for the memory-starved microcomputers
of the 1980s,
with relatively simple clock hardware a horizontal line could be
made using half the memory.
Typographically a disaster of course.

An additional wrinkle is that TV standards differed from region
to region.
Often the simplest way of accommodating this was to stretch the
existing display frame in one dimension or another.


A TV adds blur and may have better resolution in one direction
that another.
A colour pixel is made up of three different coloured phosphors,
which will be in different positions.

A modern LCD screen is more likely to have square pixels, but
there are still 3 different coloured elements within the pixel,
and different displays will lay these out in different ways.

In the end, what are we trying to achieve?

Are we trying to faithfully reproduce what was possible on a
1980s TV set? Or are we trying to find an abstract form?

The abstract form is relatively easy to obtain.
And we can stretch it to various aspect ratios.

square grid -> stretched grid ->
abstract CRT -> realised CRT -> arcade grime

The idea of shape being transformed is not uncommon in
typography.

In the era of metal type a type designer would make a drawing,
which would be cut using a pantograph, then a matrix would be made,
and type cast, which would then be assembled into a form and printed,
and possibly the paper ironed after.

drawing -> pattern lathe master -> matrix ->
lead type -> press -> paper -> ironing

In the end the typographer is communicating a message so
we have to think about the effect on the reader.

## C64

According to https://en.wikipedia.org/wiki/Commodore_64 the
standard graphics mode was 320 × 200 pixels

## REFERENCES

This gives a good brief treatment of the C64:

https://retrocomputing.stackexchange.com/a/13872/13720

It also gives some timing and dimenions that are useful for other cases.
A standard TV picture was 4:3 (i suppose this is a well known
fact).
A horizontal scan line has 52µs of visible image.
PAL has 288 lines, NTSC has 240 lines.

The C64 generated 320 pixels in 40µs (the rest being border)

