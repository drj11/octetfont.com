[brutal]: #title "Unicode Space Characters"
[brutal]: #author "David Jones"

The space character is a little bit unusual.
It is not there.
It is the absence of a mark.
It has no form.

It does have a width though.
But it may not be a very definite width.
Most computer documentation preparation systems will vary the
width of a space, in order to give a more even measure.
As a matter of style or design, you may like to have a little
more space after punctuation.

In Unicode all spaces are represented by a single codepoint:
U+0020.

Haha, just kidding.

U+0020 is a sort of general purpose generic space.
If you're typing ordinary text into an ordinary program and you
press the space bar, U+0020 is what you get.

Next most popular is U+00A0, the NO-BREAK SPACE.
A _NO-BREAK_ (or non-breaking in ordinary writing) space is one
where it would be unwise to break the line in the course of
paragraph filling.
Commonly used when giving someone's title: Mx Jones.
Other places might be names of dates (June 17),
addresses (1-10 Otemachi), and similar.

Another situation where you might need a non-breaking space is
when putting units after weights and measures.
For example, 68 kg.
Writers and style-guides vary as to whether they would, or
should, use a space between the "68" and the "kg", but
if you are following the recommendation of the SI system, there
should be a space.
And it's probably a good idea to make it a non-breaking space.

Some styles might want a thinner than normal space between 68
and kg.
Unicode offers some of these.
THIN SPACE is U+2009 and is a nominal 1/5th or 1/6th of an em.
The non-breaking version of that is NARROW NO-BREAK SPACE U+202F.

Some recommendations for typesetting large numbers have spaces
in between blocks of 3 digits: 1 000 000.
It may be sensible to use NARROW NO-BREAK SPACE U+202F for this,
so that the entire number is considered as a single world,
according to the
[Unicode UAX #29 word boundary algorithm](https://unicode.org/reports/tr29/#Default_Word_Boundaries).

We can also use U+2005, FOUR-PER-EM SPACE,
which is I suppose a little bit bigger than THIN SPACE.

Unicode has a bunch of these, it has EN and EM SPACE, EN and EM
QUAD, THREE-PER-EM SPACE, FIGURE, PUNCTUATION, and
MEDIUM MATHEMATICAL SPACE, a HAIR SPACE (very thin), and, for
use in those scripts, an IDEOGRAPHIC SPACE and an
OGHAM SPACE MARK.
The last one, the OGHAM SPACE MARK, has the distinction of being
the only space character in Unicode that has any printed marks.

## Spaces and Fonts

Now, here's the thing.
How wide a particular space is
(such as the normal space, or the FOUR-PER-EM SPACE)
is determined by the font you are using.

Unicode might have ideas about how wide certain space characters
should be in relation to each other, but it can't really enforce anything.

So how wide are they?

The space and no-break space should be the same width, and for
me they seem to be:

█ █ U+0020 SPACE

█ █ U+00A0 NO-BREAK SPACE

Quad is a typographer's term for a rectangle of metal, which
would be used to provide a space.
The _em quad_ is supposed to be the size of an _em_ which
is one of the important dimensions of a font.

Unicode has an em quad _and_ an em space;
EM QUAD decomposes to EM SPACE, meaning they are in some sense
equivalent.
And lo they seem to be the same size:

█ █ U+2001 EM QUAD

█ █ U+2003 EM SPACE

An en space is a smaller space, nominally one-half em;
again, EN QUAD decomposes to EN SPACE.

█ █ U+2000 EN QUAD

█ █ U+2002 EN SPACE

We can arrange most of the spaces by decreasing size:

█ █ █ █ █ █ █ █ █ █ █ █ █ U+2001 EM QUAD

█ █ █ █ █ █ █ █ █ █ █ █ █ U+2000 EN QUAD

█ █ █ █ █ █ █ █ █ █ █ █ █ U+2004 THREE-PER-EM SPACE

█ █ █ █ █ █ █ █ █ █ █ █ █ U+2005 FOUR-PER-EM SPACE

█ █ █ █ █ █ █ █ █ █ █ █ █ U+2006 SIX-PER-EM SPACE

█ █ █ █ █ █ █ █ █ █ █ █ █ U+200A HAIR SPACE

█ █ █ █ █ █ █ █ █ █ █ █ █ U+2009 THIN SPACE

█ █ █ █ █ █ █ █ █ █ █ █ █ U+2007 FIGURE SPACE

The Unicode Spec has this to say for the last three (FIGURE, THIN,
HAIR):

> U+2007 figure space has a fixed width, known as tabular width, which is the same width as digits used in tables. U+2008 punctuation space is a space defined to be the same width as a period. U+2009 thin space and U+200A hair space are successively smaller-width spaces used for narrow word gaps and for justification of type.

In Glyphs, the default for U+2007 FIGURE SPACE is to make it the same
width as 0 (zero).

█ █ █ █ █ █ █ █ █ █ █ █ █ U+2007 FIGURE SPACE

█0█0█0█0█0█0█0█0█0█0█0█0█ U+0030 DIGIT ZERO

The other non-breaking space, NARROW NO-BREAK SPACE,
seems to be the same size as THIN SPACE.

█ █ █ █ █ █ █ █ █ █ █ █ █ U+202F NARROW NO-BREAK SPACE

█ █ █ █ █ █ █ █ █ █ █ █ █ U+2009 THIN SPACE

PUNCTUATION SPACE, according to The Unicode Spec, is “a space defined
to be the same width as a period”
(PERIOD in Unicode is now called U+002E FULL STOP).
No idea what MEDIUM MATHEMATICAL SPACE should be used for.

█ █ █ █ █ █ █ █ █ █ █ █ █ U+2008 PUNCTUATION SPACE

█.█.█.█.█.█.█.█.█.█.█.█.█ U+002E FULL STOP

█ █ U+205F MEDIUM MATHEMATICAL SPACE

There is also a space for use with Chinese, Japanese, and Korean
ideographic scripts.
It is designed to be the same width as ordinary ideograms
(ideographic fonts typically have all characters the same
width).

█　█ U+3000 IDEOGRAPHIC SPACE


## Cadence

We can test multiples of one space against another,
to see if they are the same size.
This is using the cadence of the spaces to
test them against each other.

Here's 2 FOUR-PER-EM SPACE against an EN SPACE:

█  █  █  █  █ U+2005 FOUR-PER-EM SPACE

█ █ █ █ █ U+2002 EN SPACE

and 2 SIX-PER-EM SPACE against THREE-PER-EM SPACE:

█  █  █  █  █ U+2006 SIX-PER-EM SPACE

█ █ █ █ █ U+2004 THREE-PER-EM SPACE


## Reflections and Fractions

At least in the font I am viewing this document right now,
for the 3-, 4-, 6-, per em spaces, they are indeed spaced so
that 3 per em is bigger than the 4 per em and is plausibly twice
the width of the 6 per em.

An _en_ space is notionally half an _em_.

So if we divide an em into 12ths, and use those fractions we
would have spaces of 2/12, 3/12, 4/12, 6/12 (en), 12/12 (em).
We could put a hair space at 1/12.

It seems somewhere between possible and likely that in the era
when metal typesetting was common,
the metal bodies carrying the types of a font would come in widths that were
multiples of a twelth of an em.
These spaces (1, 2, 3, 4, 6) are not only useful in their own
right, but allow all multiples of 1/12 < 11/12 to be done with
two spaces, with only 11/12 taking three.

The _pica_ is a unit used in typography and is 1/6th of an inch<sup>[*](#pica)</sup>.
Setting the em to a pica, 1/6th of an inch,
means you would get 72 hairs to an inch.
The computer typography point (set by Apple and Adobe) is 1/72 of an inch,
so I'm going to regard all the above as being true
even if there is evidence to the contrary.

## Footnote

<a name="pica">*</a>: A pica is not always 1/6 inch and I'm well
aware of that.
A _pica_ has variously been defined as 1/6 of an
American, British, and French inch, 35/83 cm, 0.1660 inches, and
so on.
In the era of metal type where you had to lock the whole thing
into a frame by banging quoins, this mattered.
Here in the digital world, I don't have to care about all that.

# END
