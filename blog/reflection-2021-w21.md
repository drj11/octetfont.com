# 2021-W20 2021-05-27

Everything seems slow and foggy.

Wednesday was a bit of a wash.

## Fonts

`font-akern`

Started on the lower case.
Progress is surprisingly rapid, but then Cheng puts **a**,
**s**, **g**, together (i just finished **s**).

Picking up a font again after a small pause means that little
details are lost to memory. Things like how much a serif
projects, how big the terminal taper of a serif is.
The curvature typically used.

The major metrics (thick stem, thin stem, x-height, cap-height
are written down or obvious, but others have to be reconstructed
from the material.
Holding them in working memory definitely helps.

`font-asiren`

Doing the **n** seemed difficult with this sample.

`font-agon`

This was an entirely new digital drawing of an earlier pencil
sketch. Rapid progress. Some interest among friends and socials.
Interesting insights into form, which was one of the points.
It proved an interesting puzzle for some on the twitter, which
was unexpected.

Definite possiblities for applying a bit more thought to both
the kerning possibilities and to some sort of joining system
that isn't simply left-to-right.

There is a blog-like writeup in the README.


**Grand Arcade**

This is not one of my fonts, it is the drawing of Kindersley's
street-sign font that the Cardozo–Kindersley workshop did for
the Grand Arcade, Cambridge. The font is available for free from
the workshop website.

The form is of course impeccable, but the font-file as supplied
is unusable. It has a warning (about the kerning table) then you
try and install it on a Mac, and the extender lines are wrong so
it crops the bottom part of many letters. The kerning table
only has two entries and so is probably not generated properly.

The double dagger is lovely.

I started to make a `font8` tool to inspect the kerning,
needed in the long-term anyway.
Don't think i got very far with that. Enough to tell me that
`ttx` (below) was telling the truth.

I used `ttx` to inspect the file which shows a very short
kerning table (2 entries, for **OO** and **co**).

Should work this up into a proposal.

## ccmap

I wrote a `font8` tool that should help with plaques, but doesn't yet.
It shows the unicode value that appear in a `cmap` table in a
font-file.

# END
