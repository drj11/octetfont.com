[brutal]: #title "Digitising a font"
[brutal]: #author "David Jones"

What is involved in digitising a font?

From printed materials, say.

- scan
- trace
- clean
- space
- kern
- extend
- features
- variations

_Scan_ is scanning or photographing to get the materials in
digital image form.

_Trace_ is tracing around the images to make vector art.
This will usually be done in a dedicated font package
(Glyphs Mini).

_Clean_ is cleaning and tidying the vector art.
Getting rid of irrelevant small curves and sections.
Unifying strokes to have the same or related thicknesses, and so on.

_Space_ is letterspacing the glyphs.
At this point basic text can be typeset.

_Kern_ is spacing letter pairs where they need particular
attention (for example TAY will usually need kerning).
At this point the font is suitable for limited release.

_Extend_. more glyphs? Punctuation, symbols? Upper and lower case?
Accents? Upper and lower case numbers? Ligatures? Small caps?
Cyrillic, Greek, Korean, and so on?

_Features_ are OpenType features. Does it need to morph and
change shape like Ed Interlock? Does it need a large set of
unusual ligatures like Infini?

_Variations_ Bold/Light/Italic/Shadow/Outline/Chromatic. Make a
variable axis font?

## Art and Iterations

Most of these steps involve at least some artistic judgement and
most of them will involve multiple iterations.
Possibly going back and forth between steps.
Some early-stage changes are more disruptive than others.
Small adjustments to shape (expecially vertically) won't disrupt
spacing.
But more radical changes to shape could affect spacing a lot,
and may involve re-doing kerning if that has been done.

## Scan

For scanning i would say get as many pixels as possible per
glyph. A glyph 100mm high when scanning in a 600dpi scanner
would be 2,400 pixels high which would be absolutely fine.
Similar a 21st century digital camera (or phone camera) should
be okay if you take one image per glyph, and you can somehow
arrange that the focal plane is exactly the object plane.

# END
