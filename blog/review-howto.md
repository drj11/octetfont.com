[brutal]: #title "How to Create Typefaces"
[brutal]: #author "David Jones"

The book is _How to create typefaces_ by
Cristóbal Henestrosa, Laura Meseguer, José Scaglione.
I have the English language 2017 edition.

A handy little manual and practical guide to creating typefaces.
Beautifully printed in two colours, black and teal.
The teal is used to good effect in chapter titles,
figure captions, diagrams and other occasional highlights.

The first chapter, _Motives_, and the last chapter,
_Perspective_, have a short essay from each author.
A joint brief and de-brief if you will.
But in between, except for chapter 3,
a single author takes the reins.

The book is fun, personal, and charming.
The authors are not afraid to let their character spill over
into their writing.
They are not afraid to admit that their path is but one path,
and not _the path_.
This gives the book an honest appeal.

I have a minor niggle which the inevitable outcome of its
physical size:
I wish there were more material on almost all of the subjects.
But that does not detract from what is there.

It includes some of the best illustrations I've seen.

## Table of Contents

1. Motives

From concept to type, the authors lay out their tables.
They give a hint of things to follow.
They include commercial aspects, such as scope, budget, and time
while at the same time recognising that some projects may be more
personal.

Henestrosa makes an interesting point about the users of a font.
The direct users of a font are designers.
But designers are designing for a _reader_ who are really
the ultimate user of a font.
Thus they connect the type to the message.
When someone designs type, who are they designing for?

Henestrosa also has some useful things to say about
where a typeface system will be used.
In an eye-catching display? In a book?
In an extensive system for a magazine and newspaper?
All very different tasks, in size as well as direction.

Scaglione talks about the relationship between typography and graphic design,
and typography as a separate discipline.
Then in a more practical introduction, outlines some of the parts
of fonts.

2. Writing, calligraphy, drawing, and type design (Laura Meseguer)

This is a good introduction.
What makes a font a font, and not just a sequence of letters.
What makes a font family a family, and not just a few fonts.
Emphasising calligraphy as the basis of letterform.
It is a very Dutch theoretical framework for making type.

3. Processes and methods (Laura Meseguer, Cristóbal Henestrosa, José Scaglione)

Mass, double-pencil, outline.
Laura Meseguer kicks off with no messing around.
The three ways to sketch.

Henestrosa: the lowercase, the spacing, the optical compensations,
the relationship between letters and their shared parts.
Good discussion of some of the tricks, both accidental and
deliberate, that type plays.
Not all the serifs are the same, strokes that “should” be
straight carry a gentle curve.
Henestrosa appends a useful short note on scheduling
(3 months for the first cut).

Scaglione talks more about theory,
setting a type’s design in the context of its intended use.

A double spread is reserved for a production workflow chart
for the _Adelle_ typeface.

4. Digitisation (Laura Meseguer)

The chapter isn't purely about digitisation, it's also about drawing.
And method, and modularity.
And optical compensation.

Good concrete advice about Bezier curves and the "good" curves
versus "bad" curves, and when to use a tangent versus a smooth
curve point.

The letters of the lower case latin alphabet are connected to
each by their design characteristics, and then, later, the same
treatment is applied to the upper case.
Good discussion and illustration of alignment and optical
compensation.

The illustrations throughout are excellent.

5. Spacing (Cristóbal Henestrosa)

This is a good chapter on the spacing of latin fonts.
Mostly horizontal, but does include a useful discussion of linespacing,
particularly with respect to accents and extenders.

The chapter opens by illustrating the difference between _fit_,
the ordinary spacing of letters, and _kerning_,
the adjustment need to optimally space pairs of letters,
like T and o, when a simple sidebearing will not do.

The recommendation is to fix the spacing of key letters,
like _n_ and _o_, early on, and
use those to test and fix the spacing of other letters.
Working outwards from a core group of relatively stable and
well spaced letters to the trickier and more unusual ones.

Once fit is settled, kerning can be done.
The advice is good,
save for a slightly esoteric selection of kerning pairs
(BV and Vx surely hardly ever appear in languages using latin
script).
The inclusion of kerning for letters with accents and for a
range of punctuation is welcome;
it is an area often neglected in fonts designed for a very
narrow English-only market.

One minor annoyance: references to specific page numbers in
_Elements_ are useless, as Henestrosa and I have different
editions.

6. Typographic programme (José Scaglione)

This chapter extends the notion of a font beyond a single set of
letters, and into a series of fonts linked by design.
From the basic roman + italic, to a family with a series of
weights, expansions, and so on.

The chapter is quite practical, explaining which parts of a
design will change and which will stay the same.

It goes on to discuss, more briefly, programmes in the wider
typoraphic sense like having a sans and and serif, and so on.

7. Typography as software (José Scaglione)

Since this is where I think my strengths lie, it is almost
inevitable that I think that this chapter is a bit light.
I suppose it's inevitable in a book this short that this subject
is only given a short treatment.

It introduces the possibilities, in an OpenType font,
of fonts that program the letters.
Necessary in scripts like arabic where letters change shape
according to their position in the text, and
the rules for changing shape cannot be made once and for all,
the rules have to be in the font itself.

Also opentype features for automatically correcting hyphen
heights for capital letters, switching to small caps and
non-lining numerals, and so on.

The possibilities are great and this chapter only scratches the
surface.

8. Distribution (Cristóbal Henestrosa)

The problem with characterful essays can be exactly that the
author’s character shows in the essay.
This chapter is about commercial exploitation.
It is very much written from one persepctive and embedded in the
current, colonialist capitalist system.
This makes it, for me, a problematic chapter to review.
Of course I think typographers deserve to be housed and fed, so
when in support of the current system the argument is advanced
that "the present regime may not be fair, but it is the current
one. If it happens that others obtain my work for free, I demand
the same treatment: free food, free home, free clothes" may
first response is yes, you deserve to be fed, homed, and clothed
for free, not because your type is good, but because we all
deserve that.

It is fair to say that I incline toward the opposing position.
But I am not writing a book for a general audience,
a book that might be expected to have a more balanced, or nuanced position.
Or at least sketch a map of what the terrain is.

Instead of all that, this chapter pins its flag firmly to one
edge.

9. Perspective
