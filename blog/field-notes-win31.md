[brutal]: #date "2024-06-04"
[brutal]: #author "drj"
[brutal]: #title "Field Notes: Windows 3.1 Edition"

Can we create new fonts for 20th century operating systems?
Short version: Yes! Use `fontmake`.


## Reminder on installing fonts in Windows 3.1

Use Fonts control panel to install fonts.

This is a bit annoying because i haven't worked out how to sync
folders in DOSBox, so if i copy new files into an already
mounted folder DOSBox won't see it. I have to quit and restart.

## Observations

Ranalina can be installed. (it is called _Ranalint_ in the
screenshots because i am using the development version which has
a different name from the production version).

Ubuntu Regular can be installed.

In Character Map Ranalina only displays the ASCII range,
but Ubuntu Regular seems fine.

<figure>
<img src="image/win31/RanalintWin311Charmap.png"
  style="max-width:84vmin"
  alt="A screenshot showing a sample of Ranalina font and its
  Character Map; it is clear in the Character Map that the
  entire bottom portion (after «~», at ASCII position 0x7E) is
  not available and is showing the `.notdef` glyph."
  />
<figcaption>
Note how the `.notdef` glyph occupies the bottom half
</figcaption>
</figure>


<figure>
<img src="image/win31/UbuntuRegWin311Charmap.png"
  style="max-width:84vmin"
  alt="A screenshot showing a sample of Ubuntu Regular font and
  its Character Map; most of the Character Map shows proper
  glyphs."
  />
<figcaption>
Note how most characters are available in Ubuntu Regular
</figcaption>
</figure>

Slightly to my surprise the `OS/2` table version seems not to
matter.
Ubuntu Regular has a version 3 `OS/2` table, and Windows 3.1
copes with that, despite the fact version 3 was specified well
after Windows 3.1.

The `OS/2` table is somewhat forwards _and_ backwards compatible
in the sense that newer versions generally only append fields,
and therefore Windows 3.1 code that "just grabs the bytes from
their known locations" will work even when there are more fields
(which will be ignored).


## Is it the cmap?

(yes)

I rebuilt `ranalina.ttf` by converting it to UFO with `tx` (from
AFKDO) and compiling the UFO to TTF with
[`fontmake`](https://github.com/googlefonts/fontmake) (from
googlefonts):

    tx -ufo -o ranalina.ufo -f ranalina.ttf
    fontmake -o ttf -f ranalina.ufo
    # outputs the font file master_ttf/ranalina.ttf

This version of Ranalina has a different Character Map:

<figure>
<img
  style="max-width:84vmin"
  src="image/win31/RanalintFontmakeWin311Charmap.png"
  alt="A screenshot from an emulated Windows 3.1 showing a
  sample of Ranalina font and its Character Map."
  />
<figcaption>
Note how that in the bottom half,
several characters are present
</figcaption>
</figure>

- the lowercase is missing, because Ranalina has no true lowercase:
  duplicate entries in the cmap map the lowercase letters to the
  same glyphs as the uppercase, effectively _double mapping_ the
  glyphs and giving them two unicodes. `tx` assigns each glyph
  only one unicode value when converting to UFO (despite that
  fact that both TTF and UFO support multiple unicodes
  mapping to the same glyph).
- various accented letters are missing, because Ranalina
  currently supports these by using combining accents
  (U+0300 COMBINING GRAVE ACCENT and so on), and
  does not encode the pre-composed forms.
- however, the remaining "bottom half" characters are shown
  where Ranalina has them, which is an improvement over the
  original Ranalina. For example Œ ± £.

## What cmap?

My (somewhat ad hoc) `seefont cmap` tool can give some more
insight into the `cmap` tables.
In the output below, RANALINA.TTF is the original Ranaline font
file; UBUREG.TTF is Ubuntu Regular;
RANA-FM.TTF is Ranalina rebuilt by `tx` and `fontmake` above.

    ; for a in ~/WIN31/RANALINA.TTF ~/WIN31/WINDOWS/SYSTEM/{UBUREG,RANA-FM}.TTF
    do
    echo ==== $a ====
    seefont cmap "$a" | grep '^[^ ]'
    done
    ==== /Users/drj/WIN31/RANALINA.TTF ====
    cmap,length, 2020
    cmap,sub,0,4,828,0/4 Unicode/BMP+non-BMP
    cmap,sub,3,1,28,3/1 Microsoft/Unicode-BMP
    cmap,sub,3,10,828,3/10 Microsoft/Unicode-UCS-4
    cmap@28 format 4
    cmap@828 format 12
    ==== /Users/drj/WIN31/WINDOWS/SYSTEM/UBUREG.TTF ====
    cmap,length, 1514
    cmap,sub,0,3,28,0/3 Unicode/BMP
    cmap,sub,1,0,992,1/0 Macintosh/Roman
    cmap,sub,3,1,28,3/1 Microsoft/Unicode-BMP
    cmap@28 format 4
    cmap@992 format 6
    ==== /Users/drj/WIN31/WINDOWS/SYSTEM/RANA-FM.TTF ====
    cmap,length, 430
    cmap,sub,0,3,20,0/3 Unicode/BMP
    cmap,sub,3,1,20,3/1 Microsoft/Unicode-BMP
    cmap@20 format 4

The TrueType `cmap` table contains a number of subtables each in
a particular format, which correspond to binary file
structures.
The subtables can be in different encodings (for example
Unicode/MacRoman/Shift-JIS), and different subtables can share
the same data as long as it's compatible. The last example in
the previous console output shows the simplest case of this: the
RANA-FM.TTF file has two subtables that in fact share the same
data.

The output of the tool is a bit cryptic, but an example should
clarify. From RANALINA.TTF:

    cmap,sub,0,4,828,0/4 Unicode/BMP+non-BMP

This (CSV formatted) row tells us that the `cmap` table has a
subtable of _Platform Code_ 0 and _Platform Specific Encoding_
4, starting at byte offset 828 (from the beginning of the `cmap`
table); the final part of the row summarises that as Unicode
platform with `BMP+non-BMP` encoding.
More details in the [TrueType Reference
Manual](https://developer.apple.com/fonts/TrueType-Reference-Manual/RM06/Chap6cmap.html).

## Discussion

The fonts which display "bottom half" Character Map characters
are UBUREG and RANA-FM and the common cmap subtables between
them are 0/3 and 3/1.
Which one is Windows using? RANALINA.TTF also has a 3/1
subtable, so why doesn't it work?
Is Windows 3.1 using the "Unicode platform" subtable?
It is confused by the presence of a Format 12 subtable?

More investigation required.

# END
