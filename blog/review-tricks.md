[brutal]: #title "Type Tricks"
[brutal]: #date "2021-12-04"
[brutal]: #author "David Jones"

_The Geometry of Type_ by Sofie Beier.
I have the 2017 BIS Publishers edition.

It is a handy sized paperback book with friendly rounded corners.
Printed in 3-colours:
Black, a silvery marine blue, and a hilarious fluorescent pink.
The pink is used to good effect as a highlight in diagrams and a “sticker
effect”,
but unfortunately it is also used for captions that identify fonts and
their creators, and it is too small and too light to read comfortably in those
captions.
That pink caption niggle is however one of only very few negative things i have
to say about the book.

The cover declares “200+ tips” which undersells the book a little bit.
It is more than a collection of tips to create type.
The tips and tricks have been organised into a coherent program for creating a
font (and possibly font family) from scratch from start to finish.

The book is surprisingly comprehensive for such a compact book.
It _does_ have advice on ampersands, ogoneks, small caps, bold and italic
versions, kerning, the overlapping corner trick.
Almost everything you could want, and certainly enough to get you started.
It's easy to flick through and find something relevant to the type design problem
you are trying to solve.
Being compact however, the advice might be limited to a single sentence or
illustration.

Fun and useful at all stages of making type.
Recommended.

# END
