[brutal]: #title "A coding font"
[brutal]: #author "David Jones"

What font should i use for coding?

As a programmer turned type designer i’m asked this question
often enough that i should have some sort of answer.

I don’t, but here in this article i can put down some thoughts.

Obviously the _actual_ answer is “whatever you like”, but
here are some aspects that you might want to think about when
choosing.


## Why is this even a question?

Computer programs are still generally organised as a collection
of plain text files randomly scattered across folders.
I use _plain text files_ to mean that they do not
have typographic formatting themselves.
The code of the program is just a sequence of characters.
It may have been written in 15-point Comic Sans, but
once it is saved into a GitHub repo,
that fact has been lost and the code is just letters,
with no formatting.

Unlike a Word document, where the document i see uses the font
that you chose when you wrote it.

So when i open code in my code editor,
i see the formatting that i’ve set up in my editor.
At least for font-choice, size, colour of comments,
and so on.
Editors vary a lot.
From notepad to VS Code to plain old vi in a terminal.

If you use an editor in a terminal (like i do), then the editor
won’t choose the font, but the terminal emulator will;
although the editor may still be able to use font variations like
reverse video, bold, italic, underline.
In any case the programmer looking at the code gets to choose
the font, even if that “choice” is to use the default one
provided with VS Code.

Programmers like being distracted, and spending a day choosing
your _theme_ and picking fonts and sizes and colours is A Thing.


## Related questions

A related question, but not entirely the same, is

What font should i _display_ my code in?

Commonly this will be a presentation, but
might be a worksheet, a book, or a poster.

Many of the same considerations will apply, but i would say that in
general you will be working at higher resolution (even a presentation
on screen is likely to display fewer lines, and therefore use more
"pixels per line"), and there is more opportunity to consider
aesthetic over utility.


## Mono, the hipster fixie bicycle of the typographic world

The first choice is probably monospaced or not.
A monospaced font has all of its glyphs the same width so that
they can be laid out in a grid.
If you use a terminal emulator then you pretty much have to use
a monospaced font.
If you use a fancy editor like VS Code, then maybe not.
However,
it remains so common to use a monospaced font that you probably
didn’t even think about not doing that;
and, even though Pike himself uses a proportional (not
monospaced) font, the [Go Playground](https://go.dev/play/), a
web app which could easily use a proportional font,
defaults to using Menlo, a system-installed monospace font on
macOS.

In _presenting code_ there is no particular reason to use a
monospaced font, but
there is a typographic expection.
People watching presentations that mix code and commentary will
generally expect the code to be in a monospaced font.


## Legibility

For me, one of the important things about programming is legibility.
Ideally, each and every glyph should be clear and distinct from
the others.

Monospaced fonts have some aspects which help this, and some
which hinder.
For example, it helps that dot, comma, colon, all have the same
width.
It gives them plenty of space.
But that same space is also used for chinese ideographs and
emojis, which generally look incredibly cramped.

Which brings me to:


## Glyph coverage

Most programmers probably spend most of their time looking at
code written in English or another language using the Latin
alphabet.
It’s probably okay to use a font that doesn't have a gigantic
script coverage.
Though if you read and write lots of comments in Devanagari then
you need a font which covers those.

There is a bit of a paradox here.
The vast majority of programs are written in ASCII, so
the font won't need huge glyph coverage.
On the other hand, often in strings, but also increasingly in
identifiers in programs, non-ASCII characters are valid, and
seen.
So they should be shown sensibly in the editor.
The [Go playground](https://go.dev/play/) switchs to another font
for characters outside the range of its default font, as the
default example shows.


## The truth

Another important principle for me is The Truth.
In the context of programming it can sometimes be important to
know the exact contents of a program.
If the program i’m given has the code `if x == "splat"` where
_splat_ is some funny character not in my font,
i’d rather that the character be drawn from a different font, or
i have a box with the unicode hex value inside it, than
a `.notdef` glyph or the unicode substituation character.
Even if the that looks ugly.

There is conflict with some typgraphic and informatic principles
here.

How come the following is False?

    >>> len("ŵ") == len("ŵ")
    False

(by the way, by the time you see this program, it will have gone
through so many transformations that it might well be that if
you cut and paste into Python you get a different result)

The answer is because the ŵ on the left i got by typing `w`
followed by `Shift`-`Alt`-`6` (circumflex), because i use
Apple’s ABC Extended keyboard and that's how you type ŵ.
The one of the right i got by typing `Ctrl`-`Shift-`u` `175`
`Return` because that's the ISO 14755 sequence that Kitty uses
(my terminal program).

It turns out that results in two different unicode strings.
The first is U+0077 (w) U+0302 COMBINING CIRCUMFLEX ACCENT;
the second is U+0175 LATIN SMALL LETTER W WITH CIRCUMFLEX.
So the first string has 2 unicode codepoints, and the second
string has 1.

Should these strings be the same string? Well, no.
Unicode specifies various normalisation algorithms which will
translate these strings to each other, but
in general should Python do that? No.

For one thing, what if i am writing such an algorithm in Python?
I am for sure going to want to reliably represent those two
different strings.

Combining accents in Unicode are just one way to get two things
that look the same, but are different (Wikipedia calls these
_homoglyphs_).

Most obviously things like the Cyrillic **A** and the Latin **A** are
the drawn the same.
For a font that has both, you would generally expect the
Cyrillic alphabet and the Latin alphabet to be drawn in the same
style, and therefore the **A**s should match.
Internally to the font they may even use the same glyph or
drawing instructions.
I think there’s a case to be made that the Cyrillic **A** and the
Latin **A** are _the same glyph_ that is used in different alphabets,
and it’s an accident of computer encoding that they have separate
Unicode codepoints.

Which is why we get two different values in this Python code:

    >>> ord("A"), ord("А")
    (65, 1040)

So, like Word has a "show hidden" feature to visible mark spaces
and tabs and things, perhaps editor should have a "more truth"
option.


## Nearly the same

It was common on 20th century typewriter to economise on keys
and print-heads so if you could get away with not having a `1`
key and using an `l` instead, that was fine.

[image of pitman's poster here]

It's very _not fine_ in a computer program though.

So what glyphs are confusable in the context of computer
programs?

- 1 (DIGIT ONE) l (LATIN SMALL LETTER L)
- 0 (DIGIT ZERO) O (LATIN CAPITAL LETTER O) Ø
- , (COMMA) . (FULL STOP)
- : (COLON) ; (SEMICOLON)

These are the common ones, but there are plenty more.

DIGIT ZERO must be distinguished from CAPITAL LETTER O.
The usual methods are either change the shape or add a slash or
a dot.
The shape of ZERO is made more super-elliptic,
squarer, or more diamond-like. Sometimes it's the other way
round and LETTER O has the more unusual shape. It's annoying
that it's not consistent enough to be reliable, but if you're
staring at the same font all day, it won't matter.
A slash is common but can be problematic, it brings the 0 (ZERO)
close to Ø (LATIN CAPITAL LETTER O WITH STROKE).
In Menlo, which i am using to prepare this document, the
only different is that in the LETTER O WITH STROKE the stroke goes
outside the ellipse.
That's a very small different to perceive at typical screen
sizes.
A dot in the ZERO may be preferred then. Or, slightly more
radically, a truncated slash that does not touch the sides.

Occasionally you see the negative space being used, the ZERO
will have a cutout where a vertical or a diagonal stroke would
have been.

You _could_ change the height of the numbers.
I’ve never seen a monospaced coding font use lower-case or
three-quarter height numbers, but that would be an option.

The trouble with COMMA and FULL STOP is that the difference might
be too small.
For coding fonts it might be a good idea to make them both as
big as you can get away with and draw the comma notched.
Which may then mean that for the letters with a COMMA accent
that you need to draw a different comma.
Or, you know, just have gigantic accents.

COLON and SEMICOLON has the same problem as COMMA and FULL STOP.

My recommendations are:

- DIGIT ONE drawn with a diagonal flag (not horizontal) and
  feet either both sides of the stem, or not at all.
- LATIN SMALL LETTER L drawn with right-hand foot at bottom of
  stem and optional leftward flag at top.

While i’m here brackets, parentheses, and curly bois should all
be drawn clearly different from one another and as large as is
reasonable;
i recommend extending past capital letters at both top and bottom
(which is fairly common anyway).


## Frequency

In a lot of programs the frequency of some glyphs will be much
higher than typical text, and so the important of having clear
distinct glyphs will be higher.

One example is «~» TILDE
(it’s very common in a lot of C programs).
In some fonts it's drawn with a swing that is too small and ends
up looking like «-» at typically used sizes.
Particular awkward in C programs as they are both unary
operators.

TILDE is also a good moment to talk about


## Vertical Positioning

In computer programs «~» and «*» (TILDE and ASTERISK) are more likely
to be mathematical operators than accents or emphasis markers.
Mathematicians and normal people use «×» to mean multiply, but
computer programmers have gotten used to using «*» because
the character repertoire of early information systems didn’t have
«×».

That means that «~» and «*» should probably be position in the middle
vertically, so that it "ligns up" with «+», «-», «=» and so on.

Technically, quality mathematical notiation has a high «-» for
negative (as in -7, the unary operator case), and
a median «-» for the binary minus operator.
Computer programs commonly use the same symbol for both.
Maybe you think it would be cute to use a high «-» for the unary case
and a median «-» for the binary case? Well, that leads me to...


## One symbol for one value

It‘s hard enough to work out what the computer is doing and is
supposed be doing without inserting an extra layer of "untruth",
so i recommend as far as possible to have one symbol mean one thing
and for one thing to be represented by one symbol.

If the computer program notation for functions is `fn x -> x` do
not rewrite that using fancy formatting and ligatures to `λx→x`.
Because the second thing is also a valid Unicode sequence
(or looks like one), and now you can’t tell the difference.

[this was really an opportunity to rant about coding ligatures]

# END
