# 2021-W48 2021-12-02

It's so dark.


## Linocuts!

Did a 2-colour robin. Encouraged P to do a tree.


## Lino fonting

Still stalled.


## ILT

Still no movement on this. I need to ping them.

But ILT did do a big Bold Friday (Black Friday) thing,
so there were probably super busy.


## Afublo

Found the **O** from Benguiat Buffalo, and i liked it.
I traced it into a font and developed some of the rest of a font based on it.
12 letters with a few alternates so far.
It's squarishly rounded, and i like it.

Design grid is 2520 UPM, cap-height is 2000.
Everything seems so huge numerically.


## feax

`feax` (Feature Extract) can now extract feature files from
(some) real in-use fonts.
This has required a lot of expansion of the `opentype` library to
handle various lookup types.


## plak

I would _like_ to add a `--blank` option to plak so that i can
exclude the blank glyphs.

It requires a more sophisticated glyph tool
(in style of `seefont glyph`) that can handle CFF fonts.


## Fonts

`font-avimode`

Various pre-production changes.
- UPM reduced to 833 without changing cap-height
  (to enlarge the font and eliminate Line Gap);
- Redraw SharpS;
- Add examples;
- Removed dot from the i+accent combinations;
- Adjusted some previously forgotten kerning values.


`font-4mod`

Preliminary sketches for a font based on Tetris pieces.
Required some expansion of the PyPNG library.


`font-airvent`

Drawn a small caps, currently on the lower case.

- Upper case: Spaced but not kerned.
- Small caps: not spaced.

Sketched a preliminary idea of making a "riveted" version
that has holes punched into it (like Meccano).


## END
