[brutal]: #title "Half an alphabet"
[brutal]: #date "2021-12-03"
[brutal]: #author "David Jones"

Half an alphabet is a good place to pause and reflect.

There should be a good sense of what the font is about.
It's early enough that what is done can be changed, and
there is much yet to be decided.

Some of the to-be-drawn letters will be easy, some will be hard,
some will fall in the middle.

Take this alphabet that is partially complete.

<figure>
<img src="image/half/alphabet.svg">
<figcaption>
Some good letters but not a whole alphabet
</figcaption>
</figure>

We can already say a lot about the font.
It is a bold and wide sans-serif;
The **O** and other round parts of the font
are distinctly square;
there is almost no contrast in the usual
horizontal and vertical strokes which
are nearly identical in thickness,
except for the middle crossbars (**B** **E** **H**)
which are thin;
terminals are blunt cut, but softly rounded,
the terminals facing up and down being
slightly rounder than horizontally facing terminals;
there is the slightest hint of an axis (NE to SW)
in that generally the NE and SW corners are pulled
to be slightly more square than on the other diagonal.

But we can't say everything.
We don't know what the **S** or **K** will look like
(actually i am drafting the **S** now).

But we know more about what the **T** will look like,
even without drawing it.
The **T** will be easy to draw.
It will have a horizontal stroke from the **E** and
a vertical stroke from the **H**.
Which leaves only the overall width, and
the symmetry of the side strokes to be decided,
and some minor fussing over the terminals and joins.

<figure>
<img src="image/half/t.svg">
<figcaption>
A rough T drawn from pieces of E and H
</figcaption>
</figure>

We can say a little bit about **G** but not everything.
The **G** is a middling case.
It will look more-or-less like the **C** with some sort of crossbar/spur added.

<figure>
<img src="image/half/g.svg">
<figcaption>
A crude extension of C to G
</figcaption>
</figure>

The **K** is harder, in the sense that right now we know less about
how it might look.
The vertical stem of the **K** will probably look much like the **H**;
but for the rest of it,
there are fewer parts we can re-use from already drawn letters.
There is more to decide and more to draw.
Will the stroke of the arm be substantialy thinner than the leg?
Will the leg and the arm join at the midway point, or higher?
Will the junction be attached to the vertical stroke?

<figure>
<img src="image/half/SomeK.svg">
<figcaption>
A slection of K
</figcaption>
</figure>

Personally i like to match my mood and energy to the task ahead,
so i like to divide up the work a bit.
If i'm tired and lacking inspiration i'll work on developing the
easier letters, **T** and **I**.
If i feel like i can take on a challenge, i'll do **K** or **S**.

If i feel indestructible, i'll do **&** or the numbers.

The numbers shocked me a little bit when i first tried drawing them
in a proper font.
Mostly they look nothing like the letters.
The 2 has an arc joined to a diagonal line.
The 7 has a diagonal stroke that
might be curved even when nothing else is.

The reason is of course the descent of numbers.
The present-day numbers matched with latin fonts are
descended from arabic descended from indic.
The present-day latin letters are descended from
Carolingian bible script (lower case) and
from Roman monumental inscriptions (upper case).
Different origins.

You can approach this in terms of information or
principal components.
Given an **E** and an **H** could you predict a **T**? Yes,
**T** brings little new information, it lies in the
_plane_ of **E** and **H**.
What about **S**? you can predict some things about it, but
not everything.
It resolves into components, one of which is in the **E** **H**
linear subspace, the other components being orthogonal to it.

It helps to be deliberate and mindful at this stage in plotting
the future drawing.

# END
