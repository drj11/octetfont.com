[brutal]: #title "A few good ampersands"
[brutal]: #date "2021-05-19"
[brutal]: #author "David Jones"

The ampersand is essentially a ligature for _et_, the latin for _and_.

The usual form for this is a figure 8 with a crossed pair of tails on the
lower right.
Let’s have a look at some.

(the fonts in this article have all been chosen because
they are bundled with macOS, or are openly available;
while the selection is intended to be representative of the common forms,
it’s not particularly systematic.)

Baskerville is typical.

<img src="image/ampersand/ampersand-baskerville-regular.svg">

Most sans serifs use the same shape, but without the serifs.
Here modelled by Avenir.

<img src="image/ampersand/ampersand-avenir.svg">

Because i have a maths degree, i see that this form encloses 2 holes and
therefore has topological genus 2.

The reader shall indulge me in the briefest introduction to topology.


## Topological genus, the briefest introduction

Topology studies to what extent we can smoothly deform one object into another,
and _topological genus_ is a mathematical system for
describing objects with similar topology.
More simply, it’s the number of _holes_ an object has.

<figure>
<img src="image/genus.svg">
<figcaption>
Blobby objects of genus 0, 1, and 2 respectively (from left to right)
</figure>

It can get a lot more complicated that that, but i [refer you to Wikipedia,
if you want more topological complexity in your
life](https://en.wikipedia.org/wiki/Genus_(mathematics)),
because that’s cheaper than a copy of
_Algebraic Topology: A First Course_ from
Springer’s Graduate Text in Mathematics series.

When I say, above, that the Avenir _&_ has genus 2,
I mean it has 2 holes.
If i made it out of some imaginary deformable material, i could squash it
around, without making any new holes or closing existing ones, to get the
“egg with 2 holes” shape above.

## Counters, closed & open

In typography, a hole inside a shape is called a _counter_.
In the metal era of type, a _punch_ was made for each letter, the
holes in the punch were made with a _counter-punch_.

Counters in a shape can be closed, as in _O_, or open as in _G_.

Here we see PT Sans modelling the counters in
_&_ (2, both closed) and _a_ (2, one open and one closed).

<figure>
<img src="image/counter.svg">
<figcaption>
Counters are shown in blue
</figure>


In this 2-counter form, the top counter is teardrop shaped, pointed down
and slightly to the left or sometimes straight down;
the bottom counter is D shaped.
Both the counters are closed, making this a topological genus 2 form.

This is the most common form, but
there is considerable variation in the angles and terminals of the tails.


## Italic variant

The italic style is, in ordinary serif font families,
not just the regular form drawn with a tilt;
it’s based on a different model.
For this reason, in many italics, the ampersand has a complete different form, and
shows more clearly its _et ligature_ origins.

Sans serif families do tend to have italics that are
more like the regular font tilted over.
Most typographers use the term _oblique_ for this, rather than _true italic_.
So, we see this alternate form of ampersand more commonly in serif families than
sans serif families.

A typical italic form is modelled here by Baskerville Italic.

<img src="image/ampersand/ampersand-baskerville-italic.svg">

In Baskerville Italic (above) the swash terminals on the right-hand _T_ part
are perhaps a little bit more flamboyant than average, but
still well within the acceptable spectrum.

Palatino Italic is similar:

<img src="image/ampersand/ampersand-palatino-italic.svg">

Trattatello (bundled with macOS, marketed as
[P22 Operina](https://fontsinuse.com/typefaces/11114/p22-operina) elsewhere) is
based on Arrighi’s _La Operina_ pamphlet, and shows a clear correspondence to
how one might write _et_ in handwriting:

<img src="image/ampersand/ampersand-trattatello.svg">

This italic form is open, and could be thought of as a topological genus 0
form.

## Genus 1 forms

There are a couple of genus 1 forms, not related to each other.

Apple Gothic has an ampersand which is the genus 2 form, but
with the top counter made open.
Courier (and Courier New) and Pria Ravichandran’s Catamaran have a similar form.

<figure>
<img src="image/ampersand/ampersand-apple-gothic.svg">
<img src="image/ampersand/ampersand-courier.svg">
<img src="image/ampersand/ampersand-catamaran.svg">
<figcaption>
From left-to-right: Apple Gothic, Courier, Catamaran
</figcaption>
</figure>

Topologically speaking, Trebuchet has the same form (same genus), but
it is more upright.
It has a very clear connection to _et_:

<img src="image/ampersand/ampersand-trebuchet.svg">

The other genus 1 form is based on the lower case _e_.
This is used by Cochin and the formidable Hoefler Text,
both in their italic styles:

<figure>
<img src="image/ampersand/ampersand-cochin-italic.svg">
<figcaption>
Cochin
</figcaption>
</figure>

<figure>
<img src="image/ampersand/ampersand-hoefler-text-italic.svg">
<figcaption>
The formidable Hoefler Text
</figcaption>
</figure>


# Oddities

Skia, from Apple, on the face of it has a fairly ordinary genus 2 form, but
at the top crossing the strokes do not line up, producing a dog-leg effect.

<img src="image/ampersand/ampersand-skia.svg">

Unlike the usual form, this one cannot be drawn with a single stroke of a pen.

Apple Chancery has a genus 3 form which seems almost accidental.
It’s essentially the genus 2 form with an extra flamboyant swash that
connects on the left, to make a new closed counter.

<img src="image/ampersand/ampersand-apple-chancery.svg">

Then there’s Zapfino.

<img src="image/ampersand/ampersand-zapfino.svg">

Thanks Zapf.


# END
