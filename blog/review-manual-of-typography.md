[brutal]: #itle "The Thames and Hudson Manual of Typography, a review"
[brutal]: #author "David Jones"

You still find this book in current lists of recommended reading
on Typography.
I came across this book in the 1990s, but didn't get around to
buying it and reading it until now.

I have the 1980 edition.

A lot has changed, and it is fair to say that some parts of the
book are of historical interest.
Most of the material is foundational and still relevant today.

I find the book wonderfully practical and assured in the advice it gives.
It appears to be written for someone who is approaching their
final year in art college and wishes to get a job as a real typographer.
Either the designer of the parish magazine, or
the chief book designer at Penguin, or
possibly the senior typographer of a british national newspaper.

Starting with a history and some practical and theoretical
matters, the book then proceeds with practical advice of
choosing type, printing methods, choosing papers, and designing
books, business cards, invoices, and newspapers.

The chapter titles give a good idea of the coverage:
Historical outline; Studio and equipment; Legibility; Lettering
and calligraphy; Letters for printing; Methods of composition;
Paper; Cast-off and layout; Book design; The parts of a book;
Jobbing typography; Newspaper and magazine typography.

_Studio and equipment_ is written before desktop computers,
so the advice seems quite charming
(use a photocopier to keep a copy of all your business correspondence).
Designers still like to draw on paper,
so the nice drawings of example desks could still be useful.
It seems that one of the modern problems is that a jobbing designer
might be expected to have a desk both to draw at and
to have a desktop computer.

_Legibility_. This chapter may be responsible for one of the
most repeated myths about legibility and type: that sans serif
typefaces are less legible than serif typefaces.

http://asserttrue.blogspot.com/2013/01/the-serif-readability-myth.html?m=1
gives a blog-style debunking
(short version: Cyril Burt made up his 1955 article), and
buried in Microsoft's documentation for
developers of OpenType fonts is
this useful and reasonable modern summary:
https://docs.microsoft.com/en-gb/typography/develop/word-recognition

Putting the scant and dodgy evidence to one side,
it still should be obvious that text can be
more legible or less legible and that
this influences and is influenced by design.
So the subject is worth discussing, and
it is essential that designers develop
an understanding of it and a critical eye.

The following couple of chapters are about letterform and are
delightfully illustrated.
I find them to be some of the most graphically interesting material.
There are useful comparisons of typefaces, for example:
the “same” typeface from different foundries;
Fonts of different sizes from the same typeface blown up to the
same size for comparison.

Although the book was published as
phototypesetting was becoming popular,
a period of truly dreadful typography from which we are only
just recovering now in the 21st Century, most of the material is
still useful.
Even notes about how the "etch" in phototypesetting needs to
compensated for have modern analogues: material typeset on
computer is likely to be printed in distant factories on cheap
paper by people who simply don't care.


_Methods of composition_ holds up poorly.
Discussions of how the Monotype's keyboard paper tape was fed
_backwards_ into the typecaster are amusing and interesting, but
in the 21st Century that interest is historical rather than practical.
Those remarks also hold for "Castoff and layout".
There is some use in being able to roughly estimate how many
pages or columnn millimetres some text will take when typeset,
but in all likelihood you're going to be doing this fiddling
around at the computer and it will do most of that work for you.

_Paper_ is a chapter about paper.
How it is made, the different qualities the methods and materials give.
Why you might want to use a soft paper, a shiny paper, and so on.
It feels like the information is antiquated, but I have no idea.
Certainly many of the particular methods that are discussed have
probably been modernised, made more cheaply, worse, and more
widely available.

When I reached the chapter _Book design_ I felt a real turn in
the narrative. It is slightly over halfway through, and I felt
that the first half of the book had been essential theory and
foundational matters, and now it was time to apply that learning
to the real matter at hand: making a book.
The book itself is a good example of the principles described in
this chapter.
There are also a good number of examples from other books to illustrate.

_Parts of a book_ is really a extension of the book design
chapter into matters that otherwise might get overlooked
such as the title page and the jacket.
Concerning jackets, there is some amusing advice that I've never
seen elsewhere. If you like the jacket of a book you should
remove it and fold it in the back of the book, or mount it on
your wall.

The later two chapters, _Jobbing typography_ and _Newspaper and
magazine typography_, are even more practical and focussed on
the kinds of jobs that early career typographers are likely to
be doing.
Working for private families that have designers stationery and
jobbing at a local newspaper.
