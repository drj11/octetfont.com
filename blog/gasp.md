[brutal]: #xdate "2024-05-09"
[brutal]: #author "drj"

# Some notes about `gasp` table

Using `lifd` to show tables of all my currently installed fonts
(not a huge number).

Many fonts do not use `gasp`.

Of the fonts that use it, there are 4 sizes and 2 versions:

- version 0, size 8
- version 0, size 12
- version 0, size 16
- version 0, size 20
- version 1, size 8

The size of a `gasp` table is 4 + 4*n where <var>n</var> is the
number of entries.
Each entry is a uint16 PPEM size, and 16 bits of flags

Version 1, 1 entry: 65535,0xf
modern style, all gasp bits on for all sizes. For example DarkAcademia-Regular.ttf

A variant of this has 65535,0xa (cleartype smoothing and
grayscale, no gridfit). Seems to be only used in obscure
situtaions: Apple Braille and Khmer Sangam MN.ttf and Lao Sangam MN.ttf

Version 0, 1 entry: 65535, 0x2

Only Equestria uses this. Grayscale at all sizes.

Version 0, 2 entries: 8, 0x2; 65535 0x3

Used by NotoSansMongolian-Regular.ttf and
NotoSansTaiTham-Regular.ttf only

Version 0, 3 entries: 8, 0x2; 16, 0x1; 65535 0x3

A classic and its variations.
The TrueType documentationi favours this: grayscale for sizes 8
and below; gridfit (but no grayscale) for intermediate sizes up
to 16; and both grayscale and gridfit above.

Although the switch at 16 is suggested by the TrueType
documentation, and is the most popular, other choices are
exhibited by a range of fonts.
My ad hoc selection shows values from 10 to 36, for example
Ubuntu-Regular.ttf uses 18.

Version 0, 4 entries

There are two variations, both seem questionable:

Ubuntu-LightItalic.ttf 8,0x2; 26,0x1; 100,0x3; 65535,0x2
(why bother switching back to grayscale only after 100 PPEM?)

Webdings.ttf (and ingdings.ttf) 8,0x2; 28,0x1; 2048,0x3; 65535,0x0
Arguably following the TrueType document even more closely,
suggesting flags optional above 2048 PPEM.
