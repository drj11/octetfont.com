[brutal]: #title "Skate or Type"
[brutal]: #author "David Jones"

_Where i steal skateboarding terms and re-use them for type._

- aerial: entirely above the cap-line
- backside: letter faces the wrong way, or the back side of the
  glyph (left-hand side for latin glyphs).
- bail: trashing a glyph before you've finished it, and
  not even keeping a copy in that folder of unfinished dreams.
- bearings: ball serifs or any other wheel shaped
  terminal/decoration
- boardside: dropping your A or H so that the crossbar sits on
  the baseline
- bonk: nose of P touches adjacent letter (or similar)
- bowl: the skateable parts of O, D, P, and so on.
- Caballaerial: just the best.
- carve: the sexy part of n, where the curved part joins the
  vertical stroke.
- coping: reinforcing the edges.
- crooked grind: strongly asymmetric design on the lip.
- crew: your type design people.
- disaster: accidentally left bad-orientation outlines in the
  OTF and falling foul of the non-zero winding rule.
- drop-in: (as in skate) abrupt transition from horizontal to
  vertical curve.
- durometer: device to measure hardness of urethane,
  a polymer used in inking rollers.
- faceplant: when font production has gone _horribly_ wrong.
- fakie: setting a left-to-right script right-to-left or vice-versa.
- flat / flatbottom: When U has a flat bottom.
- frontside: the forward side of the glyph in its script's usual
  typesetting direction (right-hand side for latin glyphs).
- goofy-foot: drawing with the opposite foot-stance to Kis.
- grip-tape: adding texture to your font to increase grip.
- half-pipe: U. Better to use this when there is more pipe than vert.
- hand-plant: arm reaches baseline instead of foot.
- hang-up: a bump in the transition.
- hardflip?
- hardware: the things that hold your font together, also
  the thing you use to make them.
- heelflip: upside-down serifs on feet.
- kickflip: swapping the frontside and backside serifs.
- kneeslide: sliding on the knees (of a K).
- ledge: a horizontal part of an outline longer than
  the usual stroke thickness.
- line: a number of features in the same stroke.
- lip: the edge of a transition, may have coping / drop-in.
- lipslide: a straight part on the lip.
- manual: that sweet spot on the spine of the S.
- mongo foot: pushing on the frontside.
- nollie: popping the frontside on the baseline.
- nose: frontmost part.
- noseslide: a flat part on the nose.
- ollie: popping the backside on the baseline.
- pocket: (as in skate) the concave part of the nose or tail.
- pump: putting more energy in the transitions.
- quarter-pipe: half a half-pipe, in isolation or considered in
  isolation.
- rail: a vertical part of an outline longer than
  the usual stroke thickness.
- razor tail: sharp bits on the backside.
- regular-foot: drawing with the same foot-stance as Kis.
- revert: making a backside but keeping the feet on the
  baseline.
- riser pads: extending height by adding material just above the
  serifs / terminals.
- rock n roll: going up to the vert, then back down again.
- session: getting together to draw.
- sketchy: poorly executed. Unlike _sketches_, which are wonderful.
- slam: where a downward stroke meets the baseline (possibly
  with not enough transition / serif / decoration).
- snake: where one stroke overtakes another and cuts-in (in 
  Arnold Böcklin).
- spot: anywhere you can do type.
- stall: hanging on those transitions.
- switch-stance: drawing with your stance switched from your
  usual one.
- tail: backmost part.
- tailslide: flat part on the tail.
- tic-tac: a lot of wiggle in your strokes.
- transition: the bit between the parts that are horizontal or vertical.
- trucks: outlines added to aid design that
  you can't see in final OTF.
- varial / shove-it: rotating the letter (about an axis
  perpendicular to its plane).
- wheelbase: shorter wheelbase means tighter turns.
- wheelbite: bad intersection somewhere in a variable fonts
  variation space (too much weight, bad loops, and so on).
- wheels: wheels.

# END
