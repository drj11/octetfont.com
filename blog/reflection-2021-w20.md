# 2021-W20 2021-05-20

Everything is an experiment.
Try something and see if it works.

## Fonts

`font-amelt`

I had read the Poem Editions pamphlet by Éloïa Pérez and tried their
template to draw an uppercase alphabet.
It's quite interesting, particularly to take a more considered typographic
approach to it.

The following days i hand–eye traced it into digital form using Glyphs Mini.
That font is `Amelt` https://gitlab.com/drj11/font-amelt/.

Subsequently, i also made a lower case and a sort of gaellic/uncial.
Making this the first font i have created with an upper and a lower case
(and it has a third, uncial, case).

The template is good though i am doubtful that it can inspire substantially
different fonts (it may be worth comparing to EP's work).

I quite like the combination of rounded elements suggesting a child-like
setting, but with a typographical eye that removes some of the more
child-like elements.

Compared to other recent projects, `Amelt` has proceeded rapidly so far.
The numbers were horrible to do (due to the constraint of only using
circles of diameter 550 or 450).

`font-asiren`

Another, very brief, tracing experiment, after finding Quick/Sirena on
fontsinuse https://fontsinuse.com/typefaces/31841/quick

It is a calligraphic script that is fairly regular and formal in style.
It has an extremely large cap- to x-height ratio, with high ascenders
(not as high as the capitals).

Tracing just a few letters, in this case CubicType, is a good way of seeing
whether i like the font, like the tracing, and the source material is
suitable.

## Blogs

I tried to draw an ampersand for _Amelt_ which was terrible.
Karen Cheng has no advice about ampersands in «designing type», so
i looked at a few different ampersands.

Wrote a [blog post about
ampersands](https://home.octetfont.com/blog/ampersand.html) then i draw a
better ampersand.

My current advice for drawing the usual 2 counter form is:

- Stack two circles vertically, with the bottom one touching the baseline and
  the top one toching the cap-line.
  The circles may overlap, touch, or be separate, according to style, but
  usually the top one is smaller than the bottom one.
  The left-hand edge of the top circle will usually be to the right of the
  left-hand edge of the bottom circle.
- Draw a diagonal stroke that cuts the bottom circle and is tangent to the
  top circle.
  The counter of the bottom circle is usually substantially bigger than a
  half the circle.
- Remove unused parts of the circles, add a crossing stroke to make the
  upper teardrop, and add tails.
- Adjust.

The problem of how to draw an ampersand was resolved by:

- study existing form;
- evidence the study;
- draw form.

I shall continue to use this.

I used Affinity Designer to do the two diagrams in the blog post.
The learning curve is, like Hoefler Text, formidable.


## Reading

I have read EP's pamphlet (see `font-amelt` above), and that was
interesting.
There is some interest in how to introduce children to writing, the
alphabet, and typography.
Often with physical artefacts and manipulables.

I continue to read _designing type_, having now “finished” the chapter on
serif lower-case.
My previous (positive) comments still stand.
It continues to show the mainstream approach and is useful for that.

## Tools

A plaque tool is needed.
There is a crude implementation in font-amelt.
A more serious implementation would

- analyse font file to determine available characters
- intersect with requested set (e.g. latin upper case)
- generate random sequence
- render and display

# END
