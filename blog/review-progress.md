[brutal]: #title "In Progress"
[brutal]: #author "David Jones"

I love every page of this book. Hische invites the reader into her studio and her work. She gives a delightful and insightful tour of her tools, process, and work. From sketch to screen to final art as delivered, and often the art as it appears in the final product or advert. Hische drops little tips about cameras, printers, and Adobe tools; she clearly loves sharing the way she works, and wants us all to do the best work we can.

Roughly the first third of the book is process and tools, leaving the larger remainder for examples of Hische’s work, which serves multiple roles: specimen catalogue, inspiration, and workpiece-focussed process insight. It's a fun book both to read and to flick through.

The book as an object—at least the hardback edition that I have—is well made, with good quality paper, a stitched lay-flat binding, and a 5th ink for the printing: a lovely metallic grey that gives a hint of pencil graphite to the sketch reproductions.

One of my favourite bits is where Jesscia Hische opens her pencil case for us. I have now got the same pencils and the same pencil sharpener!
