# 2021-W23 2021-06-10

No reflection for previous week, W22.
That was very tiring.

## Fonts

`font-atwin`

Minor activity filling in some accents and analphabetics.

Studied breve using TextEdit.
Support among common fonts is variable,
for example the formidable Hoeffler Text has abreve, ebreve, and
ibreve, but not gbreve, obreve, and ubreve.

Avenir Next has a- e- g- i- o- u-breve, but
on the i the breve is positioned lower than the other letters.
This seems to be a pecularity of the i,
the dot is positioned at a common line for accents above, but
other accents on i are positioned slightly below.

Drew an asterisk,
which was slightly difficult because
getting the 6-fold symmetry right was frustrating.
A lot of the frustration was that partly due to drawing and
partly due to integer grid, you cannot draw a single arm
and rotate it around.
Or at least, i can't.
In the end proceeded by approximating the 6-fold symmetry,
then merging overlapping pieces,
then cutting it out to the North-East corner,
then copying that by reflection in H and V axes.

The overarching problem is: when do we stop and ship?
For that we need:
- a set of standards that are somewhere between A-to-Z and WGL4;
- adaptability for graphic and display fonts that are not
  intended to support full typographical work.

Some tool support will be useful too.
For example, given we have a set of accents—breve, acute, dot
above, and so on—and a set of letters,
we need to find out which unicode characters can be formed using
combinations of those, and
then find out which of those characters are and are not present
in a particular font.

`font-andily`

Minor activity measuring some metrics.

`font-akern`

Massive side-track into daggers and other weapons, which was fun.
I think there is potential for both far greater variation in the
dagger glyph than we usually see,
and for other glyphs representing other weapons.

I practised by breve here, first accent for Akern, before moving
onto `font-atwin`.

Prior to that we appear to completed the first draft of lower
case letters and numbers, and some of the more obvious punctuation.

`font-amelt`

unhappy with name.
Had an idea to use a reflected **S** for the **8**.
Works well.
Relief.

`font-agon`

Made the crossed-grid design for **X** the primary glyph.


## Tools

`ccmap`, `ghost`, `plak` continue.
This work is useful and more tools that inspect fonts and
can combine character and glyph sets would be useful too.


## Talks

Attending Typographics Conference 2021 which is online.
It's hard work but the talks are mostly interesting.
The kinds of people who talk at this conference seem to be
academic or academic adjacent.

I appear to have made a submission for TypeWknd.


## FontsInUse

Wrote a few more entries for FontsInUse but also got a
first-warning from their staff for not doing enough to find
good artwork.
Duly noted.
