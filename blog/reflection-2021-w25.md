# 2021-W25 2021-06-24

Everything is tiring.
Reflections are getting hard to write.
Evaluations are even harder.

## Examples

I found the hypefortype submission pack.
So i started making example images.

I got signed up to Monotype, that's cool.

Making examples is slow and difficult.

## Fonts

`font-atwin`

As a result of:
- adjusting the ccmap tool
- finding minimal charasets from HypeForType
- writing chas

I have made a substantial push to fill in the punctuation and
other marks. Euro, Thorn, sterling, etc.

`font-akern`

Add a ligature (as in garrote, pun).

Small amount of spacing and kerning.
Made a birthday card for Tom.


`font-amelt`

Still having changed name to Amplette.


## Talks

I think i saw some more TypeLab talks. They are still fun.

