# 2021-W50 2021-12-16

The mornngs are crushingly dark.


## Linocuts!

Many cards made, written, and posted.


## Lino fonting

Got digital scans from AL. Wrote a preliminary evaluation.
Started digitising the font called ONE.
About half of an alphabet so far.

The digitising shares some problems with previous digitisation
projects (Atwin, Andily) in that i can't be sure what the
intention is.

In this particular case there is also the problem of how well to
model the bumps of the linocut. I think it adds character at
medium sizes, but looks awful at very large sizes.


## ILT

Still no movement on this. I need to ping them.


## Afublo

Draw a second ampersand.

Needs a name.


## fext

(was `feax`)

Added support for the inline syntax for Context Substitutions.
It is used for particular cases of SingleSubst and
LigatureSubst, but those seem to be the most common.

The general syntax for Context Substitution (using keyword
`lookup`) is not implemented, but also i haven't found any
examples.

Most fonts.google.com fonts seem to be TTF and `fext` doesn't
work on TTF yet. Maybe extending it would help.

It is clear from running it on real world examples that some
glyph group naming would help, and possibly lookup naming.
There are lots of common rules that cold probably be
algorithmically named (`@numbers = [one two ...])`.


## plak

Added the `--chars` option.

Still needs the `--blank` option.


## feature-zone

Motivated by:
- test `fext` a more robustly, and;
- illustrate AFDKO Feature File semantics more accurately.

I created `feature-zone`, a sandpit for creating OTF files
with rules that illustrate a particular point.

Basis is a font that is stripped-back version of Airvent and
some feature files.


## Fonts

`font-avimode`

Spotted that the brackets clash with test with accents below and
expanded the gap by 64 FDU.

Added tildedotbelow.


## END
