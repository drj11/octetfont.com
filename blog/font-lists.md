[brutal]: #title "List of Fonts"

## REFERENCES

[COLES2016] “The Geometry of Type”, Stephen Coles.
aka “The Anatomy of Type”.

[CHENG2007] “designing type”, Karen Cheng.
aka _designing_.


## Lists to add:

- Rookledge's Typefinder
- Letraset Catalogue
- All the mentions in _Elements_ (or the Specimen chapter)
- The PostScript 35 https://en.wikipedia.org/wiki/PostScript_fonts#Core_Font_Set
- All the mentions in Karen Cheng's _designing type_
- Wikipedia https://en.wikipedia.org/wiki/List_of_typefaces
  interesting because it is reasonably short, since presumably a
  font has to be "notable" for inclusion on the list (it doesn't
  list all the fonts on Wikipedia though; for example Coronet is
  not listed).
- FontShop Best 100 https://bonfx.com/best-fonts/


## Classic Fonts In Use

The fontsinuse.com website provide one insight into font
popularity (although it tends to be a bit focussed on the 21st
Century and recent dates, partly because it is easier to find
samples).

The list of most popular fonts by number of entries has some
undoubted classics:

- Futura
- Helvetica (and Akzidenz-Grotesk and Neue Helvetica)
- Univers
- Gill Sans
- ITC Avant Garde Gothic
- Suisse Int'l
- Gotham
- Franklin Gothic
- Cooper Black
- News Gothic

Even in this top ten i have made some artistic judgements:
I've rolled Neue Helvetica into Helvetica (probably okay), but
also bundled them with Akzidenz-Grotesk, the much older
prototype of Helvetica; Should Univers be included in the same
grouping? It's possible that Suisse International appears more
popular than it is because the foundry are very good at getting
a mention into fontsinuse.com. Franklin Gothic and News Gothic
are both MFB Gothics, are they different
weights of the same face (not really, but they were marketed as
such for a while).

- Times New Roman
- Harbour
- Eurostile
- Trade Gothic
- Arial
- Lausanne
- Knockout
- Optima
- Neue Haas Grotesk (a Helvetica really)
- FF DIN

The next batch is a bit more interesting. Times New Roman and
Arial, obviously (the defaults on Windows for decades). The
appearance of Harbour is again surely due to the designer
submitting many entries (it's a fine typeface, but no Optima);
same for Lausanne.

Knockout is a Hoefler & Co font from 2000. Possibly too early to
tell whether it's reached classic status.

Neue Haas Grotesk is the direct prototype for Helvetica, and
Arial is a Helvetica-inspired knock-off. So really that's
another 2 in the Helvetica group.

Eurostile is the first of the superellipses to appear.

Trade Gothic according to Fonts In Use is a commercial
competitor (knock-off) of News Gothic, one of the many
essentially interchangeable gothics of its time.

Optima is a legend in its own right.

FF DIN is an interesting appearance.
The DIN font is a geometric sans-serif
based on a race-track oval drawn for DIN, the german standards
body, and used in road signs and official documents.
Because of that there are many interpretations and version, of
which FF DIN is one.

Cracking on...

Alternate Gothic
Clarendon
Founders Grotesk
Futura Condensed
Graphik
Bodoni
Apoc
ITC Souvenir
Proxima Nova

Alternate Gothic is another in the Trade / News / Franklin
"family". Or maybe i am getting tired of Benton gothics.

Clarendon, the OG Egyptian.

Founders Grotesk is Kris Sowerby's Neo-Grotesque super family.
Should it be bundled in the Helvetica family?

Futura Condensed? It's Futura really isn't it?

Graphik is a Christian Schwartz Neo Grotesque branding
superfamily.

Bodoni. Obviously.

Apoc? It's good, but i don't think it's achieved classic status
yet.

ITC Souvenir. Obviously.

Proxima Nova. A modern (1994, revised 2005) legend. Apparently a
blend between the geometry of Futura and the rhythm of
Helvetica.

Eliminating the ones that i feel have "gamed" the system,
compressing this even further we have:

- Futura / Avant Garde Gothic
- Helvetica / Neue Helvetica / Neue Haas Grotesk /
  Akzidenz-Grotesk / Arial and other Neo Grotesques in homage
  (Founders Grotesque / Graphik)
- Gill Sans
- Gotham
- Benton Gothics (Trade / News / Franklin / Alternate)
- Cooper Black
- Times New Roman
- Eurostile
- Knockout
- Optima
- DIN
- Bodoni
- ITC Souvenir (the most well known of the Souvenirs)
- Proxima Nova

The only serifs are Times and Bodoni.

From this distance what we see is that Fonts In Use is dominated
by the sans-serifs favoured by advertising, commercial branding,
and posters.
The dominant groups are geometric sans (Futura, and so on), neo
grotesques (Helvetica, and so on), traditional gothics
(dominated by Morris Fuler Benton designs).
Then some humanist and slightly difficult to characterise
designs: Gill Sans, Gotham, Cooper Black, Eurostile, Optima,
ITC Souvenir.
A rational serif and sans serif in Bodoni and DIN.
And a sprinkling of modern classic (or wannabes), Knockout and
Proxima Nova.

The only one that really stands out is Knockout. It just doesn't
seem like i could mention that in converation with someone and
expect them to know what i was talking about.


## Some fonts drj knows and likes

Albertus
Futura
V.A.G.
Eras
Rotis
Russell Square
Eurostile
Data 70
Lydian
Raleway
Andika
Catamaran
Stop
Handel Gothic
Motter Tektura
Ad Lib


## extreme weight top/bottom

aka "reverse contrast", where the thickest strokes are top and bottom.
Though "reverse contrast" is probably best reserved for the
symmetric case.

asymmetric, aka "Bottom Heavy", or Bell Bottom

https://fontsinuse.com/typefaces/11051/bottleneck
https://fontsinuse.com/typefaces/32764/chwast-art-tone
https://fontsinuse.com/typefaces/32765/chwast-art-tone-boot-black
https://fontsinuse.com/typefaces/40551/litzenburg
https://fontsinuse.com/typefaces/22963/loose-caboose-nf
https://fontsinuse.com/typefaces/108844/rhythm-bold
https://fontsinuse.com/typefaces/79468/anonymous
https://fontsinuse.com/typefaces/7819/jackson
https://fontsinuse.com/typefaces/33088/cabeza-grossa
https://fontsinuse.com/typefaces/46680/lucky
https://fontsinuse.com/typefaces/44143/village-and-orbit
https://fontsinuse.com/typefaces/152150/tanglewood,unobtainable
https://fontsinuse.com/typefaces/1142/arnold-boecklin
https://fontsinuse.com/typefaces/31758/poster-mecanorma
https://fontsinuse.com/typefaces/32359/scorpio-and-hoopla
https://fontsinuse.com/typefaces/41739/somalia
https://fontsinuse.com/typefaces/13337/alfereta
https://fontsinuse.com/typefaces/87149/funkford
https://fontsinuse.com/typefaces/10233/jeanette-and-brandywine
https://fontsinuse.com/typefaces/47737/circo

top heavy

https://fontsinuse.com/typefaces/44139/ziptop-cooper-black
https://fontsinuse.com/typefaces/30687/wedge-gothic
https://fontsinuse.com/typefaces/44108/benguiat-zenedipity

symmetric

https://fontsinuse.com/typefaces/12282/kalligraphia
https://fontsinuse.com/typefaces/32060/crayonette
https://fontsinuse.com/typefaces/32521/gothic-bold
https://fontsinuse.com/typefaces/31801/sintex, missing?
https://fontsinuse.com/typefaces/31312/zipper

mixed

https://fontsinuse.com/typefaces/47076/davison-arabesque

What should we call the style, as in Davison Arabesque, where
the weight is sometimes at the top and sometimes at the bottom?


## Gourd fonts

- courgette (Karolina Lach); not on fontsinuse
- cucumber https://fontsinuse.com/typefaces/42542/cucumber
- https://fontsinuse.com/typefaces/4850/squash (Jan van Dijk);
  looks nice, but nothing to do with gourds

- there are several "pumpkin" fonts, but none called "pumpkin"
  and none well known.


## typequality open

Open fonts from the [typequality
project](http://typequality.com/find/).

Yatra One, https://fonts.google.com/specimen/Yatra+One
Poller One, https://fonts.google.com/specimen/Poller+One
Palanquin Dark, https://fonts.google.com/specimen/Palanquin+Dark
Palanguin, https://fonts.google.com/specimen/Palanquin
Sura, https://fonts.google.com/specimen/Sura
Lusitana, https://fonts.google.com/specimen/Lusitana
Almendra, https://fonts.google.com/specimen/Almendra
Bigshot One, https://fonts.google.com/specimen/Bigshot+One
Fredoka One, https://fonts.google.com/specimen/Fredoka+One
Kotta One, https://fonts.google.com/specimen/Kotta+One
Khula, https://fonts.google.com/specimen/Khula
Kadwa, https://fonts.google.com/specimen/Kadwa
Amaranth, https://fonts.google.com/specimen/Amaranth
Arbutus Slab, https://fonts.google.com/specimen/Arbutus+Slab
Ovo, https://fonts.google.com/specimen/Ovo
Catamaran, https://fonts.google.com/specimen/Catamaran

Typequality is missing:
Chunk, https://www.theleagueofmoveabletype.com/chunk, Meredith Mandel
Junction, https://www.theleagueofmoveabletype.com/junction, Caroline Hadilaksono
League Script
League Gothic
League Spartan
Sniglet, https://www.theleagueofmoveabletype.com/sniglet, Haley Fiege


## Enterprise Open

Fonts in this section are the sort of fonts that might be used
by a large corporation or other enterprise.
Generally large families with good language/glyph support
and often commissioned from professional type design studios.

They sit close to the mainstream of design tastes.
This is both a weakness—highly populated design space, low
variation—and a strength—useful as a yardstick.

Source Serif, https://github.com/adobe-fonts/source-serif-pro
Red Hat, https://github.com/RedHatOfficial/RedHatFont
IBM Plex, https://github.com/IBM/plex
Overpass, https://overpassfont.org/
Public Sans, https://public-sans.digital.gov/
TASA Explorer/Orbiter, https://tasatype.localremote.co/
Fira, https://github.com/mozilla/Fira
Intel One Mono, https://github.com/intel/intel-one-mono

Clear Sans, https://github.com/intel/clear-sans, compiled-only (no source)
PT Sans, https://fonts.google.com/specimen/PT+Sans, boycott


## WINDOWS 3.1

Arial [Monotype:Arial Regular:Version 1 (Microsoft)]
Courier New [Monotype:Courier New:version1(Microsoft)]
Times New Roman [Monotype:Times New Roman Regular:Version 1 (Microsoft)]


## From [COLES2016]

Many people make lists of fonts,
I suppose this one has the virtue of being the subject of an entire book,
and a book that is often recommended.

### Humanist Serif

Adobe Jenson
Cala
Bembo Book
FF Clifford
FF Scala
Lexicon
Minion
Garamond Premier
MVB Verdigris

### Transitional Serif

Adobe Caslon
Baskerville Original
Mrs Eaves
Plantin
Arnhem
Times New Roman
Le Monde Journal

### Rational Serif

Bauer Bodoni
ITC Bodoni
H&FJ Didot
Filosofia
Farnham
New Century Schoolbook
Miller
Eames Century Modern
Ingeborg
Melior

### Contemporary Serif

Neue Swift
Skolar
Fedra Serif
FF Meta Serif
Doko

### Inscribed/Engraved

Luxury Diamond
Albertus
Modesto
Trajan

### Grotesque Sans

Bureau Grot
Knockout
FF Bau

### Neo-Grotesque Sans

Univers
Neue Helvetica
Akkurat
National
Antique Olive

### Gothic Sans

Bell Centennial
News Gothic
Benton Sans
Whitney

### Geometric Sans

Futura ND
Avenir
Gotham
ITC Avant Garde Gothic
Calibre/Metric
FF DIN
Interstate
Verlag
Klavika
MVB Solano Gothic
Forza

### Humanist Sans

Gill Sans
FF Yoga Sans
Frutiger
Myriad
Verdana
Syntax
Cronos
TheSans
Auto
Optima
Beorcana

### Grotesque Slab

Giza
Clarendon
Farao
Heron Serif

### Humanist Slab

PMN Caecilia
FF Unit Slab
Adelle
Freight Micro

### Script

Kinescope
Studio Slant
Radio
Bickham Script
Tangier
Suomi Hand Script

# END
