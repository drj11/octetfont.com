[brutal]: #title "A selection of quotation systems"
[brutal]: #author "David Jones"

- North American  
They asked “what is this ‘kerning’ that you speak of?”
- British  
They asked ‘what is this “kerning” that you speak of?’
- French  
They asked « what is this ‹ kerning › that you speak of? »
- Danish  
They asked » what is this › kerning ‹ that you speak of? «
- German  
They asked „what is this ‚kerning‘ that you speak of?“
- Hungarian  
They asked „what is this »kerning« that you speak of?”
- Spanish  
They asked «what is this “kerning” that you speak of?»
- Swedish  
They asked ”what is this ’kerning’ that you speak of?”
---
This is just a sampling of the major forms.
Most of these are used in more than one country or
region and have variations beyond those given here.

From a typographical and type designer’s perspective,
it is pretty clear that one should expect any style of quote
to be used in any position, top-line or base-line, opening or closing,
forward or reversed, with or without space,
in obverse and reflected forms,
with quotes facing each other or with quotes facing the same way.
