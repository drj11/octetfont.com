# 2021-W26 2021-07-01

Many things are tiring particular admin.


## Examples

Making examples is slow and difficult.

Had the idea to use Taylor Swift inspo quotes.
Which is not bad.


## Fonts

`font-atwin`

The ccmap / chas / namu combo makes it easier and clearer to see
which characters are missing from a font.

Added yen.

Added commaaccent for G K L S T (Romanian);
added various "bar" composites: Lbar, Tbar, Hbar (for Maltese).

Added some Monotype required punctuation: ellipsis, guillemet,
periodcentered, degree, bullet, section, copyright, registered.

In Glyphs the ellipsis is composed of three period glyphs.
You can use the automatic alignment feature to automatically
align these by putting a "dot" and "_dot" anchor on the period
glyph. But... that then changes the alignment of all over glyphs
where you have two period components (like, colon, diaeresis).
Colon aligns dots one above the other; ellipsis aligns dots
side-by-side.
So we can't use the same anchors for both cases;

Have asked one of the TypeCritCrew for a crit.


## TypeCooker

I tried another TypeCooker but ran out of enthusiasm after the O
and the E.
I did learn a bit about using automatic serifs for corners in
Glyphs Mini.


## Tools

`chas`

A rewrite of `cs`; it's a simple tool, but hsa potential.
Already useful.
I added the `hft` and `monotype` options
(for HypeForType and Monotype).

`namu`

Added Regexp, multiple matches, and exact field matching.
All of which make integrating with `chas` and `ccmap` easier.


## Talks

Typographics Session 9, 2021-06-29.
I really loved the Africa theme of this session. 
Literally the least that Latinate designers can do is start to
educate themselves about non-Latin systems.
The _same plea_ was made by host Simon Charwey, wishing that
design schools _in Africa_ would incorporate some of this
African cultural knowledge into their curriculum.
